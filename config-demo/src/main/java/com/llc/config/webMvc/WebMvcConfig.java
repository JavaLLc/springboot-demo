//package com.llc.config.webMvc;
//
//import java.math.BigInteger;
//import java.nio.charset.Charset;
//import java.util.List;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.converter.HttpMessageConverter;
//import org.springframework.web.servlet.config.annotation.CorsRegistry;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//import com.alibaba.fastjson.serializer.SerializeConfig;
//import com.alibaba.fastjson.serializer.SerializerFeature;
//import com.alibaba.fastjson.serializer.ToStringSerializer;
//import com.alibaba.fastjson.support.config.FastJsonConfig;
//import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
//
//@Configuration
//public class WebMvcConfig implements WebMvcConfigurer {
//
//	@Override
//	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//		//设置静态资源的访问路径
//		registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
//		WebMvcConfigurer.super.addResourceHandlers(registry);
//	}
//
//	@Override
//	public void addCorsMappings(CorsRegistry registry) {
//		registry.addMapping("/**")
//		.allowedOrigins("*")
//		.allowCredentials(true)
//		.allowedMethods("GET","POST","DELETE","PUT")
//		.maxAge(3600);
//		WebMvcConfigurer.super.addCorsMappings(registry);
//	}
//
//	@Override
//	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//		System.out.println("配置生效");
//		// 初始化转换器
//        FastJsonHttpMessageConverter fastConvert = new FastJsonHttpMessageConverter();
//        // 初始化一个转换器配置
//        FastJsonConfig fastJsonConfig = new FastJsonConfig();
//        fastJsonConfig.setSerializerFeatures(
//                SerializerFeature.WriteDateUseDateFormat,//时间格式化
//                SerializerFeature.WriteNullStringAsEmpty,//String为null转化为空字符串
//                SerializerFeature.WriteNullNumberAsZero);//Number为null转化为0
//        //解决Long转json精度丢失的问题
//        SerializeConfig serializeConfig = SerializeConfig.globalInstance;
//        serializeConfig.put(BigInteger.class, ToStringSerializer.instance);
//        serializeConfig.put(Long.class, ToStringSerializer.instance);
//        serializeConfig.put(Long.TYPE, ToStringSerializer.instance);
//
//        fastJsonConfig.setSerializeConfig(serializeConfig);
//        // 将配置设置给转换器并添加到HttpMessageConverter转换器列表中
//        fastConvert.setFastJsonConfig(fastJsonConfig);
//        
//        // 中文乱码解决方案
//        fastConvert.setDefaultCharset(Charset.forName("utf-8"));
//        converters.add(fastConvert);
//		WebMvcConfigurer.super.configureMessageConverters(converters);
//	}
//
//}
