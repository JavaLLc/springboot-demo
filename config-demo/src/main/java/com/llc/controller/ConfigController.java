package com.llc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.llc.entity.User;

import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class ConfigController {

	@GetMapping("/hello")
	public User hello() {
		User user = new User();
		user.setName("王五");
		log.info("年龄为null:" + user);
		return user;
	}

	
	/**
	 * 请求的Content-Type设置为application/x-www-form-urlencoded,那么这个Post请求被认为是HTTP POST表单请求,
	 * 参数出现在form data,参数形式是key=value&key1=value1这种形式，后端获取使用@RequestParam; 
	 * 其他情况如使用原生AJAX的POST请求如果不指定请求头Request Header,默认使用的Content-Type是text/plain;charset=UTF-8,
	 * 或者application/json ,参数出现在Request payload块,其参数形式是标准的json格式，使用@RequestParam无法获取,需要使用@RequestBody获取。
	 */
	
	/**
	 * 后端通过@RequestBody注解解决axios POST请求后端获取不到参数的问题
	 * @param url
	 * @return
	 */
	@PostMapping("/hello2")
	public User hello2(@RequestBody JSONObject url) {
		System.out.println(url.get("name"));
		User user = new User();
		user.setName(url.get("name").toString());
		log.info("年龄为null:" + user);
		return user;
	}
	
	/**
	 * 前端通过添加请求头信息,并重新封装请求参数解决axios POST请求后端获取不到参数的问题
	 * @param url
	 * @return
	 */
	@PostMapping("/hello3")
	public User hello3(String name) {
		User user = new User();
		user.setName(name);
		log.info("年龄为null:" + user);
		return user;
	}
	
}
