package com.llc.mybatisplusdemo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.llc.mybatisplusdemo.entity.UserEntity;
import com.llc.mybatisplusdemo.mapper.UserDao;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class MybatisPlusDemoApplicationTests {

    @Autowired
    private UserDao userDao;

    /**
     * 根据id查询
     * SELECT id,user_name,phone,password,salt,head,login_count,register_date,last_login_date FROM user WHERE id=?
     */
    @Test
    public void selectById() {
        UserEntity user = userDao.selectById(1410080408);
        log.info(user.toString());
    }

    /**
     * https://mp.baomidou.com/guide/crud-interface.html#mapper-crud-%E6%8E%A5%E5%8F%A3
     * selectList 查询返回对象集合
     * selectMap 查询返回map集合
     * selectByMap 查询返回对象集合,参数条件为map
     */
    /**
     * 条件查询
     * 【ge >=】【le 小于等于<=】【eq =】 【gt 大于>】【lt <】【ne 不等于<>】
     * 【like 模糊查询 LIKE】 【in】【orderAsc 正向排序】【addFilter自由拼接sql】【last拼接到最后】
     * 【allEq 基于 map 内容等于=】 等等
     * SELECT id,user_name,phone,password,salt,head,login_count,register_date,last_login_date FROM user WHERE id >= ?
     */
    @Test
    public void findSelecctice() {
        List<UserEntity> list = userDao.selectList(Wrappers.<UserEntity>lambdaQuery()
                .ge(UserEntity::getId, 1410080408));
        log.info("查询数量:{}",list.size());
    }

    /**
     * 条件map查询
     */
    @Test
    public void findSelecctice2() {
        Map<String,Object> map = new HashMap<>();
        //根据列名
        map.put("id",1410080408);
        List<UserEntity> list = userDao.selectByMap(map);
        log.info("查询数量:{}",list.size());
    }

    /**
     * 新增
     * INSERT INTO user ( user_name, phone ) VALUES ( ?, ? )
     */
    @Test
    public void save() {
        UserEntity entity = new UserEntity();
        entity.setPhone("17776669999");
        entity.setUserName("测试添加");
        int insert = userDao.insert(entity);
        log.info("新增:{}",insert);

        List<UserEntity> list = userDao.selectList(Wrappers.<UserEntity>lambdaQuery()
                .eq(UserEntity::getPhone, "17776669999"));
        log.info("查询刚刚新增的数据",list.size());
        list.forEach(System.out::println);
    }

    /**
     * 修改
     * UPDATE user SET user_name=? WHERE phone = ?
     */
    @Test
    public void update() {
        UserEntity entity = new UserEntity();
        entity.setUserName("测试修改");
        int update = userDao.update(entity, Wrappers.<UserEntity>lambdaUpdate()
                .eq(UserEntity::getPhone, "17776669999"));
        log.info("新增:{}",update);

        List<UserEntity> list = userDao.selectList(Wrappers.<UserEntity>lambdaQuery()
                .eq(UserEntity::getPhone, "17776669999"));
        log.info("查询刚刚修改的数据",list.size());
        list.forEach(System.out::println);
    }

    /**
     * 删除
     * DELETE FROM user WHERE phone = ?
     */
    @Test
    public void delete() {
        int delete = userDao.delete(Wrappers.<UserEntity>lambdaQuery()
        .eq(UserEntity::getPhone,"17776669999"));
        log.info("删除:{}",delete);
        List<UserEntity> list = userDao.selectList(Wrappers.<UserEntity>lambdaQuery()
                .eq(UserEntity::getPhone, "17776669999"));
        log.info("查询刚刚删除的数据",list);
    }

}
