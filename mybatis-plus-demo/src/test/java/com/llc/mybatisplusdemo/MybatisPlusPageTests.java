package com.llc.mybatisplusdemo;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.llc.mybatisplusdemo.entity.UserEntity;
import com.llc.mybatisplusdemo.mapper.UserDao;

import lombok.extern.slf4j.Slf4j;

/**
 * 分页查询:需要配置分页插件(见启动类)
 * @author LLC
 * @date 2019年6月10日
 * @Description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class MybatisPlusPageTests {

	@Autowired
	private UserDao userDao;

	/**
	 * 分页查询
	 * SELECT id,user_name,phone,password,salt,head,login_count,register_date,last_login_date FROM user WHERE id >= ? LIMIT ?,?
	 */
	@Test
	public void selectPage() {
		Page<UserEntity> page = new Page<>(1, 5);
		IPage<UserEntity> userIPage = userDao.selectPage(page, new QueryWrapper<UserEntity>().ge("id", 1410080408));

		List<UserEntity> list = userIPage.getRecords();
		log.info(list.size() + "");
		log.info(list.get(0) + "");
	}

}
