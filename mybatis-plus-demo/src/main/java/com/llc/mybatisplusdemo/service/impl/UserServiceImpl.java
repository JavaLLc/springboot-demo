package com.llc.mybatisplusdemo.service.impl;

import com.llc.mybatisplusdemo.entity.UserEntity;
import com.llc.mybatisplusdemo.mapper.UserDao;
import com.llc.mybatisplusdemo.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LLC
 * @since 2019-05-30
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements IUserService {

}
