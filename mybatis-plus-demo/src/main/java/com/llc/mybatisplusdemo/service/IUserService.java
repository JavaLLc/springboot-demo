package com.llc.mybatisplusdemo.service;

import com.llc.mybatisplusdemo.entity.UserEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LLC
 * @since 2019-05-30
 */
public interface IUserService extends IService<UserEntity> {

}
