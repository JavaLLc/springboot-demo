package com.llc.mybatisplusdemo.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author LLC
 * @since 2019-05-30
 */
@Controller
@RequestMapping("/userEntity")
public class UserController {

}
