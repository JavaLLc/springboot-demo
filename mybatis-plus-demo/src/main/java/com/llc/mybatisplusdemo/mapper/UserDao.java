package com.llc.mybatisplusdemo.mapper;

import com.llc.mybatisplusdemo.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LLC
 * @since 2019-05-30
 */
public interface UserDao extends BaseMapper<UserEntity> {

}
