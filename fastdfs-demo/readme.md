###分布式文件系统fastdfs
#####安装  参考：https://www.cnblogs.com/handsomeye/p/9451568.html

#####安装libfastcommon(需要安装gcc环境)
```
1.cd /home
2.wget https://github.com/happyfish100/libfastcommon/archive/V1.0.38.tar.gz
3.tar -zxvf V1.0.38.tar.gz
4.删除安装包 rm -f V1.0.38.tar.gz
5.移动并重命名 mv libfastcommon-1.0.38 /usr/local/libfastcommon
6.cd /usr/local/libfastcommon/
7.执行编译：./make.sh
8.安装：./make.sh install
```

#####安装FastDFS
```
1.cd /home
2.wget https://github.com/happyfish100/fastdfs/archive/V5.11.tar.gz
3.解压安装包：tar -zxvf V5.11.tar.gz
4.删除安装包 rm -f V5.11.tar.gz
5.移动并重命名 mv fastdfs-5.11 /usr/local/fastdfs
6.cd /usr/local/fastdfs/
7.执行编译：./make.sh
8.安装：./make.sh install
9.查看可执行命令：ls -la /usr/bin/fdfs*
```

#####配置Tracker服务
```
(指运行于服务器上的一个程序,这个程序能够追踪到底有多少人同时在下载同一个文件)
1.cd /etc/fdfs/
2.通过cp命令拷贝tracker.conf.sample，删除.sample后缀作为正式文件
cp tracker.conf.sample tracker.conf

3.编辑tracker.conf,修改相关参数
vi tracker.conf

base_path=/home/fastdfs  #tracker存储data和log的跟路径，必须提前创建好(使用mkdir命令)
port=22122 #tracker默认22122
http.server_port=80 #http端口，需要和nginx相同

4.启动tracker（支持start|stop|restart）
/usr/bin/fdfs_trackerd /etc/fdfs/tracker.conf start

5.查看tracker启动日志
进入刚刚指定的base_path(/home/fastdfs)中有个logs目录，查看tracker.log文件
tail -f /home/fastdfs/logs/trackerd.log

6.查看端口情况：netstat -apn|grep fdfs
```

#####配置Storage服务(存储服务)
```
1.进入/etc/fdfs目录，有cp命令拷贝storage.conf.sample，删除.sample后缀作为正式文件
cd /etc/fdfs/
cp storage.conf.sample storage.conf

2.编辑storage.conf,修改相关参数：
vi storage.conf

base_path=/home/storage   #storage存储data和log的跟路径，必须提前创建好
port=23000  #storge默认23000，同一个组的storage端口号必须一致
group_name=group1  #默认组名，根据实际情况修改
store_path_count=1  #存储路径个数，需要和store_path个数匹配
store_path0=/home/storage  #如果为空，则使用base_path
tracker_server=192.168.5.133:22122 #配置该storage监听的tracker的ip和port

3.启动storage（支持start|stop|restart）
/usr/bin/fdfs_storaged /etc/fdfs/storage.conf start

4.查看storage启动日志
进入刚刚指定的base_path(/home/mm/fastdfs/storage)中有个logs目录，查看storage.log文件
tail -f /home/storage/logs/storaged.log

5.此时再查看tracker日志：发现已经开始选举，并且作为唯一的一个tracker，被选举为leade
I am the new tracker leader 192.168.5.133:22122

6.查看端口情况
netstat -apn|grep fdfs

7.通过monitor来查看storage是否成功绑定
/usr/bin/fdfs_monitor /etc/fdfs/storage.conf
打印:
Storage 1:
		id = 192.168.5.133
		...
```

#####安装Nginx和fastdfs-nginx-module模块
```
1.安装nginx(yum安装的是二进制文件,是已经编译好的nginx，它只有配置文件和二进制文件,想加第三方模块要包括源代码)
cd /home
wget https://nginx.org/download/nginx-1.14.0.tar.gz
tar zxvf nginx-1.14.0.tar.gz
rm -f nginx-1.14.0.tar.gz
mv nginx-1.14.0 /usr/local/nginx
cd /usr/local/nginx
./configure --prefix=/usr/local/nginx --with-http_stub_status_module --with-http_ssl_module --with-pcre=/usr/local/pcre
make 
make install
查看版本:/usr/local/nginx/sbin/nginx -v 

2.下载fastdfs-nginx-module安装包
cd /home
wget https://github.com/happyfish100/fastdfs-nginx-module/archive/V1.20.tar.gz
tar -xvf V1.20.tar.gz
rm -f V1.20.tar.gz 
mv fastdfs-nginx-module-1.20 /usr/local/fastdfs-nginx-module

3.在storage中安装Ngnix
cd /usr/local/nginx
./configure --prefix=/usr/local/nginx --add-module=/usr/local/fastdfs-nginx-module/src/
make
make install

cd /usr/local/fastdfs-nginx-module/src/ 
vi  config
CORE_INCS="$CORE_INCS /usr/local/include/fastdfs /usr/local/include/fastcommon/"
CORE_LIBS="$CORE_LIBS -L/usr/local/lib -lfastcommon -lfdfsclient"
把local都去掉
```

#####配置Nginx和fastdfs-nginx-module模块
```
1.
cd /usr/local/fastdfs-nginx-module/src/ 
cp mod_fastdfs.conf /etc/fdfs

2.进入/etc/fdfs修改mod-fastdfs.conf
cd /etc/fdfs
vi mod-fastdfs.conf

base_path=/home/fastdfs
tracker_server=192.168.5.133:22122 #tracker的地址
url_have_group_name=true #url是否包含group名称
storage_server_port=23000 #需要和storage配置的相同
store_path_count=1  #存储路径个数，需要和store_path个数匹配
store_path0=/home//storage #文件存储的位置

3.配置nginx，80端口server增加location
cd /usr/local/nginx/conf/
vi nginx.conf

location~/M00{
	root /home/storage/data
	ngx_fastdfs_module;
}

4.拷贝fastdfs解压目录中的http.conf和mime.types
cd /usr/local/fastdfs/conf
cp mime.types http.conf /etc/fdfs/
```

#####注意:
```
防火墙需要开放端口22122/23000/80
```
