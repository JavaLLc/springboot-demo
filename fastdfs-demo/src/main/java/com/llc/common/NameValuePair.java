package com.llc.common;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author LLC
 * @date 2019年6月11日
 * @Description:
 */
@Getter
@Setter
public class NameValuePair {
	protected String name;
	protected String value;

	public NameValuePair() {
	}

	public NameValuePair(String name) {
		this.name = name;
	}

	public NameValuePair(String name, String value) {
		this.name = name;
		this.value = value;
	}

}
