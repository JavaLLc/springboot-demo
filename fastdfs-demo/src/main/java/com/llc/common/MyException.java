package com.llc.common;

/**
 * 
 * @author LLC
 * @date 2019年6月11日
 * @Description:
 */
public class MyException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public MyException() {
	}

	public MyException(String message) {
		super(message);
	}
}
