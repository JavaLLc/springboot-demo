package com.llc.fastdfs;

import java.util.Date;

import lombok.Data;

@Data
public class FileInfo {
	protected String sourceIpAddr;
	protected long fileSize;
	protected Date createTimestamp;
	protected int crc32;

	public FileInfo(long file_size, int create_timestamp, int crc32, String source_ip_addr) {
		this.fileSize = file_size;
		this.createTimestamp = new Date(create_timestamp * 1000L);
		this.crc32 = crc32;
		this.sourceIpAddr = source_ip_addr;
	}

	public void setCreateTimestamp(int create_timestamp) {
		this.createTimestamp = new Date(create_timestamp * 1000L);
	}
}
