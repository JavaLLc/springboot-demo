package com.llc;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import com.llc.fastdfs.ClientGlobal;
import com.llc.fastdfs.StorageClient;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FastdfsDemoApplicationTests {

	@Test
	public void update_file() throws Exception {
		// 1. 设置加载的配置文件路径
		File file = ResourceUtils.getFile("classpath:fastdfs_client.conf");
		System.out.println("字节数组" + file.getPath());
		// 2. 初始化客户端全局对象(指定追踪服务器地址)
		ClientGlobal.init(file.getPath());

		/**
		 * 3.  
		 * 创建存储客户端对象    这个对象进行上传的操作
		 * 第一个参数：追踪服务对象(trackerServer) 可选
		 * 第二个参数：存储服务对象(storageServer) 可选
		 * */
		StorageClient storageClient = new StorageClient();

		// 3.1 上传文件到fastdfs分布式文件系统
		String[] upload_file_arr = storageClient.upload_file("d:\\tj.png", "png", null);
		/**
		 * 4.
		 * 数组中第一个元素: group组的名称 [group1
		 * 数组中第二个元素: 图片存储文件路径 M00/00/00/wKizgFmgHO6AB8W2ABnIeSg64cA639.jpg]
		 */
		System.out.println(Arrays.toString(upload_file_arr));
		// [group1, M00/00/00/wKizgFmgJ4CAThGJABnIeSg64cA321.jpg]

	}

	// 下载文件
	@Test
	public void download_file() throws Exception {
		// 1. 设置加载的配置文件路径
		File file = ResourceUtils.getFile("classpath:fastdfs_client.conf");
		System.out.println("字节数组" + file.getPath());
		// 2. 初始化客户端全局对象(指定追踪服务器地址)
		ClientGlobal.init(file.getPath());
		// 3. 创建存储客户端对象
		StorageClient storageClient = new StorageClient();

		/**
		 * 4. 从fastdfs分布式文件系统下载资源文件
		 * [group1, M00/00/00/wKgMgFlHqumASKsWAABonuLw4M4076.jpg]
		 * 数组中第一个元素：group1组的名称
		 * 数组中第二个元素：文件存储路径
		 */
		byte[] data = storageClient.download_file("group1", "M00/00/00/wKgFhVz_Y_-AVPnAAAKsNBEAN6c449.png");
		System.out.println("字节数组大小" + data.length);

		FileOutputStream fos = new FileOutputStream(new File("D:\\2.png"));
		fos.write(data);
		fos.flush();
		fos.close();
	}

	// 删除文件
	@Test
	public void delete_file() throws Exception {

		// 1. 设置加载的配置文件路径
		File file = ResourceUtils.getFile("classpath:fastdfs_client.conf");
		System.out.println("字节数组" + file.getPath());
		// 2. 初始化客户端全局对象(指定追踪服务器地址)
		ClientGlobal.init(file.getPath());

		StorageClient storageClient = new StorageClient();

		int state = storageClient.delete_file("group1", "M00/00/00/wKgFhVz_WkmAPl_rAAKsNBEAN6c653.png");
		System.out.println(state);
	}

}
