###Rocketmq(参考:https://www.jianshu.com/p/8c4c2c2ab62e)
#####1.简介
```
Rocketmq是阿里巴巴开源的一款分布式的消息中间件，他源于jms规范但是不遵守jms规范。rocketmq天生就是分布式的。
```
#####2.优点
```
1 rmq去除对zk的依赖
2 rmq支持异步和同步两种方式刷磁盘
3 rmq单机支持的队列或者topic数量是5w
4 rmq支持消息重试
5 rmq支持严格按照一定的顺序发送消息
6 rmq支持定时发送消息
7 rmq支持根据消息ID来进行查询
8 rmq支持根据某个时间点进行消息的回溯
9 rmq支持对消息服务端的过滤
10 rmq消费并行度:顺序消费 取决于queue数量,乱序消费 取决于consumer数量
```
#####3.windows下安装(见安装.md)

#####4.启动报错RocketMQ No route info of this topic问题
```
我的原因为:依赖问题,开始我的是
com.alibaba.rocketmq
换成
org.apache.rocketmq
就可以了
参考:https://blog.csdn.net/zzzgd_666/article/details/81481584
```
(2016年，阿里巴巴将RocketMQ的内核引擎捐赠给了Apache基金会)
