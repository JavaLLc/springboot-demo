package com.llc.exception;

import com.llc.constants.ErrorCode;

import lombok.Getter;
import lombok.Setter;

/**
 * 封装异常
 * @author LLC
 * @date 2019年6月13日
 * @Description:
 */
@Getter
@Setter
public class MyException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    /**
     * 错误编码
     */
    protected ErrorCode errCode;

    /**
     * 错误信息
     */
    protected String errMsg;

    /**
     * 无参构造函数
     */
    public MyException() {
        super();
    }
    public MyException(Throwable e) {
        super(e);
    }
    
    public MyException(ErrorCode errCode, String... errMsg) {
        super(errCode.getMsg());
        this.errCode = errCode;
    }
    
    /**
     * 构造函数
     *
     * @param cause 异常
     */
    public MyException(ErrorCode errCode, Throwable cause, String... errMsg) {
        super(errCode.getCode() + errCode.getMsg(), cause);
        this.errCode = errCode;
    }


}
