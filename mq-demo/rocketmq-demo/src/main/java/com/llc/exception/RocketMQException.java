package com.llc.exception;

import com.llc.constants.ErrorCode;

/**
 * 消息异常
 * @author LLC
 * @date 2019年6月13日
 * @Description:
 */
public class RocketMQException extends MyException{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 无参构造函数
	 */
	public RocketMQException() {
		super();
	}
	public RocketMQException(Throwable e) {
		super(e);
	}
	public RocketMQException(ErrorCode errorType) {
		super(errorType);
	}
	
	public RocketMQException(ErrorCode errorCode, String ... errMsg) {
		super(errorCode, errMsg);
	}
	
	public RocketMQException(ErrorCode errCode, Throwable cause,String ... errMsg) {
		super(errCode,cause, errMsg);
	}
}
