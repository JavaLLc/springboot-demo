package com.llc;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.llc.config.MQConsumeMsgListenerProcessor;

import lombok.extern.log4j.Log4j2;

@SpringBootApplication
@RestController
@Log4j2
public class RocketmqDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RocketmqDemoApplication.class, args);
	}

	@Autowired
	private DefaultMQProducer defaultMQProducer;

	@RequestMapping("/send")
	public String send(String msg) {
		if (msg == null) {
			msg = "发送消息-hello";
		}
		log.info("开始发送消息:" + msg);
		Message sendMsg = new Message(MQConsumeMsgListenerProcessor.TOPIC, MQConsumeMsgListenerProcessor.TAG,
				msg.getBytes());
		// 默认3秒超时
		SendResult sendResult;
		try {
			sendResult = defaultMQProducer.send(sendMsg);
			log.info("消息发送响应信息：" + sendResult.toString());
		} catch (Exception e) {
			log.error("发生异常:{}", e);
		}
		return msg;
	}
}
