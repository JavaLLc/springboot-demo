package com.llc.constants;

import java.io.Serializable;

/**
 * @author LLC
 * @date 2019年6月13日
 * @Description:错误编码
 */
public interface ErrorCode extends Serializable{
	/**
	 * 错误码
	 * @return
	 */
	String getCode();
	/**
	 * 错误信息
	 * @return
	 */
	String getMsg();
}
