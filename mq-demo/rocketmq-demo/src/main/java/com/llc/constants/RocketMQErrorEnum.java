package com.llc.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 消息队列枚举
 * @author LLC
 * @date 2019年6月13日
 * @Description:
 */

@Getter
@AllArgsConstructor
public enum RocketMQErrorEnum implements ErrorCode {
	// 公共
	PARAMM_NULL("ROCKETMQ_1001", "参数为空"),

	// 消费者
	NOT_FOUND_CONSUMESERVICE("MQ_2000", "根据topic和tag没有找到对应的消费服务"), 
	HANDLE_RESULT_NULL("MQ_2001", "消费方法返回值为空"),
	CONSUME_FAIL("ROCKETMQ_2002", "消费失败")

	;

	private String code;
	private String msg;

}
