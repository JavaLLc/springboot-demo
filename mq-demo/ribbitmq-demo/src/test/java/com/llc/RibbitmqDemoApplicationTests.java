package com.llc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.llc.fanoutexchange.SenderABC;
import com.llc.manytomany.HelloToManySender2;
import com.llc.onetomany.HelloToManySender;
import com.llc.onetoone.HelloSender;
import com.llc.topicexchange.Sender;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RibbitmqDemoApplicationTests {

	@Autowired
	private HelloSender helloSender;

	@Autowired
	private HelloToManySender helloToManySender;

	@Autowired
	private HelloToManySender2 helloToManySender2;

	@Autowired
	private Sender sender;

	@Autowired
	private SenderABC senderABC;

	@Test
	public void oneToOne() {
		helloSender.send();
	}

	@Test
	public void oneToMany() {
		for (int i = 0; i <= 10; i++) {
			helloToManySender.send(i);
		}
	}

	@Test
	public void manyToMany() {
		for (int i = 0; i <= 10; i++) {
			helloToManySender.send(i);
			helloToManySender2.send(i);
		}
	}

	@Test
	public void topicexchange() {
		sender.send1();
		sender.send2();
	}

	@Test
	public void fanoutechange() {
		senderABC.send();
	}

}
