package com.llc.topicexchange;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = TopicRabbitConfig.message1)
public class Reciver1 {

	@RabbitHandler
	public void process(String txt) {
		System.out.println("消费者1:" + txt);
	}
}
