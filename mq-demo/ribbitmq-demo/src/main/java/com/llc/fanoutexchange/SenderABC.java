package com.llc.fanoutexchange;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SenderABC {

	@Autowired
	AmqpTemplate amqpTemplate;

	public void send() {

		String content = "message from sender";
		System.out.println("Sender : " + content);
		this.amqpTemplate.convertAndSend("fanoutExchange", "", content);
	}
}
