package com.llc.fanoutexchange;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "faout.A")
public class ReciverA {

	@RabbitHandler
	public void process(String txt) {
		System.out.println("消费者 A: " + txt);
	}
}
