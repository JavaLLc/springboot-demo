package com.llc.fanoutexchange;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "faout.C")
public class ReciverC {

	@RabbitHandler
	public void process(String txt) {
		System.out.println("消费者 C: " + txt);
	}
}
