package com.llc.onetoone;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.hutool.core.date.DateUtil;


/**
 * @author LLC
 * @date 2019年6月12日
 * @Description:生产者
 */
@Component
public class HelloSender {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void send() {
        String content = "hello " + DateUtil.today();
        System.out.println(content);
        this.amqpTemplate.convertAndSend("hello", content);
    }
}
