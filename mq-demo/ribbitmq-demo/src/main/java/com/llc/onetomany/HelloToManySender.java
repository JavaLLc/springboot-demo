package com.llc.onetomany;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HelloToManySender {

	@Autowired
	private AmqpTemplate amqpTemplate;

	public void send(int i) {
		this.amqpTemplate.convertAndSend("hello2", i);
	}
}
