package com.llc.onetomany;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "hello2")
public class HelloReceiver2 {

	@RabbitHandler
	public void process(Integer i) {
		System.out.println("消费者2:" + i);
	}
}
