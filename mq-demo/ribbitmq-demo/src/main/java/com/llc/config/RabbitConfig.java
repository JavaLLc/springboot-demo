package com.llc.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author LLC
 * @date 2019年6月12日
 * @Description:队列配置
 */
@Configuration
public class RabbitConfig {

	@Bean
	public Queue Queue() {
		return new Queue("hello");
	}

	@Bean
	public Queue Queue2() {
		return new Queue("hello2");
	}
}
