package com.llc.manytomany;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HelloToManySender2 {

	@Autowired
	private AmqpTemplate amqpTemplate;

	public void send(int i) {
		System.out.println("生产者2:" + i);
		this.amqpTemplate.convertAndSend("hello2", i);
	}
}
