package com.llc.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

/**
 * 生产者
 * @author LLC
 * @date 2019年6月13日
 * @Description:
 */
@Component
@Log4j2
public class KafkaProducer {

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	public void send(String msg) {
		log.info("发送消息:" + msg);
		kafkaTemplate.send("hello", msg);
	}

}
