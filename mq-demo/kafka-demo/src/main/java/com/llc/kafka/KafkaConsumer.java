package com.llc.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

/**
 * 消费者:会监听这个主题，有消息就会执行
 * @author LLC
 * @date 2019年6月13日
 * @Description:
 */
@Component
@Log4j2
public class KafkaConsumer {

	@KafkaListener(topics = "hello")
	public void listen(ConsumerRecord<?, ?> record) throws Exception {
		log.info("接收消息:" + record.value());
		System.out.printf("topic = %s, offset = %d, value = %s \n", record.topic(), record.offset(), record.value());
	}
}
