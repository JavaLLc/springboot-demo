package com.llc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.llc.kafka.KafkaProducer;

@SpringBootApplication
@RestController
public class KafkaDemoApplication {

	@Autowired
	private KafkaProducer kafkaProducer;

	public static void main(String[] args) {
		SpringApplication.run(KafkaDemoApplication.class, args);
	}

	@RequestMapping("/hello")
	public String hello() {
		kafkaProducer.send("kafka-hello");
		return "success";
	}

}
