package com.llc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.llc.kafka.KafkaProducer;

@RunWith(SpringRunner.class)
@SpringBootTest
public class KafkaDemoApplicationTests {

	@Autowired
	private KafkaProducer kafkaProducer;

	@Test
	public void contextLoads() {
		kafkaProducer.send("kafka-hello");
	}

}
