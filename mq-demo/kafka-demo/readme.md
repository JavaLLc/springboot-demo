###Kafaka(参考:https://blog.csdn.net/lingbo229/article/details/80761778)
#####1.步骤
```
启动zk
1. 启动ZK服务:       bin/zkServer.sh start
2. 查看ZK服务状态:    bin/zkServer.sh status
3. 停止ZK服务:       bin/zkServer.sh stop
4. 重启ZK服务:       bin/zkServer.sh restart
```
```
启动kafka
./bin/kafka-server-start.sh config/server.properties &
停止
./bin/kafka-server-stop.sh
```
#####2.添加topic
```
bin/kafka-topics.sh --create --zookeeper 192.168.5.133:2181/kafka100 --topic hello --partitions 1
或者bin/kafka-topics.sh --create --zookeeper 127.0.0.1:2181/kafka100 --topic hello --replication-factor 1 --partitions 1
partion：topic的数据内容被划分为几块存储
replication-factor: 物理存储topic的内容采用几个副本的容错策略
hello为topic名称
```

#####3.启动项目
```
访问:http://localhost:8081/hello
即可发送消息
```
#####4.简介
```
kafka对消息保存时根据Topic进行归类，发送消息者成为Producer,消息接受者成为Consumer,此外kafka集群有多个kafka实例组成，每个实例(server)成为broker。
无论是kafka集群，还是producer和consumer都依赖于zookeeper来保证系统可用性集群保存一些meta信息。

 一个Topic可以认为是一类消息，每个topic将被分成多个partition(区),每个partition在存储层面是append log文件。
 任何发布到此partition的消息都会被直接追加到log文件的尾部，每条消息在文件中的位置称为offset（偏移量），offset为一个long型数字，它是唯一标记一条消息。
 它唯一的标记一条消息。kafka并没有提供其他额外的索引机制来存储offset，因为在kafka中几乎不允许对消息进行“随机读写”。
```
#####5.特点
```
- 高吞吐量、低延迟：kafka每秒可以处理几十万条消息，它的延迟最低只有几毫秒，每个topic可以分多个partition, consumer group 对partition进行consume操作。
- 可扩展性：kafka集群支持热扩展
- 持久性、可靠性：消息被持久化到本地磁盘，并且支持数据备份防止数据丢失
- 容错性：允许集群中节点失败（若副本数量为n,则允许n-1个节点失败）
- 高并发：支持数千个客户端同时读写
```
#####6.使用场景
```
- 日志收集：一个公司可以用Kafka可以收集各种服务的log，通过kafka以统一接口服务的方式开放给各种consumer，例如hadoop、Hbase、Solr等。
- 消息系统：解耦和生产者和消费者、缓存消息等。
- 用户活动跟踪：Kafka经常被用来记录web用户或者app用户的各种活动，如浏览网页、搜索、点击等活动，这些活动信息被各个服务器发布到kafka的topic中，
然后订阅者通过订阅这些topic来做实时的监控分析，或者装载到hadoop、数据仓库中做离线分析和挖掘。
- 运营指标：Kafka也经常用来记录运营监控数据。包括收集各种分布式应用的数据，生产各种操作的集中反馈，比如报警和报告。
- 流式处理：比如spark streaming和storm
- 事件源
等...
```

#####7.设计思想
```
- Kakfa Broker Leader的选举：Kakfa Broker集群受Zookeeper管理。所有的Kafka Broker节点一起去Zookeeper上注册一个临时节点，
因为只有一个Kafka Broker会注册成功，其他的都会失败，所以这个成功在Zookeeper上注册临时节点的这个Kafka Broker会成为Kafka Broker Controller，
其他的Kafka broker叫Kafka Broker follower。（这个过程叫Controller在ZooKeeper注册Watch）。这个Controller会监听其他的Kafka Broker的所有信息，
如果这个kafka broker controller宕机了，在zookeeper上面的那个临时节点就会消失，此时所有的kafka broker又会一起去Zookeeper上注册一个临时节点，
因为只有一个Kafka Broker会注册成功，其他的都会失败，所以这个成功在Zookeeper上注册临时节点的这个Kafka Broker会成为Kafka Broker Controller，
其他的Kafka broker叫Kafka Broker follower。例如：一旦有一个broker宕机了，这个kafka broker controller会读取该宕机broker上
所有的partition在zookeeper上的状态，并选取ISR列表中的一个replica作为partition leader（如果ISR列表中的replica全挂，
选一个幸存的replica作为leader; 如果该partition的所有的replica都宕机了，则将新的leader设置为-1，等待恢复，
等待ISR中的任一个Replica“活”过来，并且选它作为Leader；或选择第一个“活”过来的Replica（不一定是ISR中的）作为Leader），
这个broker宕机的事情，kafka controller也会通知zookeeper，zookeeper就会通知其他的kafka broker。
```

#####8.bug
```
这里曾经发生过一个bug，TalkingData使用Kafka0.8.1的时候，kafka controller在Zookeeper上注册成功后，它和Zookeeper通信的timeout时间是6s，
也就是如果kafka controller如果有6s中没有和Zookeeper做心跳，那么Zookeeper就认为这个kafka controller已经死了，就会在Zookeeper上把这个临时节点删掉，
那么其他Kafka就会认为controller已经没了，就会再次抢着注册临时节点，注册成功的那个kafka broker成为controller，然后，
之前的那个kafka controller就需要各种shut down去关闭各种节点和事件的监听。但是当kafka的读写流量都非常巨大的时候，TalkingData的一个bug是，由于网络等原因，
kafka controller和Zookeeper有6s中没有通信，于是重新选举出了一个新的kafka controller，但是原来的controller在shut down的时候总是不成功，
这个时候producer进来的message由于Kafka集群中存在两个kafka controller而无法落地。导致数据淤积。

这里曾经还有一个bug，TalkingData使用Kafka0.8.1的时候，当ack=0的时候，表示producer发送出去message，
只要对应的kafka broker topic partition leader接收到的这条message，producer就返回成功，
不管partition leader 是否真的成功把message真正存到kafka。当ack=1的时候，表示producer发送出去message，
同步的把message存到对应topic的partition的leader上，然后producer就返回成功，partition leader异步的把
message同步到其他partition replica上。当ack=all或-1，表示producer发送出去message，同步的把
message存到对应topic的partition的leader和对应的replica上之后，才返回成功。但是如果某个kafka controller 切换的时候，
会导致partition leader的切换（老的 kafka controller上面的partition leader会选举到其他的kafka broker上）,但是这样就会导致丢数据。
```


