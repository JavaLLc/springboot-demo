package com.llc;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.llc.jms.JmsService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActivemqDemoApplicationTests {

	@Autowired
	private JmsService JmsService;
	
	@Test
	public void contextLoads() {
		JmsService.send("发送消息--"+new Date());
	}

}
