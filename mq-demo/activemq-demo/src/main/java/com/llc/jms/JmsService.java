package com.llc.jms;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.jms.Queue;

/**
 * 
 * @author LLC
 * @date 2019年6月12日
 * @Description:
 */
@Component
public class JmsService {

	@Resource
	private JmsMessagingTemplate jmsMessagingTemplate;
	@Resource
	private Queue queue;

	/**
	 * 发送
	 * @param msg
	 */
	public void send(String msg) {
		jmsMessagingTemplate.convertAndSend(queue, msg);
	}

	/**
	 * 接收
	 * @param text
	 */
	@JmsListener(destination = "myQueue")
	public void receiveQueue(String text) {
		System.out.println(text);
	}
}
