package com.llc.jms;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.Queue;

/**
 * 
 * @author LLC
 * @date 2019年6月12日
 * @Description:
 */
@Configuration
public class JmsQueueConfig {

	@Bean
	public Queue queue() {
		return new ActiveMQQueue("myQueue");
	}
}
