package com.llc.fluent;

import org.springframework.stereotype.Component;

import com.baidu.unbiz.fluentvalidator.ValidationError;
import com.baidu.unbiz.fluentvalidator.Validator;
import com.baidu.unbiz.fluentvalidator.ValidatorContext;
import com.baidu.unbiz.fluentvalidator.ValidatorHandler;

/**
 * 校验器
 * @author LLC
 * @date 2019年6月28日
 * @Description:
 */
@Component
public class FluentValidatorDemo extends ValidatorHandler<Integer> implements Validator<Integer> {

	@Override
	public boolean validate(ValidatorContext context, Integer t) {
		if (t < 1) {
			context.addError(ValidationError.create("ID要大于1").setField("id"));
			return false;
		}
		return true;
	}

}
