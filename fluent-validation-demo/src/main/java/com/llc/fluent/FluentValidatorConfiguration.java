package com.llc.fluent;

import org.springframework.aop.framework.autoproxy.BeanNameAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baidu.unbiz.fluentvalidator.interceptor.FluentValidateInterceptor;

/**
 * 拦截器
 * @author LLC
 * @date 2019年6月28日
 * @Description:
 */
@Configuration
public class FluentValidatorConfiguration {

	// 注入我们的验证回调
	@Autowired
	private FluentValidateCallback fluentValidateCallback;

	@Bean("fluentValidateInterceptor")
	public FluentValidateInterceptor fluentValidateInterceptor() {
		FluentValidateInterceptor validateInterceptor = new FluentValidateInterceptor();
		validateInterceptor.setCallback(fluentValidateCallback);
		validateInterceptor.setLocale("zh_CN");
		validateInterceptor.setHibernateDefaultErrorCode(10000);
		return validateInterceptor;
	}

	@Bean
	public BeanNameAutoProxyCreator beanNameAutoProxyCreator() {
		BeanNameAutoProxyCreator proxyCreator = new BeanNameAutoProxyCreator();
		// 配置拦截对象 如拦截service(*ServiceImpl)
		proxyCreator.setBeanNames("*Controller");
		proxyCreator.setInterceptorNames("fluentValidateInterceptor");
		return proxyCreator;
	}
}
