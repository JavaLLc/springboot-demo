package com.llc.fluent;

import java.util.List;

import org.springframework.stereotype.Component;

import com.baidu.unbiz.fluentvalidator.ValidateCallback;
import com.baidu.unbiz.fluentvalidator.ValidationError;
import com.baidu.unbiz.fluentvalidator.Validator;
import com.baidu.unbiz.fluentvalidator.validator.element.ValidatorElementList;

/**
 * 验证回调 
 * @author LLC
 * @date 2019年6月28日
 * @Description:
 */
@Component
public class FluentValidateCallback implements ValidateCallback {

	/**
	 * 所有验证步骤结束,发现验证存在失败后
	 */
	@Override
	public void onFail(ValidatorElementList arg0, List<ValidationError> arg1) {
		// 可以自定义异常
		throw new RuntimeException("验证失败:" + arg1);
	}

	/**
	 * 所有验证步骤结束,成功回调
	 */
	@Override
	public void onSuccess(ValidatorElementList arg0) {
		System.out.println("验证成功");
	}

	/**
	 * 执行验证过程中发生了异常后
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void onUncaughtException(Validator arg0, Exception arg1, Object arg2) throws Exception {
		// 可以自定义异常
		throw new RuntimeException(arg1);
	}

}
