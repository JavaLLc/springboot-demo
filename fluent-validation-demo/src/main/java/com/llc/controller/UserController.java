package com.llc.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baidu.unbiz.fluentvalidator.annotation.FluentValid;
import com.llc.entity.User;

@RestController
public class UserController {

	@RequestMapping("/hello")
	public User demo(@FluentValid User user) {
		return user;
	}
	
}
