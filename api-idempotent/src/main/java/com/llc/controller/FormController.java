package com.llc.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.llc.annotation.CheckToken;
import com.llc.annotation.CreatToken;
import com.llc.constant.SourceConstant;
import com.llc.service.TokenService;

/**
 * @author LLC
 * @date 2019年6月14日
 * @Description:表单提交请求
 */
@RestController
public class FormController {

	@Autowired
	private TokenService tokenService;

	/**
	 * 注解@CreatToken,自动生成token,参数不需要携带
	 * @param req
	 * @return
	 */
	@RequestMapping("/hello")
	@CreatToken
	public String indexPage(HttpServletRequest req) {
		return "hello";
	}

	// 使用CheckToken注解方式保证请求幂等性
	@RequestMapping(value = "/formToken")
	@CheckToken(type = SourceConstant.EXTAPIFROM)
	@ResponseBody
	public String addOrder(String name, HttpServletRequest request) {
		// 业务逻辑
		return tokenService.idermpotentDemo(name);
	}
}
