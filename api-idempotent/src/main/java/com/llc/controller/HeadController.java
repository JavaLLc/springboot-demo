package com.llc.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.llc.annotation.CheckToken;
import com.llc.constant.SourceConstant;
import com.llc.service.TokenService;
import com.llc.utils.RedisToken;

/**
 *  token在请求头参数
 * @author LLC
 * @date 2019年6月14日
 * @Description:处理rpc调用请求
 */
@RestController
public class HeadController {

	@Autowired
	private RedisToken redisToken;

	@Autowired
	private TokenService tokenService;

	/**
	 * 创建token
	 * @return
	 */
	@RequestMapping(value = "/createToken")
	public String createRedisToken() {
		return redisToken.getToken();
	}

	// 使用CheckToken注解方式保证请求幂等性
	@RequestMapping(value = "/headToken")
	@CheckToken(type = SourceConstant.EXTAPIHEAD)
	public String addOrder(String name, HttpServletRequest request) {
		// 业务逻辑
		String result = tokenService.idermpotentDemo(name);
		return result != null ? "调用成功" : "调用失败";
	}

}
