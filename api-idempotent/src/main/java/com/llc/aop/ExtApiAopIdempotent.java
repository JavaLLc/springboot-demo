package com.llc.aop;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.llc.annotation.CheckToken;
import com.llc.annotation.CreatToken;
import com.llc.constant.SourceConstant;
import com.llc.utils.RedisToken;

/**
 * @author LLC
 * @date 2019年6月14日
 * @Description:接口幂等切面
 */
@Aspect
@Component
public class ExtApiAopIdempotent {

	@Autowired
	private RedisToken redisToken;

	// 切入点，拦截所有请求
	@Pointcut("execution(public * com.llc.controller.*.*(..))")
	public void checkAop() {
	}

	// 前置通知,不需要验证token的接口自动生成Token
	@Before("checkAop()")
	public void before(JoinPoint point) {
		MethodSignature signature = (MethodSignature) point.getSignature();
		// 如果方法上有CreatToken注解表示 自动生成token
		CreatToken declaredAnnotation = signature.getMethod().getDeclaredAnnotation(CreatToken.class);
		if (declaredAnnotation != null) {
			getRequest().setAttribute("token", redisToken.getToken());
		}
	}

	// 环绕通知拦截所有访问
	@Around("checkAop()")
	public Object doBefore(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		// 判断方法上是否有加ExtApiAopIdempotent注解
		MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();
		CheckToken declaredAnnotation = methodSignature.getMethod().getDeclaredAnnotation(CheckToken.class);
		if (declaredAnnotation != null) {
			String type = declaredAnnotation.type();
			String token = null;
			HttpServletRequest request = getRequest();
			if (type.equals(SourceConstant.EXTAPIHEAD)) {
				// 获取请求头中的token令牌
				token = request.getHeader("token");
			} else {
				// 从表单中获取token
				token = request.getParameter("token");
			}
			if (StringUtils.isEmpty(token)) {
				return "参数错误";
			}
			// 校验token
			boolean isToken = redisToken.checkToken(token);
			if (!isToken) {
				return "请勿重复提交!";
			}
		}
		// 放行
		Object proceed = proceedingJoinPoint.proceed();
		return proceed;
	}

	public HttpServletRequest getRequest() {
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = attributes.getRequest();
		return request;
	}

	public void response(String msg) throws IOException {
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletResponse response = attributes.getResponse();
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		PrintWriter writer = response.getWriter();
		try {
			writer.print(msg);
		} finally {
			writer.close();
		}
	}

}
