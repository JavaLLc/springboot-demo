package com.llc.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.UUID;

/**
 * @author LLC
 * @date 2019年6月14日
 * @Description:生成token,放入redis中
 */
@Component
public class RedisToken {

	@Autowired
	private BaseRedisService baseRedisService;

	private static final long TOKENTIME = 30 * 60;

	public String getToken() {
		String token = "token" + UUID.randomUUID();
		baseRedisService.setString(token, token, TOKENTIME);
		return token;
	}

	public boolean checkToken(String tokenKey) {
		String tokenValue = baseRedisService.getString(tokenKey);
		if (StringUtils.isEmpty(tokenValue)) {
			return false;
		}
		// 验证通过即删除,保证每个接口对应的token只能访问一次,保证接口幂等性问题
		baseRedisService.delKey(tokenKey);
		return true;
	}
}
