package com.llc.synplace;

/**
 * Synchronized
 * @author LLC
 * @date 2019年6月14日
 * @Description:类锁和对象锁
 */
public class SynchronizedPlace {

	public Object obj = new Object();

	// 1.静态同步函数,使用本类字节码做类锁（即SynchronizedPlace.class）
	public static synchronized void method1() {
	}

	// 同步代码块，使用字节码做类锁
	public void method2() {
		synchronized (SynchronizedPlace.class) {
		}
	}

	// 同步函数，使用本类对象实例即this做对象锁
	public synchronized void method3() {
	}

	// 同步代码块，使用本类对象实例即this做对象锁
	public void method4() {
		synchronized (this) {
		}
	}

	// 同步代码块，使用共享数据obj实例做对象锁。
	public void method5() {
		synchronized (obj) {
		}
	}

}
