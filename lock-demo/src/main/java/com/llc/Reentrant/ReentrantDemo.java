package com.llc.Reentrant;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import lombok.extern.log4j.Log4j2;

/**
 * 可重入锁:
 * 当线程A调用outer()方法的时候，会进入使用传进来mlock实例来进行mlock.lock()加锁，此时outer()方法中的这片区域的锁mlock就被线程A持有了，
 * 当线程B想要调用outer()方法时会先判断，发现这个mlock这把锁被其它线程持有了，因此进入等待状态。
  线程A进入outer()方法后，它还要调用inner()方法，并且inner()方法中使用的也是mlock()这把锁，
  线程A先判断mlock这把锁是否已经被持有了，判断后发现这把锁确实被持有了，
 A允许进入加了mlock锁的outer()方法中调用加了mlock锁的inner方法呢
 * @author LLC
 * @date 2019年6月14日
 * @Description:
 */
@Log4j2
public class ReentrantDemo implements Runnable {

	private Lock mLock;

	public ReentrantDemo(Lock mLock) {
		this.mLock = mLock;
	}

	public void outer() {
		mLock.lock();
		mLock.lock();
		inner();
		mLock.unlock();
		mLock.unlock();
	}

	public void inner() {
		mLock.lock();
		log.info("业务逻辑");
		mLock.unlock();
	}

	@Override
	public void run() {
		outer();
	}

	public static void main(String[] args) {
		ReentrantDemo lockDemo = new ReentrantDemo(new ReentrantLock());

		Thread t1 = new Thread(lockDemo);
		Thread t2 = new Thread(lockDemo);
		t1.start();
		t2.start();
		try {
			// 等待t1线程和t2线程执行完毕
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(1);
	}

}
