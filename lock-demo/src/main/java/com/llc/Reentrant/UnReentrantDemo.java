package com.llc.Reentrant;

/**
 * 不可重入锁（自旋锁）:
 * 当isLocked被设置为true后，在线程调用unlock()解锁之前不管线程是否已经获得锁，都只能wait()
 * 线程A获得第一把锁后isLocked被设置为true后,再获取第二把锁时只能wait,将一直等待下去
 * @author LLC
 * @date 2019年6月14日
 * @Description:
 */
public class UnReentrantDemo implements Runnable{

	private boolean isLocked = false;

	public synchronized void lock() throws InterruptedException {
		while (isLocked) {
			wait();
		}
		isLocked = true;
	}

	public synchronized void unlock() {
		isLocked = false;
		notify();
	}

	@Override
	public void run() {
		try {
			lock();
			lock();
			System.out.println("执行业务逻辑");
			unlock();
			unlock();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		UnReentrantDemo lockDemo = new UnReentrantDemo();

		Thread t1 = new Thread(lockDemo);
		Thread t2 = new Thread(lockDemo);
		t1.start();
		t2.start();
		try {
			// 等待t1线程和t2线程执行完毕
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(1);
	}
}
