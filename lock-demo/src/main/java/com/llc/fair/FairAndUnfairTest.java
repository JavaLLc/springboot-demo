package com.llc.fair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import lombok.extern.log4j.Log4j2;

/**
 * ReentrantLock实现公平锁与非公平锁
 * @author LLC
 * @date 2019年6月14日
 * @Description:
 */
@Log4j2
public class FairAndUnfairTest {
	// 能够使一个线程等待其他线程完成各自的工作后再执行
	// CountDownLatch是通过一个计数器来实现的，计数器的初始值为线程的数量。每当一个线程完成了自己的任务后，计数器的值就会减1。当计数器值到达0时，
	// 它表示所有的线程已经完成了任务，然后在闭锁上等待的线程就可以恢复执行任务。
	private static CountDownLatch start;

	private static class MyReentrantLock extends ReentrantLock {

		private static final long serialVersionUID = 1L;

		public MyReentrantLock(boolean fair) {
			super(fair);
		}

		public Collection<Thread> getQueuedThreads() {
			List<Thread> arrayList = new ArrayList<Thread>(super.getQueuedThreads());
			Collections.reverse(arrayList);
			return arrayList;
		}
	}

	private static class Worker extends Thread {

		private Lock lock;

		public Worker(Lock lock) {
			this.lock = lock;
		}

		@Override
		public void run() {
			try {
				start.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			// 连续两次打印当前的Thread和等待队列中的Thread
			for (int i = 0; i < 2; i++) {
				lock.lock();
				try {
					log.info("当前的Thread by [" + getName() + "], 等待队列中的Thread by "
							+ ((MyReentrantLock) lock).getQueuedThreads());
				} finally {
					lock.unlock();
				}
			}
		}

		public String toString() {
			return getName();
		}
	}

	public static void main(String[] args) {
		//Lock fairLock = new MyReentrantLock(true);
		Lock unfairLock = new MyReentrantLock(false);

		//testLock(fairLock);
		testLock(unfairLock);
	}

	private static void testLock(Lock lock) {
		start = new CountDownLatch(1);
		for (int i = 0; i < 5; i++) {
			Thread thread = new Worker(lock);
			thread.setName("" + i);
			thread.start();
		}
		start.countDown();
	}
}
