package com.llc.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity {
	public static String BL = "123";
	private String id;
	private String name;

	public void demo(String name) {
		System.out.println(name);
	}
}
