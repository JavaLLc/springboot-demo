package com.llc.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.llc.entity.UserEntity;

/**
 *  反射
 * @author LLC
 * @date 2019年6月20日
 * @Description:
 */
public class Reflect {

	@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
	public static void main(String[] args) {
		UserEntity u = new UserEntity();
		// 可以通过类的类型创建该类的实例对象
		try {
			// 方式1:
			Class c1 = UserEntity.class;
			// 方式2:
			Class c2 = u.getClass();
			// 方式3:
			Class c3 = Class.forName("com.llc.entity.UserEntity");

			// 转化实例
			UserEntity user1 = (UserEntity) c1.newInstance();
			UserEntity user2 = (UserEntity) c2.newInstance();
			UserEntity user3 = (UserEntity) c3.newInstance();

			// 获取方法信息
			Method[] methods = c1.getMethods();
			for (Method method : methods) {
				// System.out.println(method.getName());
			}

			// 获取成员变量的信息 getFileds()获取public getDeclaredFields()获取所有
			Field[] fields = c1.getFields();
			for (Field field : fields) {
				// System.out.println(field.getName());
			}

			// 获取构造函数的信息
			Constructor[] constructors = c1.getDeclaredConstructors();
			for (Constructor con : constructors) {
				// System.out.print(con.getName() + "(");
				Class[] typeParas = con.getParameterTypes();
				for (Class class1 : typeParas) {
					// System.out.print(class1.getName() + " ,");
				}
				// System.out.println(")");
			}

			// 方法反射的操作
			// 获取一个方法：需要获取方法的名称和方法的参数才能决定一个方法
			Method method = c3.getMethod("demo", String.class);
			// 方法的反射操作(声明接收类,参数)
			method.invoke(u, "反射调用方法");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
