package com.llc.util;

import java.math.BigDecimal;

public class BigDecimalTool {

	// 判空
	public static BigDecimal getNotNull(BigDecimal a) {
		if (a == null) {
			a = new BigDecimal(0);
		}
		return a;
	}

	/**
	 * 相加
	 * @param a
	 * @param b
	 * @return
	 */
	public static BigDecimal add(BigDecimal a, BigDecimal b) {
		return getNotNull(a).add(getNotNull(b));
	}

	/**
	 * 相减
	 * @param a
	 * @param b
	 * @return
	 */
	public static BigDecimal subtract(BigDecimal a, BigDecimal b) {
		return getNotNull(a).subtract(getNotNull(b));
	}

	/**
	 * 相乘
	 * @param a
	 * @param b
	 * @return
	 */
	public static BigDecimal multiply(BigDecimal a, BigDecimal b) {
		return getNotNull(a).multiply(getNotNull(b));
	}

	/**
	 * 相除
	 * divide(BigDecimal divisor, int scale, RoundingMode roundingMode)
	 * scale为小数位数/roundingMode为小数模式
	 * ROUND_HALF_UP四舍五入
	 * @param a
	 * @param b
	 * @return
	 */
	public static BigDecimal divide(BigDecimal a, BigDecimal b) {
		return getNotNull(a).divide(getNotNull(b), 2, BigDecimal.ROUND_HALF_UP);
	}

	public static void main(String[] args) {
		BigDecimal a = new BigDecimal(7);
		BigDecimal b = new BigDecimal(3);
		System.out.println(divide(a, b));
		// 保留小数两位
		System.out.println(new BigDecimal(1.333333).setScale(2, BigDecimal.ROUND_HALF_UP));
	}

}
