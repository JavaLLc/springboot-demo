package com.llc.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.llc.common.resp.Result;
import com.llc.service.IJobService;
import com.llc.vo.QuartzVO;

/**
 * Quartz任务
 * @author LLC
 * @date 2019年6月28日
 * @Description:
 */
@RestController
@RequestMapping("/job")
public class JobController {
	private final static Logger LOGGER = LoggerFactory.getLogger(JobController.class);

	@Autowired
	@Qualifier("Scheduler")
	private Scheduler scheduler;
	@Autowired
	private IJobService jobService;

	/**
	 * 新建任务
	 * @param quartz
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping("/add")
	public Result save(QuartzVO quartz) {
		LOGGER.info("新增任务");
		try {
			// 获取Scheduler实例、废弃、使用自动注入的scheduler、否则spring的service将无法注入
			// Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			// 如果是修改 展示旧的 任务
			if (quartz.getOldJobGroup() != null) {
				JobKey key = new JobKey(quartz.getOldJobName(), quartz.getOldJobGroup());
				scheduler.deleteJob(key);
			}
			Class cls = Class.forName(quartz.getJobClassName());
			cls.newInstance();
			// 构建job信息
			JobDetail job = JobBuilder.newJob(cls).withIdentity(quartz.getJobName(), quartz.getJobGroup())
					.withDescription(quartz.getDescription()).build();
			// 触发时间点
			CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(quartz.getCronExpression());
			Trigger trigger = TriggerBuilder.newTrigger()
					.withIdentity("trigger" + quartz.getJobName(), quartz.getJobGroup()).startNow()
					.withSchedule(cronScheduleBuilder).build();
			// 交由Scheduler安排触发
			scheduler.scheduleJob(job, trigger);
		} catch (Exception e) {
			e.printStackTrace();
			return Result.error();
		}
		return Result.ok();
	}

	/**
	 * 任务列表
	 * 分页功能不实现
	 * @param quartz
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@PostMapping("/list")
	public Result list(QuartzVO quartz, Integer pageNo, Integer pageSize) {
		LOGGER.info("任务列表");
		List<QuartzVO> list = jobService.listQuartzEntity(quartz.getJobName());
		return Result.ok(list);
	}

	/**
	 * 触发任务
	 * @param quartz
	 * @param response
	 * @return
	 */
	@PostMapping("/trigger")
	public Result trigger(QuartzVO quartz, HttpServletResponse response) {
		try {
			JobKey key = new JobKey(quartz.getJobName(), quartz.getJobGroup());
			scheduler.triggerJob(key);
		} catch (SchedulerException e) {
			e.printStackTrace();
			return Result.error();
		}
		return Result.ok();
	}

	@PostMapping("/pause")
	public Result pause(QuartzVO quartz, HttpServletResponse response) {
		LOGGER.info("停止任务");
		try {
			JobKey key = new JobKey(quartz.getJobName(), quartz.getJobGroup());
			scheduler.pauseJob(key);
		} catch (SchedulerException e) {
			e.printStackTrace();
			return Result.error();
		}
		return Result.ok();
	}

	@PostMapping("/resume")
	public Result resume(QuartzVO quartz, HttpServletResponse response) {
		LOGGER.info("恢复任务");
		try {
			JobKey key = new JobKey(quartz.getJobName(), quartz.getJobGroup());
			scheduler.resumeJob(key);
		} catch (SchedulerException e) {
			e.printStackTrace();
			return Result.error();
		}
		return Result.ok();
	}

	/**
	 * 移除任务
	 * @param quartz
	 * @param response
	 * @return
	 */
	@PostMapping("/remove")
	public Result remove(QuartzVO quartz, HttpServletResponse response) {
		try {
			TriggerKey triggerKey = TriggerKey.triggerKey(quartz.getJobName(), quartz.getJobGroup());
			// 停止触发器
			scheduler.pauseTrigger(triggerKey);
			// 移除触发器
			scheduler.unscheduleJob(triggerKey);
			// 删除任务
			scheduler.deleteJob(JobKey.jobKey(quartz.getJobName(), quartz.getJobGroup()));
			System.out.println("removeJob:" + JobKey.jobKey(quartz.getJobName()));
		} catch (Exception e) {
			e.printStackTrace();
			return Result.error();
		}
		return Result.ok();
	}
}
