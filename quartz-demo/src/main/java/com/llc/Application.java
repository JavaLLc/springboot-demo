package com.llc;

import org.mybatis.spring.annotation.MapperScan;
import org.quartz.SchedulerException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author LLC
 * @date 2019年6月28日
 * @Description:
 */
@SpringBootApplication
@MapperScan("com.llc.dao")
public class Application {
	
	public static void main(String[] args) throws InterruptedException, SchedulerException {
		SpringApplication.run(Application.class, args);
	}
}