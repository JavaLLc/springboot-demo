package com.llc.job;

import java.io.Serializable;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import com.llc.service.IJobService;
import com.llc.vo.QuartzVO;

/**
 * 实现序列化接口、防止重启应用出现quartz Couldn't retrieve job because a required class was not found 的问题
 */
public class TestJob implements Job, Serializable {

	private static final long serialVersionUID = 1L;

	// 注入jobService 执行相关业务操作
	@Autowired
	private IJobService jobService;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		List<QuartzVO> list = jobService.listQuartzEntity("");
		System.out.println(list);
		System.out.println("任务执行成功");
	}
}
