package com.llc.dao;

import java.util.List;

import com.llc.vo.QuartzVO;

/**
 * @author LLC
 * 注解Repository:标注数据访问组件，即DAO组件
 */
public interface IJobDao {
	
	List<QuartzVO> selectList(String jobName);

	Long selectCount(String jobName);
}
