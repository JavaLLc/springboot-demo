package com.llc.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author LLC
 * @date 2019年6月28日
 * @Description:
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class QuartzVO {

	private String jobName;// 任务名称
	private String jobGroup;// 任务分组
	private String description;// 任务描述
	private String jobClassName;// 执行类
	private String cronExpression;// 执行时间
	private String triggerName;// 执行时间
	private String triggerState;// 任务状态

	private String oldJobName;// 任务名称 用于修改
	private String oldJobGroup;// 任务分组 用于修改

}
