package com.llc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.llc.dao.IJobDao;
import com.llc.service.IJobService;
import com.llc.vo.QuartzVO;

@Service("jobService")
public class JobServiceImpl implements IJobService {

	@Autowired
	private IJobDao jobDao;

	@Override
	public List<QuartzVO> listQuartzEntity(String jobName) {
		List<QuartzVO> list = jobDao.selectList(jobName);
		return list;
	}

	@Override
	public Long count(String jobName) {
		return jobDao.selectCount(jobName);
	}
}
