package com.llc.service;

import java.util.List;

import com.llc.vo.QuartzVO;


public interface IJobService {

	/**
	 * 查询任务列表
	 * @param quartz
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	List<QuartzVO> listQuartzEntity (String jobName);

	/**
	 * 查询任务总数
	 * @param quartz
	 * @return
	 */
	Long count(String jobName);
}
