#quartz
#####项目
```
本项目复制于 小柒2012 
网址:https://gitee.com/52itstyle/spring-boot-quartz
前端页面完全照搬,后端做了修改,SpringBoot版本也改为2.0版本

Quartz 是一个完全由 Java 编写的开源作业调度框架，为在 Java 应用程序中进行作业调度提供了简单却强大的机制。
Quartz 可以与 J2EE 与 J2SE 应用程序相结合也可以单独使用。
Quartz 允许程序开发人员根据时间的间隔来调度作业。
Quartz 实现了作业和触发器的多对多的关系，还能把多个作业与不同的触发器关联。
```

#####概念
```
Job 表示一个工作，要执行的具体内容。此接口中只有一个方法:void execute(JobExecutionContext context)
JobDetail 表示一个具体的可执行的调度程序，Job 是这个可执行程调度程序所要执行的内容，另外 JobDetail 还包含了这个任务调度的方案和策略。 
Trigger 代表一个调度参数的配置，什么时候去调。 
Scheduler 代表一个调度容器，一个调度容器中可以注册多个 JobDetail 和 Trigger。当 Trigger 与 JobDetail 组合，就可以被 Scheduler 容器调度了。 
```

##### 几种任务调度
```
- Timer，简单无门槛，一般也没人用。
- spring @Scheduled注解，一般集成于项目中，小任务很方便。
- 开源工具 Quartz，分布式集群开源工具，以下两个分布式任务应该都是基于Quartz实现的，可以说是中小型公司必选，当然也视自身需求而定。
- 分布式任务 XXL-JOB，是一个轻量级分布式任务调度框架，支持通过 Web 页面对任务进行 CRUD 操作，支持动态修改任务状态、暂停/恢复任务，以及终止运行中任务，支持在线配置调度任务入参和在线查看调度结果。
- 分布式任务 Elastic-Job，是一个分布式调度解决方案，由两个相互独立的子项目 Elastic-Job-Lite 和 Elastic-Job-Cloud 组成。定位为轻量级无中心化解决方案，使用 jar 包的形式提供分布式任务的协调服务。支持分布式调度协调、弹性扩容缩容、失效转移、错过执行作业重触发、并行调度、自诊。
- 分布式任务 Saturn，Saturn是唯品会在github开源的一款分布式任务调度产品。它是基于当当elastic-job来开发的，其上完善了一些功能和添加了一些新的feature。目前在github上开源大半年，470个star。Saturn的任务可以用多种语言开发比如python、Go、Shell、Java、Php。其在唯品会内部已经发部署350+个节点，每天任务调度4000多万次。同时，管理和统计也是它的亮点。
```

##### 开发环境
```
JDK1.8、Maven、Eclipse
```

## 技术栈
```
SpringBoot2.0.6、thymeleaf、quartz2.3.0、iview、vue、layer、AdminLTE、bootstrap
```

## 启动说明
```
- 项目使用的数据库为MySql，选择resources/sql中的tables.sql文件初始化数据库信息。
- 在resources/application.properties 以及quartz.properties文件中替换为自己的数据源。
- 运行Application main方法，启动项目。
- 项目访问地址：http://localhost:8080/quartz
```

## 已实现功能
```
- 任务列表
- 任务新增和修改
- 任务执行
- 表达式生成器(集成：https://gitee.com/finira/cronboot)
- 任务移除
- Job中注入service为空的问题
- 系统启动，如果数据库任务为零则初始化测试任务，用于测试
```


