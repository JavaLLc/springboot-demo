## Ehcache(参考:https://blog.csdn.net/weixin_36279318/article/details/82820880 )
#####1.简介
```
Spring cache是代码级的缓存，他一般是使用一个ConcurrentMap。也就是说实际上还是是使用JVM的内存来缓存对象的，
那么肯定会造成大量的内存消耗。但是使用方便。
spring cache 的关键原理就是 spring AOP，通过 spring AOP，其实现了在方法调用前、调用后获取方法的入参和返回值，进而实现了缓存的逻辑
```

#####2.使用
```
在启动类添加注解@EnableCaching
在service层添加缓存的位置
@CacheConfig(cacheNames = "cacheStrategy") // 读取缓存配置
@Cacheable(key = "#p0") // 启用缓存
@CachePut(key = "#p0") // 更新缓存
@CacheEvict(key = "#p0") // 删除缓存
```

#####3.几个重要概念&缓存注解
```
Cache		缓存接口，定义缓存操作。实现有：RedisCache、EhCacheCache、 ConcurrentMapCache等
CacheManager	缓存管理器，管理各种缓存（Cache）组件
@Cacheable	主要针对方法配置，能够根据方法的请求参数对其结果进行缓存
@CacheEvict	清空缓存
@CachePut	保证方法被调用，又希望结果被缓存。
@EnableCaching	开启基于注解的缓存
keyGenerator	缓存数据时key生成策略
serialize	缓存数据时value序列化策略
```

#####4.@Cacheable/@CachePut/@CacheEvict 主要的参数
```
value	缓存的名称，在 spring 配置文件中定义，必须指定 至少一个
例如： @Cacheable(value=”mycache”) 或者 @Cacheable(value={”cache1”,”cache2”}

key	
缓存的 key，可以为空，如果指定要按照 SpEL 表达 式编写，如果不指定，则缺省按照方法的所有参数 进行组合
例如： @Cacheable(value=”testcache”,key=”#userName”

condition	
缓存的条件，可以为空，使用 SpEL 编写，返回 true 或者 false，只有为 true 才进行缓存/清除缓存，在 调用方法之前之后都能判断
例如： @Cacheable(value=”testcache”,condition=”#userNam e.length()>2”)

allEntries (@CacheEvict ) 是否清空所有缓存内容，缺省为 false，如果指定为 true，则方法调用后将立即清空所有缓存
例如：@CachEvict(value=”testcache”,allEntries=true)

beforeInvocation (@CacheEvict)
是否在方法执行前就清空，缺省为 false，如果指定 为 true，则在方法还没有执行的时候就清空缓存， 缺省情况下，如果方法执行抛出异常，则不会清空 缓存
例如：@CachEvict(value=”testcache”， beforeInvocation=true)

unless (@CachePut) (@Cacheable)	用于否决缓存的，不像condition，该表达式只在方 法执行之后判断，
此时可以拿到返回值result进行判 断。条件为true不会缓存，fasle才缓存	
例如：@Cacheable(value=”testcache”,unless=”#result == null”)
```

#####5.Cache SpEL available metadata
```
名字			位置			描述			实例
methodName	 	root object		当前被调用的方法名	 	#root.methodName
method	 	 	root object		当前被调用的方法	 	#root.method.name
target	 		root object		当前被调用的目标对象	#root.target
targetClass	 	root object		当前被调用的目标对象类	#root.targetClass
args	 		root object		当前被调用的方法的参数列表	#root.args[0]
caches	 		root object	 	当前方法调用使用的缓存列表（如@Cacheable(value={"cache1", "cache2"})），则有两个cache。 #root.caches[0].name
argument name		evaluation context	方法参数的名字. 可以直接 #参数名 ，也可以使用 #p0或#a0 的 形式，0代表参数的索引。	#a0、#p0
result			evaluation context	方法执行后的返回值（仅当方法执行之后的判断有效，如 ‘unless’，’cache put’的表达式 ’cache evict’的表达式 beforeInvocation=false）	#result
```

#####6.测试
```
1.访问http://localhost:8081/find?name=hello
打印:缓存中没有则会执行这个方法, 去数据库中查询
再次访问http://localhost:8081/find?name=hello
没有再打印,表示缓存成功

2.访问http://localhost:8081/add 此时方法名save作为key
访问http://localhost:8081/find?name=save
发现没有打印那句话,说明缓存成功

再访问http://localhost:8081/delete?name=save 删除缓存
再次访问http://localhost:8081/find?name=save
打印：缓存中没有则会执行这个方法, 去数据库中查询
说明缓存删除成功
```




