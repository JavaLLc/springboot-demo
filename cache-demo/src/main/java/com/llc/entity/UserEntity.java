package com.llc.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private Integer age;
}
