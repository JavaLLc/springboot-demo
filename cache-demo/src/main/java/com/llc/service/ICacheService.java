package com.llc.service;

import com.llc.entity.UserEntity;

public interface ICacheService {

	public UserEntity findUserByName(String name);
	
	public UserEntity save(UserEntity user);
	
	public boolean deleteByName(String name);

}
