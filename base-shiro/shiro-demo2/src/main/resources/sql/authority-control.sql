/*
Navicat MySQL Data Transfer

Source Server         : my
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : authority-control

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2019-06-27 16:52:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_permission`;
CREATE TABLE `t_permission` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '菜单名称',
  `pid` int(11) NOT NULL COMMENT '父级菜单id',
  `parent_name` varchar(50) DEFAULT NULL COMMENT '父级菜单名称',
  `type` tinyint(4) NOT NULL COMMENT '菜单类型 1:目录 2：菜单 2：按钮',
  `url` varchar(50) DEFAULT NULL COMMENT '菜单url',
  `code` varchar(50) DEFAULT NULL COMMENT '授权编码',
  `color` varchar(32) DEFAULT NULL COMMENT '颜色',
  `icon` varchar(32) DEFAULT NULL COMMENT '图标',
  `sort` tinyint(4) NOT NULL COMMENT '排序',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_permission
-- ----------------------------
INSERT INTO `t_permission` VALUES ('1', '系统管理', '0', null, '1', null, null, null, null, '1');
INSERT INTO `t_permission` VALUES ('2', '用户管理', '1', '系统管理', '2', 'user/listUI', 'user:listUI', null, null, '0');
INSERT INTO `t_permission` VALUES ('3', '新增', '2', '用户管理', '3', null, 'user:add', 'btn-primary', 'icon-ok', '2');
INSERT INTO `t_permission` VALUES ('4', '编辑', '2', '用户管理', '3', null, 'user:update', 'btn-warning', 'icon-edit', '3');
INSERT INTO `t_permission` VALUES ('5', '删除', '2', '用户管理', '3', null, 'user:delete', 'btn-danger', 'icon-trash', '4');
INSERT INTO `t_permission` VALUES ('6', '角色管理', '1', '系统管理', '2', 'role/listUI', 'role:listUI', null, null, '2');
INSERT INTO `t_permission` VALUES ('7', '新增', '6', '角色管理', '3', null, 'role:add', 'btn-primary', 'icon-ok', '2');
INSERT INTO `t_permission` VALUES ('8', '编辑', '6', '角色管理', '3', null, 'role:update', 'btn-warning', 'icon-edit', '3');
INSERT INTO `t_permission` VALUES ('9', '删除', '6', '角色管理', '3', null, 'role:delete', 'btn-danger', 'icon-trash', '4');
INSERT INTO `t_permission` VALUES ('10', '权限管理', '1', '系统管理', '2', 'permission/listUI', 'permission:listUI', null, null, '3');
INSERT INTO `t_permission` VALUES ('11', '新增', '10', '权限管理', '3', null, 'permission:add', 'btn-primary', 'icon-ok', '1');
INSERT INTO `t_permission` VALUES ('12', '编辑', '10', '权限管理', '3', null, 'permission:update', 'btn-warning', 'icon-edit', '2');
INSERT INTO `t_permission` VALUES ('13', '删除', '10', '权限管理', '3', null, 'permission:delete', 'btn-danger', 'icon-trash', '3');
INSERT INTO `t_permission` VALUES ('14', '设置角色', '2', '用户管理', '3', null, 'user:setRole', 'btn-success', 'icon-cog', '1');
INSERT INTO `t_permission` VALUES ('15', '设置权限', '6', '角色管理', '3', null, 'role:setPermission', 'btn-success', 'icon-cog', '1');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '角色名称',
  `descr` varchar(255) DEFAULT NULL COMMENT '角色说明',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', 'admin', '最高权限');
INSERT INTO `t_role` VALUES ('2', '副管理员', '没有删除功能');

-- ----------------------------
-- Table structure for t_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_role_permission`;
CREATE TABLE `t_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role_permission
-- ----------------------------
INSERT INTO `t_role_permission` VALUES ('30', '1', '1');
INSERT INTO `t_role_permission` VALUES ('31', '1', '2');
INSERT INTO `t_role_permission` VALUES ('32', '1', '3');
INSERT INTO `t_role_permission` VALUES ('33', '1', '4');
INSERT INTO `t_role_permission` VALUES ('34', '1', '5');
INSERT INTO `t_role_permission` VALUES ('35', '1', '14');
INSERT INTO `t_role_permission` VALUES ('36', '1', '6');
INSERT INTO `t_role_permission` VALUES ('37', '1', '7');
INSERT INTO `t_role_permission` VALUES ('38', '1', '8');
INSERT INTO `t_role_permission` VALUES ('39', '1', '9');
INSERT INTO `t_role_permission` VALUES ('40', '1', '15');
INSERT INTO `t_role_permission` VALUES ('41', '1', '10');
INSERT INTO `t_role_permission` VALUES ('42', '1', '11');
INSERT INTO `t_role_permission` VALUES ('43', '1', '12');
INSERT INTO `t_role_permission` VALUES ('44', '1', '13');
INSERT INTO `t_role_permission` VALUES ('45', '2', '1');
INSERT INTO `t_role_permission` VALUES ('46', '2', '2');
INSERT INTO `t_role_permission` VALUES ('47', '2', '3');
INSERT INTO `t_role_permission` VALUES ('48', '2', '4');
INSERT INTO `t_role_permission` VALUES ('49', '2', '14');
INSERT INTO `t_role_permission` VALUES ('50', '2', '6');
INSERT INTO `t_role_permission` VALUES ('51', '2', '7');
INSERT INTO `t_role_permission` VALUES ('52', '2', '8');
INSERT INTO `t_role_permission` VALUES ('53', '2', '15');
INSERT INTO `t_role_permission` VALUES ('54', '2', '10');
INSERT INTO `t_role_permission` VALUES ('55', '2', '11');
INSERT INTO `t_role_permission` VALUES ('56', '2', '12');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `email` varchar(64) DEFAULT NULL COMMENT '电子邮箱',
  `phone` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态1:启用 0：禁用',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'admin', '82d1901f6b9dfc4be714318f7f6a01cd', 'admin@163.com', null, '1', '2017-12-12 12:42:20', '2017-12-12 12:42:22', 'abc');
INSERT INTO `t_user` VALUES ('2', 'llc', '82d1901f6b9dfc4be714318f7f6a01cd', '1593697276@qq.com', '13366688999', '0', '2018-09-21 06:07:07', '2019-03-16 11:22:24', null);
INSERT INTO `t_user` VALUES ('3', '张七', '82d1901f6b9dfc4be714318f7f6a01cd', '1573677776@qq.com', '16285879687', '1', '2019-03-16 11:05:55', '2019-03-16 12:30:14', null);
INSERT INTO `t_user` VALUES ('4', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '0', '2019-03-16 11:08:11', '2019-03-16 13:35:47', null);
INSERT INTO `t_user` VALUES ('5', '李斯', '82d1901f6b9dfc4be714318f7f6a01cd', '1299567@163.com', '16285879687', '0', '2019-03-16 12:29:33', '2019-03-16 12:29:33', null);
INSERT INTO `t_user` VALUES ('9', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '0', '2019-03-16 13:54:05', '2019-03-16 13:54:05', null);
INSERT INTO `t_user` VALUES ('10', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '0', '2019-03-16 13:54:27', '2019-03-16 13:54:27', null);
INSERT INTO `t_user` VALUES ('11', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '0', '2019-03-16 13:57:18', '2019-03-16 13:57:18', null);
INSERT INTO `t_user` VALUES ('15', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '0', '2019-03-16 13:57:56', '2019-03-16 13:57:56', null);
INSERT INTO `t_user` VALUES ('21', '张7', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '1', '2019-03-16 17:07:21', '2019-03-17 17:10:34', null);
INSERT INTO `t_user` VALUES ('22', '张33', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '1', '2019-03-16 17:07:29', '2019-03-17 17:05:32', null);
INSERT INTO `t_user` VALUES ('27', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '0', '2019-03-18 16:56:14', '2019-03-18 16:56:14', null);
INSERT INTO `t_user` VALUES ('28', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '0', '2019-03-18 16:56:17', '2019-03-18 16:56:17', null);
INSERT INTO `t_user` VALUES ('29', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '0', '2019-03-18 16:56:21', '2019-03-18 16:56:21', null);
INSERT INTO `t_user` VALUES ('30', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '0', '2019-03-18 16:56:24', '2019-03-18 16:56:24', null);
INSERT INTO `t_user` VALUES ('31', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '0', '2019-03-18 16:56:28', '2019-03-18 16:56:28', null);
INSERT INTO `t_user` VALUES ('32', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '0', '2019-03-18 16:56:31', '2019-03-18 16:56:31', null);
INSERT INTO `t_user` VALUES ('33', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '0', '2019-03-18 16:56:35', '2019-03-18 16:56:35', null);
INSERT INTO `t_user` VALUES ('34', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '0', '2019-03-18 16:56:38', '2019-03-18 16:56:38', null);
INSERT INTO `t_user` VALUES ('35', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '1', '2019-03-18 16:56:41', '2019-03-22 16:36:02', null);
INSERT INTO `t_user` VALUES ('42', '张三', '82d1901f6b9dfc4be714318f7f6a01cd', '1234567@163.com', '16285879687', '0', '2019-03-22 16:41:29', '2019-03-22 16:41:29', null);

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES ('1', '1', '1');
INSERT INTO `t_user_role` VALUES ('2', '2', '2');
