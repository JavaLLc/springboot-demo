package com.llc.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.llc.entity.User;
import com.llc.vo.Result;

@Controller
public class LoginController {

	/**
	 * RESTful根据url来访问页面
	 * @param name
	 * @return
	 */
	@RequestMapping(value = "/{name}hh", method = RequestMethod.GET)
	public String demo(@PathVariable("name") String name) {
		return "/" + name;
	}

	@RequestMapping("login")
	@ResponseBody
	public Result login(String userName, String password) {
		UsernamePasswordToken token = new UsernamePasswordToken(userName.trim(), password);
		Subject subject = SecurityUtils.getSubject();
		try {
			System.out.println("开始登录");
			subject.login(token);
		} catch (UnknownAccountException e) {
			return Result.fail(403, "用户名不存在");
		} catch (IncorrectCredentialsException e) {
			System.out.println("登陆失败" + e);
			return Result.fail(403, "密码不正确");
		}
		return Result.succeed("manageUI");
	}

	@RequestMapping("/logout")
	@ResponseBody
	public Result logout(HttpSession session) {
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return Result.succeed("indexhh");
	}

	@RequiresPermissions("permission:listUI")
	@RequestMapping("manageUI")
	public String manageUI(HttpServletRequest request) {
		// 主体
		Subject subject = SecurityUtils.getSubject();
		User user = (User) subject.getPrincipal();
		request.setAttribute("loginUser", user);
		return "manageUI";
	}

	public static void main(String[] args) {
		String hex = DigestUtils.md5Hex("admin");
		System.out.println(hex);
	}
}
