package com.llc.web.realm;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.pagehelper.util.StringUtil;
import com.llc.entity.Permission;
import com.llc.entity.User;
import com.llc.service.PermissionService;
import com.llc.service.UserService;

public class MyRealm extends AuthorizingRealm {

	@Autowired
	private UserService userService;

	@Autowired
	private PermissionService permissionService;

	private static final Logger log = LoggerFactory.getLogger(MyRealm.class);

	/**
	 * 认证
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		log.info("------------------认证方法");
		// 获取用户名
		UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;
		String userName = usernamePasswordToken.getUsername();
		log.info("====UserRealm:token.getUsername()：" + userName);
		// String userName = (String) token.getPrincipal();
		// 通过用户名获取用户对象
		User user = this.userService.findUserByUserName(userName);
		if (user == null) {
			return null;
		}
		// 通过 userId 获取该用户拥有的所有权限，返回值根据自己要求设置，并非固定值。
		Map<String, List<Permission>> permissionMap = this.permissionService.getPermissionMapByUserId(user.getId());
		// （目录+菜单，分层级）
		user.setMenuList(permissionMap.get("menuList"));
		// （目录+菜单+按钮）
		user.setPermissionList(permissionMap.get("permissionList"));
		log.info("打印用户信息");
		ByteSource salt = ByteSource.Util.bytes("abc");
		SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user, user.getPassword(), salt, this.getName());
		log.info(info.toString());
		return info;
	}

	/**
	 * 授权
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		log.info("------------------授权方法");
		User user = (User) principals.getPrimaryPrincipal();
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		// （目录+菜单+按钮）
		List<Permission> permissionList = user.getPermissionList();
		// 添加权限授权
		for (Permission permission : permissionList) {
			if (StringUtil.isNotEmpty(permission.getCode())) {
				info.addStringPermission(permission.getCode());
			}
		}
		if (user.getUserName().equals("admin")) {
			log.info("添加角色授权");
			// 添加角色授权
			info.addRole("admin");
		}
		return info;
	}

}
