package com.llc.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Permission implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private String name;
	
	private Integer pid;
	
	private String parentName;
	
	private Integer type;
	
	private String url;
	
	private String code;
	
	private int sort;
	
	private String color;
	
	private String icon;
	
	private Boolean checked;
	
    private List<Permission> children = new ArrayList<>();

}
