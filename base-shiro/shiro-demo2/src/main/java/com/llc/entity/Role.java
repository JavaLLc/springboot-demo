package com.llc.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Role implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String name;

	private String descr;

	private Boolean selected;
}
