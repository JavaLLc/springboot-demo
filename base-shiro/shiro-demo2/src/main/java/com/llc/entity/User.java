package com.llc.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class User implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private String userName;
	
	private String password;
	
	private String email;
	
	private String phone;
	
	private String salt;
	
	private Integer status;
	
	private Date createTime;
	
	private Date updateTime;
	
	/**
	 * 封装目录+菜单
	 */
	private List<Permission> menuList;
	
	/**
	 * 封装目录+菜单+按钮
	 */
	private List<Permission> permissionList;

}
