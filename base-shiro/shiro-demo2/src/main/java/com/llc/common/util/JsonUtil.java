package com.llc.common.util;

import com.alibaba.fastjson.JSON;

/**
 * Json工具
 * @author LLC
 * @date 2019年6月27日
 * @Description:
 */
public class JsonUtil {

	public static String toJSONString(Object obj) {
		return JSON.toJSONString(obj);
	}

	public static Object parseObject(String text, Class<?> clazz) {
		return JSON.parseObject(text, clazz);
	}
}
