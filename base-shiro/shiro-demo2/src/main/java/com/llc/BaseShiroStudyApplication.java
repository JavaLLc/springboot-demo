package com.llc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@MapperScan("com.llc.mapper")
@SpringBootApplication
@EnableScheduling // 开启定时任务
@EnableTransactionManagement // 开启事务管理
public class BaseShiroStudyApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseShiroStudyApplication.class, args);
	}

}
