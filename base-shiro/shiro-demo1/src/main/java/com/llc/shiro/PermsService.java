package com.llc.shiro;

import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Component;

/**
 * 在JavaScript中使用Thymeleaf表达式
 *  实现按钮权限-例如:
 * var editFlag = "[[${@perms.hasPerm('user:list')}]]";
 * @author LLC
 * @date 2019年6月27日
 * @Description:
 */
@Component("perms")
public class PermsService {
	public boolean hasPerm(String permission) {
		return SecurityUtils.getSubject().isPermitted(permission);
	}
}
