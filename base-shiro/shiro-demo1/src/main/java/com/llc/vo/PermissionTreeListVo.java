package com.llc.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * 权限树
 * @author LLC
 * @date 2019年6月27日
 * @Description:
 */
@Getter
@Setter
public class PermissionTreeListVo {
	private Integer id;
	private String permissionId;
	private String name;
	private Integer parentId;
	private Boolean open = true;
	private Boolean checked = false;
}
