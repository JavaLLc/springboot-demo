package com.llc.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * session
 * @author LLC
 * @date 2019年6月26日
 * @Description:
 */
@Getter
@Setter
public class UserSessionVo {
	private String sessionId;
	private String username;
}
