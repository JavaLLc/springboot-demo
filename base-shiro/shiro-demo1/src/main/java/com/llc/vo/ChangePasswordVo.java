package com.llc.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 修改密码
 * @author LLC
 * @date 2019年6月26日
 * @Description:
 */
@Getter
@Setter
@ToString
public class ChangePasswordVo {
	String oldPassword;
	String newPassword;
	String confirmNewPassword;
}
