package com.llc.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author LLC
 * @date 2019年6月26日
 * @Description:
 * @param <T>
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseVo<T> {
	private Integer status;
	private String msg;
	private T data;
}
