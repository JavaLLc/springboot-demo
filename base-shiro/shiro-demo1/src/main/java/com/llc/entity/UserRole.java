package com.llc.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author LLC
 * @date 2019年6月26日
 * @Description:
 */
@Getter
@Setter
@ToString
public class UserRole implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;

	/**
	 * 用户id
	 */
	private String userId;

	/**
	 * 角色id
	 */
	private String roleId;

}