package com.llc.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author LLC
 * @date 2019年6月26日
 * @Description:
 */
@Getter
@Setter
@ToString
public class RolePermission implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer id;

    /**
     * 角色id
     */
    private String roleId;

    /**
     * 权限id
     */
    private String permissionId;
   
}