package com.llc.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author LLC
 * @date 2019年6月26日
 * @Description:
 */
@Getter
@Setter
@ToString
public class Role implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	/**
	 * 角色id
	 */
	private String roleId;

	/**
	 * 角色名称
	 */
	private String name;

	/**
	 * 角色描述
	 */
	private String description;

	/**
	 * 状态：1有效; 0无效
	 */
	private Integer status;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;

}