package com.llc.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author LLC
 * @date 2019年6月26日
 * @Description:
 */
@Getter
@Setter
@ToString
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	/**
	 * 用户id
	 */
	private String userId;

	/**
	 * 用户名
	 */
	private String username;

	private String password;

	/**
	 * 加密盐值
	 */
	private String salt;

	/**
	 * 昵称
	 */
	private String nickname;
	/**
	 * 邮箱
	 */
	private String email;

	/**
	 * 联系方式
	 */
	private String phone;

	/**
	 * 年龄：1男2女
	 */
	private Integer sex;

	/**
	 * 年龄
	 */
	private Integer age;

	/**
	 * 头像
	 */
	private String img;

	/**
	 * 用户状态：1有效; 0无效
	 */
	private Integer status;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;

	/**
	 * 最后登录时间
	 */
	private Date lastLoginTime;

	/**
	 * 登录ip
	 */
	private String loginIpAddress;

	/**
	 * 角色
	 */
	private List<Role> roles;

	/**
	*
	* 重写获取盐值方法，自定义realm使用
	* @return the credentials salt
	*/
	public String getCredentialsSalt() {
		return username + "hhh" + salt;
	}
}