package com.llc.service;

import java.util.Set;

/**
 * 
 * @author LLC
 * @date 2019年6月26日
 * @Description:
 */
public interface PermissionService {

    /**
     * 根据用户id查询权限集合
     * @param userId
     * @return set
     */
    Set<String> findPermsByUserId(String userId);

}
