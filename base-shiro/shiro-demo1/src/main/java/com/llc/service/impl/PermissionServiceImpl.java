package com.llc.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.llc.service.PermissionService;

/*
 * 
 */
@Service
public class PermissionServiceImpl implements PermissionService {

	@Override
	public Set<String> findPermsByUserId(String userId) {
		Set<String> set = new HashSet<>();
		set.add("权限1");
		set.add("account:create");
		return set;
	}

}
