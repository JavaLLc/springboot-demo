package com.llc.service;

import java.io.Serializable;
import java.util.List;

import com.llc.entity.User;
import com.llc.vo.UserOnlineVo;

/**
 * 
 * @author LLC
 * @date 2019年6月26日
 * @Description:
 */
public interface UserService {
	/**
	 * 在线用户列表
	 * @param userVo
	 * @return
	 */
	public List<UserOnlineVo> selectOnlineUsers(UserOnlineVo userVo);

	/**
	 * 踢出
	 * @param sessionId
	 * @param username
	 */
	public void kickout(Serializable sessionId, String username);

	/**
	 * 根据用户名查询用户
	 * @param username
	 * @return user
	 */
	User selectByUsername(String username);

	/**
	 * 根据用户id查询用户
	 * @param userId
	 * @return user
	 */
	User selectByUserId(String userId);

}
