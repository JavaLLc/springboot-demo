package com.llc.service;

import java.util.List;
import java.util.Set;

import com.llc.entity.Role;

/**
 * 
 * @author LLC
 * @date 2019年6月26日
 * @Description:
 */
public interface RoleService {

    /**
     * 根据用户id查询角色集合
     * @param userId
     * @return set
     */
    Set<String> findRoleByUserId(String userId);

    /**
     * 根据条件查询角色列表
     * @param role
     * @return list
     */
    List<Role> selectRoles(Role role);

}
