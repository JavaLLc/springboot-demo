package com.llc.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.llc.entity.Role;
import com.llc.service.RoleService;

/**
 * @version V1.0
 * @date 2018年7月11日
 * @author superzheng
 */
@Service
public class RoleServiceImpl implements RoleService {

	@Override
	public Set<String> findRoleByUserId(String userId) {
		Set<String> set = new HashSet<>();
		set.add("role1");
		set.add("role2");
		return set;
	}

	@Override
	public List<Role> selectRoles(Role role) {
		List<Role> list = new ArrayList<>();
		role = new Role();
		role.setId(1);
		role.setName("role1");
		list.add(role);
		return list;
	}

}
