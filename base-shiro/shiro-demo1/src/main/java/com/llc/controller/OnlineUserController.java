package com.llc.controller;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.llc.service.UserService;
import com.llc.vo.ResponseVo;
import com.llc.vo.UserOnlineVo;

@Controller
@RequestMapping("/online/user")
public class OnlineUserController {
	@Autowired
	private UserService userService;

	// 在线用户列表
	@PostMapping("/list")
	@ResponseBody
	public List<UserOnlineVo> onlineUsers(UserOnlineVo user) {
		List<UserOnlineVo> userList = userService.selectOnlineUsers(user);
		System.out.println(userList);
		return userList;
	}

	// 强制踢出用户
	@PostMapping("/kickout")
	@ResponseBody
	public ResponseVo<String> kickout(String sessionId, String username) {
		try {
			if (SecurityUtils.getSubject().getSession().getId().equals(sessionId)) {
				return new ResponseVo<String>(500, "不能踢出自己", null);
			}
			userService.kickout(sessionId, username);
			return new ResponseVo<String>(200, "踢出用户成功", null);
		} catch (Exception e) {
			return new ResponseVo<String>(500, "踢出用户失败", null);
		}
	}

}
