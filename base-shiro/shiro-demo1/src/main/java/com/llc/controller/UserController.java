package com.llc.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.llc.entity.User;
import com.llc.service.UserService;
import com.llc.shiro.MyShiroRealm;
import com.llc.util.CopyUtil;
import com.llc.util.PasswordHelper;
import com.llc.vo.ChangePasswordVo;
import com.llc.vo.ResponseVo;

/**
 * @author LLC
 * @date 2019年6月27日
 * @Description:
 */
@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private MyShiroRealm myShiroRealm;
	@Autowired
	private UserService userService;
	@Autowired
	private MyShiroRealm shiroRealm;

	/**
	 * 保存分配角色
	 * @param userId
	 * @param roleIdStr
	 * @return
	 */
	@PostMapping("/assign/role")
	@ResponseBody
	public ResponseVo<String> assignRole(String userId, String roleIdStr) {
		String[] roleIds = roleIdStr.split(",");
		List<String> roleIdsList = Arrays.asList(roleIds);
		System.out.println("角色名称" + roleIdsList);
		// 新增角色 TODO
		List<String> userIds = new ArrayList<>();
		userIds.add(userId);
		// 根据userId 清除当前session存在的用户的权限缓存
		myShiroRealm.clearAuthorizationByUserId(userIds);
		return new ResponseVo<String>(200, "成功", null);
	}

	/**
	 * 修改密码
	*/
	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	@ResponseBody
	public ResponseVo<String> changePassword(ChangePasswordVo changePasswordVo) {
		System.out.println(changePasswordVo);
		if (!changePasswordVo.getNewPassword().equals(changePasswordVo.getConfirmNewPassword())) {
			return new ResponseVo<String>(500, "两次密码输入不一致", null);
		}
		User loginUser = userService.selectByUserId(((User) SecurityUtils.getSubject().getPrincipal()).getUserId());
		
		User newUser = CopyUtil.getCopy(loginUser, User.class);
		String sysOldPassword = loginUser.getPassword();
		newUser.setPassword(changePasswordVo.getOldPassword());
		String entryOldPassword = PasswordHelper.getPassword(newUser);
		if (sysOldPassword.equals(entryOldPassword)) {
			newUser.setPassword(changePasswordVo.getNewPassword());
			PasswordHelper.encryptPassword(newUser);
			// 执行修改 TODO
			// 清除登录缓存
			List<String> userIds = new ArrayList<>();
			userIds.add(loginUser.getUserId());
			// 清除认证信息
			shiroRealm.removeCachedAuthenticationInfo(userIds);
			SecurityUtils.getSubject().logout();
		} else {
			return new ResponseVo<String>(500, "您输入的旧密码有误", null);
		}
		return new ResponseVo<String>(200, "修改密码成功", null);
	}

}
