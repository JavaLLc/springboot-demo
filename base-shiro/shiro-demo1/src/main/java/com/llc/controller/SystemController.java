package com.llc.controller;

import java.io.Serializable;
import java.util.Deque;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.subject.Subject;
import org.crazycake.shiro.RedisCacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.llc.entity.User;
import com.llc.vo.ResponseVo;

/**
 * @author LLC
 * @date 2019年6月27日
 * @Description:
 */
@Controller
public class SystemController {
	@Autowired
	private RedisCacheManager redisCacheManager;

	/* 提交登录 */
	@GetMapping("/login")
	@ResponseBody
	public ResponseVo<String> login(HttpServletRequest request, String username, String password,
			@RequestParam(value = "rememberMe", defaultValue = "0") Integer rememberMe) {
		System.out.println(username);
		System.out.println(password);
		UsernamePasswordToken token = new UsernamePasswordToken(username, password);
		try {
			token.setRememberMe(1 == rememberMe);
			Subject subject = SecurityUtils.getSubject();
			subject.login(token);
		} catch (LockedAccountException e) {
			token.clear();
			return new ResponseVo<String>(500, "用户已经被锁定不能登录，请联系管理员", null);
		} catch (AuthenticationException e) {
			token.clear();
			return new ResponseVo<String>(500, "用户名或者密码错误", null);
		}
		// 更新最后登录时间 TODO
		return new ResponseVo<String>(200, "登录成功", null);
	}

	/* 登出 */
	@RequestMapping(value = "/logout")
	@ResponseBody
	public ResponseVo<String> logout() {
		Subject subject = SecurityUtils.getSubject();
		if (null != subject) {
			String username = ((User) SecurityUtils.getSubject().getPrincipal()).getUsername();
			Serializable sessionId = SecurityUtils.getSubject().getSession().getId();
			Cache<String, Deque<Serializable>> cache = redisCacheManager
					.getCache(redisCacheManager.getKeyPrefix() + username);
			Deque<Serializable> deques = cache.get(username);
			for (Serializable deque : deques) {
				if (sessionId.equals(deque)) {
					deques.remove(deque);
					break;
				}
			}
			cache.put(username, deques);
		}
		subject.logout();
		return new ResponseVo<String>(200, "退出成功", null);
	}

	/**
	 * 有权限访问测试
	 * @return
	 */
	@RequiresPermissions("权限1")
	@GetMapping("/menu")
	@ResponseBody
	public String getMenus() {
		return "有权限访问";
	}

	/**
	 * 没有权限访问测试
	 * @return
	 */
	@RequiresPermissions("权限2")
	@GetMapping("/menu2")
	@ResponseBody
	public String getMenus2() {
		return "没有权限访问";
	}

}
