package com.llc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.llc.dao")
public class ShiroDemo1Application {

	public static void main(String[] args) {
		SpringApplication.run(ShiroDemo1Application.class, args);
	}
}
