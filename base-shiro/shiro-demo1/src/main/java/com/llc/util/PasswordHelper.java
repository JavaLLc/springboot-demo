package com.llc.util;

import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

import com.llc.entity.User;

/**
 * 密码加密工具类
 * @author LLC
 * @date 2019年6月26日
 * @Description:
 */
public class PasswordHelper {
	private static RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();
	private static String algorithmName = "md5";
	private static int hashIterations = 2;

	/**
	 * 密码加密
	 * @param user
	 */
	public static void encryptPassword(User user) {
		user.setSalt(randomNumberGenerator.nextBytes().toHex());
		String newPassword = new SimpleHash(algorithmName, user.getPassword(),
				ByteSource.Util.bytes(user.getCredentialsSalt()), hashIterations).toHex();
		user.setPassword(newPassword);
	}

	/**
	 * 获得加密的密码
	 * @param user
	 * @return
	 */
	public static String getPassword(User user) {
		String encryptPassword = new SimpleHash(algorithmName, user.getPassword(),
				ByteSource.Util.bytes(user.getCredentialsSalt()), hashIterations).toHex();
		return encryptPassword;
	}

	//872359cc44c637cc73b7cd55c06d95e4
	public static void main(String[] args) {
		User user = new User();
		user.setUsername("admin");
		user.setPassword("123456");
		user.setSalt("8cd50474d2a3c1e88298e91df8bf6f1c");
		//encryptPassword(user);
		String newPassword = new SimpleHash(algorithmName, user.getPassword(),
				ByteSource.Util.bytes(user.getCredentialsSalt()), hashIterations).toHex();
		System.out.println(newPassword);
	}
}
