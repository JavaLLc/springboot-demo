package com.llc.generate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GeneratePojo {

	private String dbName;

	private String tableName;

	private String db_driver;

	private String db_url;

	private String db_user;

	private String db_pwd;

	private String downloadPath;

	private String outputPath;

	private String packageEntity;
	private String packageDao;
	private String packageRepository;

	private String pathEntiy;
	private String pathDao;
	private String pahtRepository;
	private String pathXml;

}
