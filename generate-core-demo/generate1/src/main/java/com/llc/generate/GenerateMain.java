package com.llc.generate;

/**
 * 链接数据库生成实体对象
 * @author LLC
 * @date 2019年6月6日
 * @Description:
 */
public class GenerateMain {

	public static void main(String[] args) throws Exception {
		GeneratePojo pojo = new GeneratePojo();
		pojo.setDb_driver("com.mysql.jdbc.Driver");
		pojo.setDb_url("jdbc:mysql://127.0.0.1:3306/weixin?useUnicode=true&characterEncoding=utf-8");
		pojo.setDb_user("root");
		pojo.setDb_pwd("1234567");
		// 数据库
		pojo.setDbName("weixin");
		pojo.setPackageDao("com.llc.dao");
		pojo.setPackageEntity("com.llc.entity");
		pojo.setPackageRepository("com.llc.jpa");
		// 当前项目的根目录
		pojo.setOutputPath("/generate/data/");
		GenerateUtils tableEntity = new GenerateUtils();
		tableEntity.generateEntity(pojo);
	}

}