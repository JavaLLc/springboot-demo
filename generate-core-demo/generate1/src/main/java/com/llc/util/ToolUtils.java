package com.llc.util;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class ToolUtils {

	public static void main(String[] args) {
		String desc = "去除特殊符号\"OrderSourceEnum\"";
		System.out.println(replaceQuote(desc));
	}

	public static String replaceQuote(String dest) {
		if (StringUtils.isNotBlank(dest)) {
			if (dest.indexOf("\"") != -1) {
				dest = dest.replaceAll("\"", "");
			}
		}
		return dest;
	}

	/**
	 * 特殊字符串数据过滤
	 *
	 * @param str
	 *            字符串
	 * @return 过滤后的字符串
	 */
	public static String replaceBlank(String str) {
		String dest = "";
		if (StringUtils.isNotBlank(str)) {
			Pattern p = Pattern.compile("\\s*|\t|\r|\n");
			Matcher m = p.matcher(str);
			dest = m.replaceAll("");
		}

		return dest;
	}

	public static String replaceGenerate(String str) {
		String dest = "";
		if (StringUtils.isNotBlank(str)) {
			Pattern p = Pattern.compile("\\s*|\t|\r|\n");
			Matcher m = p.matcher(str);
			dest = m.replaceAll("");
			if (dest.indexOf("\"") != -1) {
				dest = dest.replaceAll("\"", "");
			}
		}

		return dest;
	}

	public static String getUUID() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	public static String getRepUUID() {
		String uuid = UUID.randomUUID().toString();
		uuid = uuid.replaceAll("-", "");
		return uuid;
	}
}
