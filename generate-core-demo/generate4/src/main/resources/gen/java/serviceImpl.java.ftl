package ${table.pkgName}.service.impl;

import ${table.pkgName}.dao.${table.clsName}Mapper;
import ${table.pkgName}.pojo.${table.clsName};
import ${table.pkgName}.pojo.${table.clsName}Example;
import ${table.pkgName}.service.${table.clsName}Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.sql.SQLException;
import java.util.List;


@Service
@Transactional(readOnly = false)
public class ${table.clsName}ServiceImpl implements ${table.clsName}Service {

    @Autowired
    private ${table.clsName}Mapper mapper;

    @Override
    public Boolean deleteById(String id) throws SQLException {
        int i = mapper.deleteByPrimaryKey(id);
        if (i > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean deleteByExample(${table.clsName}Example example) throws SQLException {
        return null;
    }

    @Override
    public Boolean insert(${table.clsName} ${table.paramName}) throws SQLException {
        int i = mapper.insert(${table.paramName});
        if (i > 0) {
            return true;
        }
        return false;
    }

    @Override
    public ${table.clsName} selectById(String id) throws SQLException {
    ${table.clsName} ${table.paramName} = mapper.selectByPrimaryKey(id);
        return ${table.paramName} != null ? ${table.paramName} : null;
    }

    @Override
    public List<${table.clsName}> selectByExample(${table.clsName}Example example) throws SQLException {
        return null;
    }

    @Override
    public Boolean updateById(${table.clsName} ${table.paramName}) throws SQLException {
        int i = mapper.updateByPrimaryKey(${table.paramName});
        if (i > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean updateByExample(${table.clsName} ${table.paramName}, ${table.clsName}Example example) throws SQLException {
        return null;
    }
}
