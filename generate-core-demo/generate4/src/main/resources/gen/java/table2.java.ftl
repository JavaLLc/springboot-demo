
/**
 * <p>
*   ${table.common}
* </p>
 *
 * @version 1.0
 * @author LLC
 * @since 2019-05-28
 */

@Getter
@Setter
@ToString
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "gf_ac_order_sale")
@ApiModel("${table.common}")
public class ${table.clsName}Entity implements Serializable{

private static final long serialVersionUID = -1L;


<#list columnList as c>
<#if c.javaType == 'Long'>
    /**
     * ${c.columnComment}
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "${c.columnName}", nullable = false, unique = true, length = 20)
    @ApiModelProperty(value = "${c.columnComment}")
    private ${c.javaType} ${c.javaFieldId};
</#if>
<#if c.javaType == 'String'>
    /**
     * ${c.columnComment}
     */
    @Column(name = "${c.columnName}", length = ${c.maxLength})
    @ApiModelProperty(value = "${c.columnComment}")
    private ${c.javaType} ${c.javaFieldId};
</#if>
<#if c.javaType == 'Date'>
    /**
     * ${c.columnComment}
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "${c.columnName}")
    @ApiModelProperty(value = "${c.columnComment}")
    private ${c.javaType} ${c.javaFieldId};
</#if>
<#if c.javaType == 'Integer'>
     /**
     * ${c.columnComment}
     */
    @Column(name = "${c.columnName}")
    @ApiModelProperty(value = "${c.columnComment}")
    private ${c.javaType} ${c.javaFieldId};
</#if>
<#if c.javaType == 'BigDecimal'>
    /**
     * ${c.columnComment}
     */
    @Column(name = "${c.columnName}", precision = 20, scale = 2)
    @ApiModelProperty(value = "${c.columnComment}")
    private ${c.javaType} ${c.javaFieldId};
</#if>
</#list>

}
