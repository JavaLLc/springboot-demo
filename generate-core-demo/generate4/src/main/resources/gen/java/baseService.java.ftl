package ${table.pkgName}.service;

import java.sql.SQLException;
import java.util.List;

public interface BaseService<T,R> {

/**
* 按主键删除
*
* @param id 主键ID
* @return ID
*/
Boolean deleteById(String id) throws SQLException;

/**
* 按条件删除
*
* @param r 自定义条件
* @return ID
*/
Boolean deleteByExample(R r) throws SQLException;

/**
* 添加数据
*
* @param t 封装对象
*/
Boolean insert(T t) throws SQLException;

/**
* 按主键查询
*
* @param id 主键ID
* @return 封装对象
*/
T selectById(String id) throws SQLException;

/**
* 按条件查询
*
* @param r 自定义条件
* @return 封装对象
*/
List<T> selectByExample(R r) throws SQLException;

    /**
    * 按主键更新值不为null的字段
    *
    * @param t 封装对象
    * @return ID
    */
    Boolean updateById(T t) throws SQLException;

    /**
    * 按条件更新值不为null的字段
    *
    * @param t 封装对象
    * @param r 自定义条件
    * @return ID
    */
    Boolean updateByExample(T t, R r) throws SQLException;

    }
