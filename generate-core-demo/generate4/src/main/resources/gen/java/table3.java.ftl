
/**
 * <p>
*   ${table.common}
* </p>
 *
 * @version 1.0
 * @author LLC
 * @since 2019-05-31
 */

@Getter
@Setter
@ToString
@ApiModel("${table.common}")
public class ${table.clsName}Entity implements Serializable{

private static final long serialVersionUID = -1L;


<#list columnList as c>
<#if c.javaType == 'Long'>
    /**
     * ${c.columnComment}
     */
    @ApiModelProperty(value = "${c.columnComment}")
    private ${c.javaType} ${c.javaFieldId};
</#if>
<#if c.javaType == 'String'>
    /**
     * ${c.columnComment}
     */
    @ApiModelProperty(value = "${c.columnComment}")
    private ${c.javaType} ${c.javaFieldId};
</#if>
<#if c.javaType == 'Date'>
    /**
     * ${c.columnComment}
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "${c.columnComment}")
    private ${c.javaType} ${c.javaFieldId};
</#if>
<#if c.javaType == 'Integer'>
     /**
     * ${c.columnComment}
     */
    @ApiModelProperty(value = "${c.columnComment}")
    private ${c.javaType} ${c.javaFieldId};
</#if>
<#if c.javaType == 'BigDecimal'>
    /**
     * ${c.columnComment}
     */
    @ApiModelProperty(value = "${c.columnComment}")
    private ${c.javaType} ${c.javaFieldId};
</#if>
</#list>

}
