package ${table.pkgName}.service;

import ${table.pkgName}.pojo.${table.clsName};
import ${table.pkgName}.pojo.${table.clsName}Example;

public interface ${table.clsName}Service extends BaseService<${table.clsName}, ${table.clsName}Example> {

}
