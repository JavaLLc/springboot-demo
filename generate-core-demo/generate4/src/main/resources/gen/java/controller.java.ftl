package ${table.pkgName}.controller;

import ${table.pkgName}.pojo.${table.clsName};
import ${table.pkgName}.service.${table.clsName}Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by Administrator on 2017/10/23.
*/

@RestController
@RequestMapping("/${table.paramName}")
public class ${table.clsName}Controller {

@Autowired
private ${table.clsName}Service ${table.paramName}Service;

/**
* 条件查找
*
* @return
*/
@PostMapping("/findList")
public String findList() {
return null;
}

/**
* 通过ID查找一条数据
*
* @param id 主键id
* @return 结果信息
*/
@GetMapping("/findOne")
public ${table.clsName} findOne(String id) {
return null;
}

/**
* 删除
*
* @param id 主键id
* @return 结果信息
*/
@GetMapping("/delete")
public String delete(String id) {
return null;
}


/**
* 保存
*
* @param ${table.paramName} 封装对象
* @return
*/
@PostMapping("/save")
public String save(${table.clsName} ${table.paramName}) {
return null;
}

/**
* 修改
*
* @param ${table.paramName} 信息封装对象
* @return
*/
@PostMapping("/update")
public String updateFocus(${table.clsName} ${table.paramName}) {
return null;
}

}
