<?xml version = "1.0" encoding = "UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD com.example.Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace = "${table.pkgName}.dao.${table.clsName}Dao">
    <sql id="${table.lowOneclsName}Columns">
<#assign columnField>
    <#list columnList as c>
        a.${c.columnName} AS "${c.javaFieldId}",
    </#list>
    </#assign>
${columnField?substring(0, columnField?last_index_of(","))}
    </sql>

    <sql id="${table.lowOneclsName}Joins">
    </sql>

    <select id="get" resultType="${table.clsName}">
        SELECT
        <include refid="${table.lowOneclsName}Columns"/>
        FROM ${table.tableName} a
        <include refid="${table.lowOneclsName}Joins"/>
        WHERE a.id = ${"#"}{id}
    </select>

    <select id="findList" resultType="${table.clsName}">
        SELECT
        <include refid="${table.lowOneclsName}Columns"/>
        FROM ${table.tableName} a
        <include refid="${table.lowOneclsName}Joins"/>
        <where>
            1 = 1
<#list columnList as c>
    <#if c.where >
        <if test="${c.javaFieldId} != null and ${c.javaFieldId} != ''">
        <#if c.whereType == '='>
                AND a.${c.columnName} = ${"#"}{${c.javaFieldId}}
        </#if>
        <#if c.whereType == 'Like'>
                AND a.${c.columnName} LIKE CONCAT('%',${"#"}{${c.javaFieldId}},'%')
        </#if>
        </if>
    </#if>
</#list>
        </where>
    </select>

    <select id="findAllList" resultType="${table.clsName}">
        SELECT
        <include refid="${table.lowOneclsName}Columns"/>
        FROM ${table.tableName} a
        <include refid="${table.lowOneclsName}Joins"/>
    </select>

    <insert id="insert">
        INSERT INTO ${table.tableName}(
<#assign insertField>
        <#list columnList as c>
            <#if c.isInsert >
            ${c.columnName},
            </#if>
        </#list>
    </#assign>
${insertField?substring(0, insertField?last_index_of(","))}
        ) VALUES (
<#assign insertField>
        <#list columnList as c>
            <#if c.isInsert >
            ${"#"}{${c.javaName}},
            </#if>
        </#list>
    </#assign>
${insertField?substring(0, insertField?last_index_of(","))}
        )
    </insert>

    <update id="update">
        UPDATE ${table.tableName} SET
    <#assign insertField>
        <#list columnList as c>
            <#if c.isUpdate >
        ${c.columnName} = ${"#"}{${c.javaName}},
            </#if>
        </#list>
    </#assign>
${insertField?substring(0, insertField?last_index_of(","))}
        WHERE id = ${"#"}{id}
    </update>

    <update id="dynamicUpdate">
        UPDATE ${table.tableName} SET
        <#list columnList as c>
            <#if c.javaName!='id'&&c.javaName!='createBy.id'&&c.javaName!='createDate'&&c.javaName!='updateBy.id'&&c.javaName!='updateDate'&&c.javaName!='remarks' >
        <if test="${c.javaName} != null and ${c.javaName} != ''">
        ${c.columnName} = ${"#"}{${c.javaName}},
        </if>
            </#if>
        </#list>
        update_by = ${"#"}{updateBy.id},
        update_date = ${"#"}{updateDate}
        WHERE id = ${"#"}{id}
    </update>

    <delete id="delete">
        DELETE FROM ${table.tableName}
        WHERE id = ${"#"}{id}
    </delete>
</mapper>