package com.llc.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created by dvnuo on 2017/2/15.
 */
@Mapper
@SuppressWarnings("rawtypes")
public interface GenTableDao {

	List<Map> findTable(Map<String,String> map);

    List<Map> findColumn(@Param("tableSchema") String tableSchema, @Param("tableName") String tableName);

}
