package com.llc.dao;

import com.llc.util.StringUtils;

/**
 * Created by dvnuo on 2017/2/16.
 */
public class Table {
    private String tableName;
    private String pkgName;
    private String clsName;
    private String common;
    private String path;

    public Table(String tableName, String pkgName, String clsName, String common, String path) {
        this.tableName = tableName;
        this.pkgName = pkgName;
        this.clsName = clsName.trim();
        this.common = common;
        this.path = path;
    }

    public String getBaseServicePath() {
        String ph = StringUtils.replace(pkgName, ".", "/");
        return path + "/" + ph + "/service/" + "BaseService.java";
    }

    public String getServicePath() {
        String ph = StringUtils.replace(pkgName, ".", "/");
        return path + "/" + ph + "/service/" + clsName + "Service.java";
    }

    public String getServiceImplPath() {
        String ph = StringUtils.replace(pkgName, ".", "/");
        return path + "/" + ph + "/service/impl/" + clsName + "ServiceImpl.java";
    }

    public String getControllerPath() {
        String ph = StringUtils.replace(pkgName, ".", "/");
        return path + "/" + ph + "/controller/" + clsName + "Controller.java";
    }


    public String getRequestName() {
        return getProjectName().toLowerCase();
    }

    public String getParamName() {
        return this.clsName.substring(0, 1).toLowerCase() + clsName.substring(1);
    }

    public String getProjectName() {
        String[] pn = tableName.split("_");
        return pn[pn.length - 1];
    }

    public String getLowOneclsName() {
        return this.clsName.substring(0, 1).toLowerCase() + this.clsName.substring(1);
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getPkgName() {
        return pkgName;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }

    public String getClsName() {
        return clsName;
    }

    public void setClsName(String clsName) {
        this.clsName = clsName;
    }

    public String getCommon() {
        return common;
    }

    public void setCommon(String common) {
        this.common = common;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


}
