package com.llc.dao;

/**
 * Created by dvnuo on 2017/2/16.
 */
public class Column {

    private String columnName;
    private String columnComment;
    private String maxLength;
    private String javaType;
    private String javaName;
    private boolean nullAble;
    private boolean insert;
    private boolean update;
    private boolean where;
    private String whereType;
    private String dataType;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public void setColumnComment(String columnComment) {
        this.columnComment = columnComment;
    }

    public String getMaxLength() {
        return this.maxLength;
    }

    public void setMaxLength(String maxLength) {
        this.maxLength = maxLength;
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public String getJavaName() {
        return javaName;
    }

    public void setJavaName(String javaName) {
        this.javaName = javaName;
    }

    public void setNullAble(boolean nullAble) {
        this.nullAble = nullAble;
    }

    public boolean getIsInsert() {
        return insert;
    }

    public void setInsert(boolean insert) {
        this.insert = insert;
    }

    public boolean getIsUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

    public boolean isWhere() {
        return where;
    }

    public void setWhere(boolean where) {
        this.where = where;
    }

    public String getWhereType() {
        return whereType;
    }

    public void setWhereType(String whereType) {
        this.whereType = whereType;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getVarColumnName(){
        return this.javaName;
    }

    public String getJavaFieldId(){
        return this.javaName;
    }

    public String getColumnType(){
        return this.javaType;
    }

    public String getNullAble(){
        return nullAble?"YES":"NO";
    }

    public String getStandardColumnName(){
        return this.javaName.substring(0, 1).toUpperCase() + this.javaName.substring(1);
    }

}
