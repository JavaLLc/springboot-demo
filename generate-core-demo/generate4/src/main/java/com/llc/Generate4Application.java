package com.llc;

import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Generate4Application implements ExitCodeGenerator {

	public static void main(String[] args) {
		SpringApplication.run(Generate4Application.class, args);
	}

	/**
	 * 生命周期结束时,会执行ExitCodeGenerator接口的getExitCode()
	 * @return
	 */
	@Override
	public int getExitCode() {
		System.out.println("GenCode is over!");
		return 0;
	}
}
