package com.llc.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * Freemarker简化使用工具类.
 */
public class FreemarkerUtils {

    private static Configuration configuration;

    static{
        configuration = new Configuration(Configuration.VERSION_2_3_20);
        configuration.setDefaultEncoding("UTF-8");
        configuration.setClassForTemplateLoading(FreemarkerUtils.class, "/gen/");
    }

    /**
     * 生成文件
     * @param file
     * @param ftl
     * @param data
     */
    public static void genFileByFtl(String file, String ftl, Map<String, Object> data){
        Writer writer = null;
        try{
            Template template = configuration.getTemplate(ftl);

            File target = new File(file);
            if(!target.getParentFile().exists()){
                target.getParentFile().mkdirs();
            }
            if(target.exists()){
                target.delete();
            }
            writer = new OutputStreamWriter(new FileOutputStream(target), "utf-8");
            template.process(data, writer);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(writer!=null){
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 根据模板和数据生成内容
     * @param ftl
     * @param data
     * @return
     */
    public static String genByFtl(String ftl, Map<String, Object> data){
        StringWriter writer = new StringWriter();

        try {
            Template template = configuration.getTemplate(ftl);
            template.process(data, writer);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }

        return writer.toString();
    }

}
