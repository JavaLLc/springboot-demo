package com.llc;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.llc.dao.Column;
import com.llc.dao.GenTableDao;
import com.llc.dao.Table;
import com.llc.util.FreemarkerUtils;
import com.llc.util.StringUtils;

/**
 * Created by dvnuo on 2017/2/15.
 */
@RestController
@SuppressWarnings({"unchecked","rawtypes"})
public class TableController {

    private  int count = 1;
    @Autowired
    GenTableDao genTableDao;

    private static  List<Map> baselist;

    @Value("${mysql.table_schema}")
    private String tableSchema;

    @RequestMapping("/findTable")
    public List<Map> findTable() {
        System.out.println("需要查询的库" + tableSchema);
        Map<String,String> map = new HashMap();
        map.put("tableSchema",tableSchema);
        return genTableDao.findTable(map);
    }

    @RequestMapping("/findColumn")
    public List<Map> findColumn(@RequestParam String tableName, @RequestParam String tableSchema) {
        List<Map> list = genTableDao.findColumn(tableSchema, tableName);
        for (Map<String, String> m : list) {
            String col = m.get("COLUMN_NAME");
            col = StringUtils.toCamelCase(col);
            m.put("JAVA_NAME", col);
        }

        baselist = list;

        System.out.println(baselist);
        return list;
    }

	@RequestMapping("/genIt/{tableName}/{pkgName}/{clsName}/{common}")
    public String genIt(@PathVariable String tableName, @PathVariable String pkgName, @PathVariable String clsName, @PathVariable String common, @RequestHeader("pa") String path, @RequestBody List<Column> columnList) throws UnsupportedEncodingException {
        common = URLDecoder.decode(common, "UTF-8");
        Table table = new Table(tableName, pkgName, clsName, common, path);

        Map map = new HashMap();
        map.put("table", table);
        map.put("columnList", columnList);

        System.out.println(baselist);

        if(baselist!=null){
            for(int i = 0;i<columnList.size();i++){
                if(columnList.get(i)!=null&&columnList.get(i).getMaxLength()!=null){
                    Map map1 = baselist.get(i);
                    Object max_length = map1.get("MAX_LENGTH");
                    if(max_length!=null){
                        columnList.get(i).setMaxLength(max_length.toString());
                    }

                }

            }
        }

        columnList.stream().forEach(s->{
            System.out.println(s.getMaxLength());
        });

        //生成model  table.getControllerPath():生成存放的路径+包名+类名
        if(count==1){
            //FreemarkerUtils.genFileByFtl(table.getBaseServicePath(), "java/baseService.java.ftl", map);
            count++;
            System.out.print(count+"******************************");
        }
        //FreemarkerUtils.genFileByFtl(table.getControllerPath(), "java/controller.java.ftl", map);
        //FreemarkerUtils.genFileByFtl(table.getServicePath(), "java/service.java.ftl", map);
        //FreemarkerUtils.genFileByFtl(table.getServiceImplPath(), "mapper/mapper.xml.ftl", map);
        FreemarkerUtils.genFileByFtl(table.getServicePath(), "java/table.java.ftl", map);
        //FreemarkerUtils.genFileByFtl(table.getServicePath(), "java/demo.java.ftl", map);

        return "OK";
    }

}
