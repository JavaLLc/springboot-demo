package com.llc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import brave.sampler.Sampler;

@EnableEurekaClient
@SpringBootApplication
@RestController
public class ServerZipkinApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerZipkinApplication.class, args);
	}

	/**
	 * 负载均衡
	 * @return
	 */
	@LoadBalanced
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Autowired
	private RestTemplate restTemplate;

	/**
	 * 调用另一个服务的接口
	 * @return
	 */
	@RequestMapping("/hi")
	public String callHome() {
		return restTemplate.getForObject("http://SERVICE-HI2/hi2", String.class);
	}

	/**
	 * 对外暴露的接口
	 * @return
	 */
	@RequestMapping("/hi1")
	public String info() {
		return "i'm service-hi1";
	}

	/**
	 * 采样器
	 * @return
	 */
	@Bean
	public Sampler defaultSampler() {
		return Sampler.ALWAYS_SAMPLE;
	}
}
