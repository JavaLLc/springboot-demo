###springcloud实例2
#####1.链路追踪
```
server-zipkin
server-zipkin2
下载zipkin-server jar包,下载地址:https://dl.bintray.com/openzipkin/maven/io/zipkin/java/zipkin-server/
或者链接: https://pan.baidu.com/s/1w614Z8gJXHtqLUB6dKWOpQ 密码: 26pf
启动:java -jar zipkin-server-2.10.1-exec.jar  9411为Zipkin默认端口号
启动:server-zipkin和server-zipkin2
访问:http://localhost:9411/ 会出现zipkin界面,点击Dependencies,可以发现服务的依赖关系,点击find traces,可以看到具体服务相互调用的数据
访问:http://localhost:8988/hi i'm service-hi2
访问:http://localhost:8989/hi i'm service-hi1

注:引入ribbon依赖才能通过服务名调用
```
#####2.Eureka集群
```
eureka-server1
eureka-server2
eureka-server3
eureka-client2
本地测试环境需要修改hosts文件
启动eureka-server1,eureka-server2,eureka-server3,eureka-client2
访问:http://localhost:8761/,http://localhost:8762/,http://localhost:8763/可以看到他们互相注册,
当停止了其中一个服务,在其他注册中心还可以看到SERVICE-HI服务
```
#####3.Eureka-Spring Security Basic认证
```
eureka-security-server
eureka-security-client
依次启动,访问http://localhost:8761/
输入账号密码
注意:
服务端在入口类需要添加安全配置,不做csrf校验,
且客户端配置的注册中心地址需要携带账号密码
否则会出现一下错误:
Cannot execute request on any known server
```
#####4.断路器监控
```
hystrix-dashboard
启动实例1的eureka-server,再启动hystrix-dashboard,
访问:http://localhost:8766/actuator/hystrix.stream 可以看到数据
【如果404的话,在启动类配置bean,具体看此例的启动类】
访问:http://localhost:8766/hystrix  》》 可以看到Hystrix Dashboard页面
在界面依次输入：http://localhost:8766/actuator/hystrix.stream 、2000 、标题 
点确定。
在另一个窗口输入： http://localhost:8762/hi?name=forezp
重新刷新monitor页面，可以看到良好的图形化界面
```
#####5.断路器聚合监控
```
hystrix-turbine
启动实例1的eureka-server,再启动hystrix-dashboard,
hystrix-dashboard修改端口为8767再启动一个实例,启动hystrix-turbine
【界面1】访问:http://localhost:8777/turbine.stream 可以看到有数据
【界面2】访问:http://localhost:8766/hystrix 或者 http://localhost:8766/hystrix
在界面依次输入：http://localhost:8777/turbine.stream 、2000 、标题 
点确定。
在另一个窗口输入： http://localhost:8766/hi
回到【界面2】可以看到有图形了,且Thread Pools下有1个,
在另一个窗口输入： http://localhost:8767/hi
回到【界面2】可以看到有图形了,且Thread Pools下有2个
```




