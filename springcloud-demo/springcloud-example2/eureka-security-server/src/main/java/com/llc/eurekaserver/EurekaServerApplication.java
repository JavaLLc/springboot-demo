package com.llc.eurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableEurekaServer
@SpringBootApplication
public class EurekaServerApplication extends WebSecurityConfigurerAdapter {

	public static void main(String[] args) {
		SpringApplication.run(EurekaServerApplication.class, args);
	}

	/**
	 * http.csrf().ignoringAntMatchers("/**"):Spring Security 默认开启了所有 CSRF 攻击防御，需要禁用 /eureka 的防御
	 * 重写之前自动配置跳到了登录页面
	 * 重写之后回到之前的 basic auth 认证方式
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// 页面不做csrf校验
		http.csrf().ignoringAntMatchers("/**")
				// 请求授权-回到之前的 basic auth 认证方式
				.and().authorizeRequests().anyRequest().authenticated().and().httpBasic();
	}

}
