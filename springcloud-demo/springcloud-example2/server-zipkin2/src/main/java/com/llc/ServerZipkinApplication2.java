package com.llc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import brave.sampler.Sampler;

@EnableEurekaClient
@SpringBootApplication
@RestController
public class ServerZipkinApplication2 {

	public static void main(String[] args) {
		SpringApplication.run(ServerZipkinApplication2.class, args);
	}

	@LoadBalanced
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Autowired
	private RestTemplate restTemplate;

	@RequestMapping("/hi")
	public String callHome() {
		return restTemplate.getForObject("http://SERVICE-HI1/hi1", String.class);
	}

	/**
	 * 对外暴露的接口
	 * @return
	 */
	@RequestMapping("/hi2")
	public String info() {
		return "i'm service-hi2";
	}

	@Bean
	public Sampler defaultSampler() {
		return Sampler.ALWAYS_SAMPLE;
	}
}
