###集群
#####1.配置
```
#主节点master:
server:
  port: 8761
eureka:
  instance:
    hostname: register-master                           #主机名，主机名不能为IP或者重名，windows/Linux 测试环境配置hosts文件
    leaseRenewalIntervalInSeconds: 10                   #租期更新时间间隔（默认30秒）
    leaseExpirationDurationInSeconds: 30                #租期到期时间（默认90秒）
  client:
    register-with-eureka: true                           # 表示是否注册自身到eureka服务器,单实例设为false
    fetch-registry: true                                # 表示是否注册自身到eureka服务器,单实例设为false
    serviceUrl:
      defaultZone: http://register-salve1:8762/eureka/,http://register-salve2:8763/eureka/   #向注册集群的地址，如有多个“,”隔开
  server:
    enable-self-preservation: false                 #（设为false，关闭自我保护）目的是实现有宕机的服务自动剔除，以为有自我保护时，自我保护功能会等待修复宕机的服务
    eviction-interval-timer-in-ms: 4000             #清理间隔（单位毫秒，默认是60*1000）PS清除宕机的服务

spring:
  application:
    name: register-center


子节点类似。
```

#####2.修改hosts
```
本地测试环境需要修改hosts文件,
本例按照windows,在C:\Windows\System32\drivers\etc下
编辑hosts文件,追加
127.0.0.1 register-master
127.0.0.1 register-salve1
127.0.0.1 register-salve2
```

