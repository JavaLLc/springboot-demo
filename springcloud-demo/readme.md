###springcloud(参考:https://blog.csdn.net/forezp/article/details/70148833/)
```
1.实例1:
服务注册与发现: eureka-server
配置服务器: config-server
服务提供者
服务消费者
路由网关: service-zuul等

2.实例2:
链路追踪
Eureka集群
Eureka-Spring Security Basic认证
断路器监控
断路器聚合监控等

3.实例3:
Spring Cloud Gateway

4.实例4:替换eureka的其他注册中心
```
