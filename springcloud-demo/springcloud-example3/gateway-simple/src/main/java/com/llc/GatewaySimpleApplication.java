package com.llc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@SpringBootApplication
@RestController
public class GatewaySimpleApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewaySimpleApplication.class, args);
	}

	@Bean
	public RouteLocator myRoutes(RouteLocatorBuilder builder) {
		return builder.routes()
				.route(p -> p.path("/hello2")
						.filters(f -> f.addRequestParameter("name", "World")
							//	.hystrix(c -> c.setName("fallbackcmd").setFallbackUri("forward:/fallback"))//不生效,还没找到具体原因
						).uri("http://localhost:8081/gateway2"))
				.build();
	}

	/**
	 * Mono是一个Reactive stream，对外输出一个“fallback”字符串
	 * @return
	 */
	@RequestMapping("/fallback")
	public Mono<String> fallback() {
		return Mono.just("fallback");
	}

}
