package com.llc;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author fangzhipeng
 * create 2018-12-05
 * KeyResolver需要实现resolve方法,如根据Hostname进行限流,需要用hostAddress去判断。实现完KeyResolver后,需要将这个类的Bean注册到Ioc容器中。
 **/
public class HostAddrKeyResolver implements KeyResolver {

	@Override
	public Mono<String> resolve(ServerWebExchange exchange) {
		return Mono.just(exchange.getRequest().getRemoteAddress().getAddress().getHostAddress());
	}

}