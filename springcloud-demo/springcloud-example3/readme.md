###springcloud实例3
#####springcloud gateway
#####1.简介
```
Spring Cloud Gateway是Spring Cloud官方推出的第二代网关框架，取代Zuul网关。Spring Cloud Gateway几乎包含了zuul的所有功能
网关作为流量的，在微服务系统中有着非常作用，网关常见的功能有路由转发、权限校验、限流控制等作用

协议转换，路由转发
流量聚合，对流量进行监控，日志输出
作为整个系统的前端工程，对流量进行控制，有限流的作用
作为系统的前端边界，外部流量只能通过网关才能访问系统
可以在网关层做权限的判断
可以在网关层做缓存

注意:Spring Cloud Gateway 是使用 netty+webflux ,不需要再引入spring-boot-starter-web
```

#####实例
```
1.gateway-simple,gateway-web
启动gateway-simple,gateway-web
访问:http://localhost:9090/hello?name=zs  》》 浏览器显示:zs
访问:http://localhost:9090/hello2  》》 浏览器显示:World
停掉gateway-web,访问http://localhost:9090/hello?name=zs 》》 浏览器显示:fallback

2.gateway-current-limiting
启动gateway-web,gateway-current-limiting
启动测试工具JMeter(安装略,使用参考:https://www.cnblogs.com/leeboke/p/5227320.html,
说明参考:https://blog.csdn.net/u013258415/article/details/78320605),
线程数设置10模拟10个用户,Ramp-up Period(in Seconds)：表示每个用户启动的延迟时间 默认1就行,循环次数为10,
这样就会发送100个请求,发现只有4个请求成功,令牌总量3个和填充的1个
```
#####predicate简介
```
Predicate来自于java8的接口。Predicate 接受一个输入参数，返回一个布尔值结果。
该接口包含多种默认方法来将Predicate组合成其他复杂的逻辑（比如：与，或，非）。
可以用于接口请求参数校验、判断新老数据是否有变化需要进行更新操作。add–与、or–或、negate–非。
builder.routes().route(p->..) 里面的p就是指predicate
```
#####filters
```
- RewritePath=/foo/(?<segment>.*), /$\{segment}
重写路径,访问的是/foo/hello 将重写为/hello
即<segment>会被$\{segment}替换
- RewritePath=/api-a/hello, /hello
/api-a/hello会被/hello替换
```
#####限流(参考:https://blog.csdn.net/forezp/article/details/85081162)
```
在Spring Cloud Gateway中，有Filter过滤器，因此可以在“pre”类型的Filter中自行实现上述三种过滤器。但是限流作为网关最基本的功能，
Spring Cloud Gateway官方就提供了RequestRateLimiterGatewayFilterFactory这个类，适用Redis和lua脚本实现了令牌桶的方式。
```

#####常见的限流算法
```
1.计数器算法
一般会限制一秒钟的能够通过的请求数，比如限流qps为100，实现思路:从第一个请求进来开始计时，在接下去的1s内，每来一个请求，就把计数加1，如果累加的数字达到了100，
那么后续的请求就会被全部拒绝。等到1s结束后，把计数恢复成0，重新开始计数。
具体的实现：对于每次服务调用，可以通过AtomicLong#incrementAndGet()方法来给计数器加1并返回最新值，通过这个最新值和阈值进行比较。
弊端：如果在单位时间1s内的前10ms，已经通过了100个请求，那后面的990ms，把请求拒绝，这种现象称为“突刺现象”

2.漏桶算法
为了消除"突刺现象"，可以采用漏桶算法实现限流，漏桶算法这个名字就很形象，算法内部有一个容器，类似生活用到的漏斗，当请求进来时，相当于水倒入漏斗，然后从下端小口慢慢匀速的流出。
不管上面流量多大，下面流出的速度始终保持不变。不管服务调用方多么不稳定，通过漏桶算法进行限流，每10毫秒处理一次请求。因为处理的速度是固定的，请求进来的速度是未知的，
可能突然进来很多请求，没来得及处理的请求就先放在桶里，既然是个桶，肯定是有容量上限，如果桶满了，那么新进来的请求就丢弃。
在算法实现方面，可以准备一个队列，用来保存请求，另外通过一个线程池（ScheduledExecutorService）来定期从队列中获取请求并执行，可以一次性获取多个并发执行。
弊端：无法应对短时间的突发流量

3.令牌桶算法
从某种意义上讲，令牌桶算法是对漏桶算法的一种改进，桶算法能够限制请求调用的速率，而令牌桶算法能够在限制调用的平均速率的同时还允许一定程度的突发调用。
在令牌桶算法中，存在一个桶，用来存放固定数量的令牌。算法中存在一种机制，以一定的速率往桶中放令牌。每次请求调用需要先获取令牌，只有拿到令牌，才有机会继续执行，
否则选择选择等待可用的令牌、或者直接拒绝。放令牌这个动作是持续不断的进行，如果桶中令牌数达到上限，就丢弃令牌，所以就存在这种情况，桶中一直有大量的可用令牌，
这时进来的请求就可以直接拿到令牌执行，比如设置qps为100，那么限流器初始化完成一秒后，桶中就已经有100个令牌了，这时服务还没完全启动好，等启动完成对外提供服务时，该限流器可以抵挡瞬时的100个请求。只有桶中没有令牌时，请求才会进行等待，最后相当于以一定的速率执行。
实现思路：可以准备一个队列，用来保存令牌，另外通过一个线程池定期生成令牌放到队列中，每来一个请求，就从队列中获取一个令牌，并继续执行。
```





