package com.llc.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GatewayController {

	@RequestMapping("/gateway")
	public String gatewayDemo(String name) {
		System.out.println("------------gateway-------------");
		return name;
	}

	@RequestMapping("/gateway2")
	public String gatewayDemo2(String name) {
		System.out.println("------------gateway2-------------");
		return name;
	}
}
