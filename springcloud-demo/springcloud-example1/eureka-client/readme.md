###服务提供者 (eureka client)
```
当client向server注册时，它会提供一些元数据，例如主机和端口，URL，主页等。Eureka server
从每个client实例接收心跳消息。 如果心跳超时，则通常将该实例从注册server中删除。
```

#####关于配置
```
需要指明spring.application.name,这个很重要，
这在以后的服务与服务之间相互调用一般都是根据这个name
```


#####关于注解:
```
@EnableEurekaClient 表明自己是一个eurekaclient
-------------------
注:
spring cloud中discovery service有许多种实现（eureka、consul、zookeeper等等），
@EnableDiscoveryClient基于spring-cloud-commons, @EnableEurekaClient基于spring-cloud-netflix。
其实用更简单的话来说，就是如果选用的注册中心是eureka，那么就推荐@EnableEurekaClient，
如果是其他的注册中心，那么推荐使用@EnableDiscoveryClient
```


#####启动:
```
http://localhost:8762/hi?name=llc
浏览器:hi llc ,i am from port:8762
```
