###springcloud实例1（配置详情:https://www.cnblogs.com/zhixiang-org-cn/p/9846326.html）
#####1.服务注册与发现: eureka-server
```
依赖:(spring-cloud-starter-netflix-eureka-server)
```
#####2.Eureka客户端-服务提供者: eureka-client
```
先启动1(eureka-server),然后启动2(eureka-client)
访问http://localhost:8761/即可看到注册的服务
```
#####3.server-feign(自带断路器)
```
1(eureka-server),2(eureka-client),3(server-feign)依次启动
2(eureka-client)修改端口为8763再启动一个实例
访问http://localhost:8761/即可看到注册的3个服务
多次访问http://localhost:8765/hi?name=hello,浏览器交替显示
hi hellofrom8765 ,i am from port:8762
hi hellofrom8765 ,i am from port:8763

当关闭2(eureka-client)时,再次访问http://localhost:8765/hi?name=hello,此时服务不可用,断路器Hystrix被打开
浏览器显示:
sorry hellofrom8765
```
#####4.server-ribbon(集成断路器)
```
启动顺序和第三步的server-feign一样,启动4(server-ribbon)
多次访问http://localhost:8764/hi?name=hello,浏览器交替显示
hi hello ,i am from port:8762
hi hello ,i am from port:8763

当关闭2(eureka-client)时,再次访问http://localhost:8764/hi?name=hello,此时服务不可用,断路器Hystrix被打开
浏览器显示:
hi,hello,sorry,error!
```

#####5.配置服务器: config-server
```
启动环境:
config自动更新需要mq

依赖:(
配置服务:spring-cloud-config-server
MQ:spring-cloud-starter-bus-amqp
向注册中心注册:spring-cloud-starter-netflix-eureka-client
)
```

#####6.配置客户端(集成消息总线): config-client
```
和服务提供者(集成在一起)
（
    向注册中心注册: spring-cloud-starter-netflix-eureka-client
    配置客户端: spring-cloud-starter-config
    web: spring-boot-starter-web
    MQ: spring-cloud-starter-bus-amqp
    actuator: spring-boot-starter-actuator
    ）
    
依旧先启动1(eureka-server),启动5(config-server),启动6(config-client)
访问:http://localhost:8881/hi  》》 浏览器显示:hello world2
此时,修改配置文件为hello world7
postman访问http://localhost:8888/actuator/bus-refresh(post请求,headers-> Content-Type:application/json)
没有返回任何内容,也没有报错,表示成功刷新配置了。
这时再次访问http://localhost:8881/hi 》》 浏览器显示:hello world7
```

#####7.路由网关: service-zuul
```
（
    向注册中心注册: spring-cloud-starter-netflix-eureka-client
    路由: spring-cloud-starter-netflix-zuul
）
1(eureka-server),2(eureka-client),3(server-feign),4(server-ribbon),service-zuul(service-zuul)依次启动
(过滤器)
访问:http://localhost:8769/api-a/hi?name=ribbon  》》 浏览器显示:token is empty
(路由-过滤分发)
访问:http://localhost:8769/api-a/hi?name=ribbon&token=22  》》 浏览器显示:hi ribbon ,i am from port:8763
访问:http://localhost:8769/api-b/hi?name=ribbon&token=22  》》 浏览器显示:hi ribbonfrom8765 ,i am from port:8763
```
