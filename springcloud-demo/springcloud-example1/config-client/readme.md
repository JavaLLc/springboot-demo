###配置客户端
#####Spring Cloud Bus消息总线:
```
将分布式的节点用轻量的消息代理连接起来。它可以用于广播配置文件的更改或者服务之间的通讯，也可以用于监控。
本例子是用Spring Cloud Bus实现通知微服务架构的配置文件的更改

/actuator/bus-refresh接口可以指定服务，即使用"destination"参数，
比如 “/actuator/bus-refresh?destination=customers:**” 即刷新服务名为customers的所有服务。
```


#####关于配置:
```
server.port= 0 即随机端口
server.port=${random.int[10000,19999]}1 指定范围随机端口

eureka.client.allow-redirects=true
显示服务器是否可以将客户机请求重定向到集群/备份服务器。
如果设置为false,则服务器将直接处理请求,
如果设置为true,这可能发送HTTP重定向到客户端,与新服务器的位置。

eureka.instance.leaseRenewalIntervalInSeconds=1
加快客户端连接到其他服务的过程。在生产中，最好坚持使用默认值，因为在服务器内部有一些计算，他们对续约做出假设

eureka.instance.leaseExpirationDurationInSeconds=2
指定时间内没有数据上报可能会被清理掉

eureka.instance.instance-id:
服务实体向eureka注册时，在eureka界面下显示注册名默认是“IP名:应用名:应用端口名”，
即${spring.cloud.client.ipAddress}:${spring.application.name}:${spring.application.instance_id:${server.port}}
注:
必须写在application.properties或application.yml中。
不能写在bootstrap.properties和bootstrap.yml中
```

#####注解:
```
@SpringBootApplication
@EnableDiscoveryClient
@RestController
@RefreshScope

注:开启refresh机制, 需要给加载变量的类上面加载@RefreshScope注解,动态刷新配置
------------------------------------------------

spring.cloud.config.profile
dev开发环境配置文件
test测试环境
pro正式环境
```
