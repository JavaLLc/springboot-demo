###服务的注册与发现Eureka(Finchley版本)
#####0.关于依赖:
```
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
        </dependency>
```

#####1.关于配置(相关解释参考:https://www.cnblogs.com/li3807/p/7282492.html)
```
fetch-registry: 检索服务选项，当设置为True(默认值)时，会进行服务检索,注册中心不负责检索服务
register-with-eureka: 服务注册中心也会将自己作为客户端来尝试注册自己,为true（默认）时自动生效
eureka.client.serviceUrl.defaultZone是一个默认的注册中心地址。配置该选项后，可以在服务中心进行注册。
eureka.server.enable-self-preservation: 关闭注册中心的保护机制，
        Eureka 会统计15分钟之内心跳失败的比例低于85%将会触发保护机制，
        不剔除服务提供者，如果关闭服务注册中心将不可用的实例正确剔除
---------------------
注:
所以一般情况下，当我们设置服务为注册中心时，
需要关闭eureka.client.fetch-registry与eureka.client.register-with-eureka，
在做注册中心集群的时候，register-with-eureka必须打开，因为需要进行相互注册，不然副本无法可用
---------------------

总结:
Eureka如果不做注册中心时，它就是一个客户端，向注册中心提供注册的实例，
每一个实例注册之后需要向注册中心发送心跳来检测服务的可用性，当需要调用服务的时候，
它们会自己从注册中心去检索对应的服务信息，为了保证高性能，Eureka并不是每次都会去注册中心查询服务，
Eureka Client注册到Eureka Server时，彼此会进行数据同步，
也就是说Eureka Client会缓存一份Eureka Server中的服务列表信息，
当本地服务不可用的时候才会去注册中心获取，这样可以较低服务端的请求压力，也提升了服务调用的时间
```


#####2.关于注解:
```
启动一个服务注册中心，只需要一个注解@EnableEurekaServer
eureka是一个高可用的组件，它没有后端缓存，每一个实例注册之后需要向注册中心发送心跳（因此可以在内存中完成），
必须要指定一个 server。
```