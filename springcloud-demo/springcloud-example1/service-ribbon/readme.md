###service-ribbon服务消费者(ribbon+restTemplate)
---
#####服务调用方式
```
服务与服务的通讯是基于http restful的。
Spring cloud有两种服务调用方式:
    1.ribbon+restTemplate
    2.feign
```

#####简介:
```
ribbon是一个负载均衡客户端，可以很好的控制http和tcp的一些行为。Feign默认集成了ribbon
启动多个服务提供者形成一个集群

注:idea下启动多个实例,参考:https://blog.csdn.net/forezp/article/details/76408139
eclipse启动多个实例,修改配置文件的端口号启动即可
```

#####注解:
```
通过@EnableDiscoveryClient向服务中心注册
并且向程序的ioc注入一个bean: restTemplate;
并通过@LoadBalanced注解表明这个restRemplate开启负载均衡的功能
-------------------

restTemplate:
写一个测试类HelloService，通过之前注入ioc容器的restTemplate来消费service-hi服务的“/hi”接口，
在这里我们直接用的程序名替代了具体的url地址，在ribbon中它会根据服务名来选择具体的服务实例，
根据服务实例在请求的时候会用具体的url替换掉服务名
```

#####启动:
```
开两个服务提供者
在浏览器上多次访问http://localhost:8764/hi?name=llc，浏览器交替显示：
```


#####异常:
```
ava.lang.NoSuchMethodError: org.springframework.boot.builder.SpringApplicationBuilder.
<init>([Ljava/lang/Object;)V
springboot版本冲突
把org.springframework.boot改成1.5.2.RELEASE
或者把spring-boot-starter-web去掉,因为2.1.2.RELEASE已经包含web
```


#####加入断路器hystrix
```
为了保证其高可用，单个服务通常会集群部署。由于网络原因或者自身的原因，服务并不能保证100%可用，
如果单个服务出现问题，调用这个服务就会出现线程阻塞，此时若有大量的请求涌入，
Servlet容器的线程资源会被消耗完毕，导致服务瘫痪。服务与服务之间的依赖性，故障会传播，
会对整个微服务系统造成灾难性的严重后果，这就是服务故障的“雪崩”效应。
Netflix开源了Hystrix组件，实现了断路器模式
```
