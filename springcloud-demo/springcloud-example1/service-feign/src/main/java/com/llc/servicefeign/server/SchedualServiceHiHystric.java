package com.llc.servicefeign.server;

import org.springframework.stereotype.Component;

//断路器回调类
@Component
public class SchedualServiceHiHystric implements SchedualServiceHi {
    @Override
    public String sayHiFromClientOne(String name) {
        return "sorry "+name;
    }
}
