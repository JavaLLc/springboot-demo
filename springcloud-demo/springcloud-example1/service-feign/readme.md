###service-feign服务消费者
#####简介:
```
Feign是一个声明式的伪Http客户端，它使得写Http客户端变得更简单。使用Feign，
只需要创建一个接口并注解。它具有可插拔的注解特性，可使用Feign 注解和JAX-RS注解。
Feign支持可插拔的编码器和解码器。【Feign默认集成了Ribbon，并和Eureka结合，默认实现了负载均衡的效果】

1.Feign 采用的是基于接口的注解
2.Feign 整合了ribbon，具有负载均衡的能力
3.整合了Hystrix，具有熔断的能力
```


#####注解:
```
@EnableEurekaClient
@EnableDiscoveryClient
@EnableFeignClients
加上@EnableFeignClients注解开启Feign的功能

定义一个feign接口，通过@ FeignClient（“服务名”），来指定调用哪个服务。
比如在代码中调用了service-hi服务的“/hi”：
@FeignClient(value = "service-hi")
public interface SchedualServiceHi {
    @RequestMapping(value = "/hi",method = RequestMethod.GET)
    String sayHiFromClientOne(@RequestParam(value = "name") String name);
}
----------------------------------------------

多次访问http://localhost:8765/hi?name=llc,浏览器交替显示
```

#####配置：
```
Feign是自带断路器的
默认没有打开。需要在配置文件中配置打开它
feign.hystrix.enabled=true
```
