###配置服务器
```
关于注解:
@EnableConfigServer 开启配置服务器的功能
@EnableDiscoveryClient 向注册中心注册
```
#####springcloud-config 自动更新配置
```
原理步骤:
    修改配置提交
  post请求127.0.0.1/8888/actuator/bus-refresh
    通知到配置服务器config-server,config-server到git获取最新配置发布消息到mq
    客户端config-client订阅消息,收到消息后到config-server更新配置

注意:也可以本地文件的形式测试springcloud-example1下新建文件夹config-file,此目录下新建配置文件config-client-dev.properties内容为:
foo=hello world ;配置spring.cloud.config.server.git.uri=${user.dir}/config-file;
spring.cloud.config.server.git.searchPaths注释掉。本例子为git形式

1.服务与发现
127.0.0.1/8671

2.config-server
127.0.0.1/8888

3.config-client
127.0.0.1/随机端口

4.更新配置
127.0.0.1/8888/actuator/bus-refresh
```

