###springcloud实例4
#####注册中心
#####1.Consul(参考:https://blog.csdn.net/love_zngy/article/details/82216696)
```
我的是windows版本,下载:略
启动:双击run.bat或者cmd:consul agent -dev        # -dev表示开发模式运行，另外还有-server表示服务模式运行
默认端口是8500,修改其他端口方法如下:
1.在打开consul.exe的根目录 ,在次目录下创建一个   basic.json的文本以及一个data命名的空文件夹
2.basic.json内容如下:(我这边修改的端口为9500)
{
"ports": {
        "server": 9300,
        "serf_lan": 9301,
        "serf_wan": 9302,
		  "http": 9500,
        "dns": 9600
  }
}
3.在consul.exe的根目录下cmd,输入命令:
consul agent -server -bootstrap-expect 1 -data-dir .\ -advertise 127.0.0.1 -client 0.0.0.0 -ui -config-dir .\
访问:http://localhost:9500/即可看到界面
```


