###TestNG(参考:https://www.yiibai.com/testng/basic-annotations.html)
#####1.简介
```
TestNG是一个测试框架，其灵感来自JUnit和NUnit，但引入了一些新的功能，使其功能更强大，使用更方便。
```
#####2.优点
```
注解
TestNG使用Java和面向对象的功能
支持综合类测试(例如，默认情况下，不用创建一个新的测试每个测试方法的类的实例)
独立的编译时测试代码和运行时配置/数据信息
灵活的运行时配置
主要介绍“测试组”。当编译测试，只要要求TestNG运行所有的“前端”的测试，或“快”，“慢”，“数据库”等
支持依赖测试方法，并行测试，负载测试，局部故障
灵活的插件API
支持多线程测试
```
#####3.安装
```
eclipse-help-Eclipse Marketplace-搜索testng-点击安装-安装完重启
右键new-other-找到testng目录
```
#####4.注解
```
@BeforeSuite 在该套件的所有测试都运行在注释的方法之前，仅运行一次。
@AfterSuite 在该套件的所有测试都运行在注释方法之后，仅运行一次。
@BeforeClass 在调用当前类的第一个测试方法之前运行，注释方法仅运行一次。
@AfterClass 在调用当前类的第一个测试方法之后运行，注释方法仅运行一次
@BeforeTest 注释的方法将在属于<test>标签内的类的所有测试方法运行之前运行。
@AfterTest 注释的方法将在属于<test>标签内的类的所有测试方法运行之后运行。
@BeforeGroups 配置方法将在之前运行组列表。 此方法保证在调用属于这些组中的任何一个的第一个测试方法之前不久运行。
@AfterGroups 此配置方法将在之后运行组列表。该方法保证在调用属于任何这些组的最后一个测试方法之后不久运行。
@BeforeMethod 注释方法将在每个测试方法之前运行。
@AfterMethod 注释方法将在每个测试方法之后运行。
@DataProvider 标记一种方法来提供测试方法的数据。 注释方法必须返回一个Object [] []，其中每个Object []可以被分配给测试方法的参数列表。 
要从该DataProvider接收数据的@Test方法需要使用与此注释名称相等的dataProvider名称。
@Factory 将一个方法标记为工厂，返回TestNG将被用作测试类的对象。 该方法必须返回Object []。
@Listeners 定义测试类上的侦听器。
@Parameters 描述如何将参数传递给@Test方法。
@Test 将类或方法标记为测试的一部分。
```
#####5.总结
```
TestNG在参数化测试，依赖测试和套件测试(分组概念)方面更加突出。 TestNG用于高级测试和复杂集成测试。 它的灵活性对于大型测试套件尤其有用。 
此外，TestNG还涵盖了整个核心的JUnit4功能。
```






