package com.llc.testng;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

public class Demo {

	@Test
	public void testngDemo() {
		TestNgServer tn = new TestNgServer();
		System.out.println(tn.getMsg());
	}

	/**
	 * 超时测试
	 */
	@Test(timeOut = 1000)
	public void timeOutDemo() {
		TestNgServer tn = new TestNgServer();
		System.out.println(tn.getMsg());
	}

	/**
	 * 负载测试
	 * invocationCount确定TestNG应该运行这个测试方法的次数
	 */
	@Test(invocationCount = 10)
	public void repeatThis() {
		System.out.println("repeatThis");
	}

	/**
	 * Firefox浏览器将提示关闭3次
	 * 注意:执行该方法需要安装Firefox浏览器及Selenium IDE、SeleniumRC、IEDriverServer
	 * 参考:https://www.cnblogs.com/zmhsoup/p/5249663.html
	 */
	@Test(invocationCount = 3)
	public void loadTestThisWebsite() {
		WebDriver driver = new FirefoxDriver();  
        driver.get("http://www.baidu.com");
        System.out.println("Page Title is " + driver.getTitle());
        AssertJUnit.assertEquals("Google", driver.getTitle());
        driver.quit();
	}

	/**
	 * 启动一个包含3个线程的线程池，并运行测试方法7次
	 */
	@Test(invocationCount = 7, threadPoolSize = 3)
    public void testThreadPools() {
        System.out.printf("Thread Id : %s%n", Thread.currentThread().getId());
    }
    	
}
