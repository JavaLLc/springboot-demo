package com.llc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.llc.util.FtpOperation;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FtpDemoApplicationTests {

	@Autowired
	private FtpOperation ftp;

	/**
	 * 上传
	 */
	@Test
	public void upload() {
		File file = new File("D://cs.xlsx2");
		if (!file.exists()) {
			return;
		}
		InputStream inputStream;
		try {
			inputStream = new FileInputStream(file);
			// 上传且重命名 false表示不删除
			ftp.uploadToFtp(inputStream, "123.txt", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void upload2() {
		try {
			// 直接上传
			ftp.uploadToFtp("D://cs.xlsx");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void inputstreamtofile(InputStream ins, File file) throws Exception {
		OutputStream os = new FileOutputStream(file);
		int bytesRead = 0;
		byte[] buffer = new byte[8192];
		while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
			os.write(buffer, 0, bytesRead);
		}
		os.close();
		ins.close();
	}

	/**
	 * 下载
	 */
	@Test
	public void download() {
		// 下载的文件名
		String fileName = "12.txt";
		String fileType = FtpOperation.getExtention(fileName);
		try {
			InputStream file = ftp.downloadFile(fileName);
			inputstreamtofile(file, new File("D://113" + fileType));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
