package com.llc.entiity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author ZhaoQian
 * @XmlAccessorType(XmlAccessType.FIELD)指定映射本类的所有字段
 * @XmlRootElement 用在class类的注解，常与@XmlRootElement，@XmlAccessorType一起使用.也可以单独使用,如果单独使用,需要在get方法上加@XmlElement等注解.
 * @XmlType,在使用@XmlType的propOrder 属性时，必须列出JavaBean对象中的所有XmlElement，否则会报错。
 */
@Entity//指名这是一个实体Bean
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Customer")
@XmlType(name = "Customer", propOrder = {"age", "name"})
@Getter
@Setter
@ToString
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    // 其实@XmlType已经默认会读取下面的name和age.@XmlElement在@XmlType存在的情况下,只会起到一个标识作用.
    @XmlAttribute(name = "姓名")//name可以起别名
    private int id;
    @XmlElement
    private String name;
    @XmlElement
    private int age;

}