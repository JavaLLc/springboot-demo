package com.llc.demo;

import com.llc.entiity.Customer;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * 读取一个XML文件并输出对象.
 */
public class XMLExample {

    public static void main(String[] args) {
        File file = new File("D:\\HelloWorld.xml");
        try {
            //反着来
            JAXBContext jc = JAXBContext.newInstance(Customer.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            Customer cus = (Customer) unmarshaller.unmarshal(file);
            System.out.println("data:" + cus);
            System.out.println("data:" + cus.getId());
            System.out.println("data:" + cus.getName());
            System.out.println("data:" + cus.getAge());
        } catch (JAXBException e) {
            System.out.println("input xml error!");
            e.printStackTrace();
        }

    }

}