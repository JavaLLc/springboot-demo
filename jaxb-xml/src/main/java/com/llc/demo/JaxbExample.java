package com.llc.demo;

import com.llc.entiity.Customer;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 * JAXB,输出一个XML文件进行交互.
 */
public class JaxbExample {

    //@Inject Customer customer;

    public static void main(String[] args) {
        Customer cus = new Customer();
        cus.setId(1);
        cus.setName("llc");
        cus.setAge(20);

        try {
            File file = new File("D:\\HelloWorld.xml");
            //初始化JAXBContext.JAXBContext类提供的JAXB API的客户端的入口点。
            //它提供一个抽象的用于管理XML / Java绑定的必要信息，以实现JAXB绑定框架行动：解组，编组和验证。
            JAXBContext jc = JAXBContext.newInstance(Customer.class);
            //将Java对象Marshal成XML内容的Marshal的初始化设置.
            Marshaller jaxbMarshaller = jc.createMarshaller();
            //output
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(cus, file);
            jaxbMarshaller.marshal(cus, System.out);

        } catch (JAXBException e) {
            System.out.println("output xml error!");
            e.printStackTrace();
        }
    }

}