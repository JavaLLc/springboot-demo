package com.llc.log4j2demo;

import com.llc.log4j2demo.util.LoggerUtil;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Log4j2
public class Log4j2DemoApplicationTests {

    /**
     * 无法定位代码位置
     */
    @Test
    public void contextLoads() {
        String msg = "Spring Boot -- Log4j2";
        LoggerUtil.i(msg);
        LoggerUtil.d(msg);
    }

    /**
     * 可以定位代码位置
     */
    @Test
    public void contextLoads2() {
        String msg = "Spring Boot -- Log4j2";
        log.info(msg);
        log.error(msg);
    }

}
