﻿#log4j2

### 优点：
- 重新配置不会失去事件，具有比logback、log4j 1.x版本更大的吞吐量  
- Log4j 2在稳定记录状态下，对单机应用是无垃圾的，对Web应用是低垃圾的。
	这不仅降低了垃圾回收器的压力，还可以提供更好的响应性能。
- 支持自定义日志级别。自定义日志级别可以在代码或配置中定义
- 支持Lambda表达式
- 支持异步输出日志

## 配置
### 一、配置文件存放位置

**Tips** 
log4j 2.0与以往的1.x有一个明显的不同，其配置文件只能采用 ==.xml==, ==.json==或者 ==.jsn== 作为后缀名，
***不支持*** ==.properties==

系统选择配置文件的优先级(从先到后)如下：
- .classpath下的名为log4j2-test.json 或者log4j2-test.jsn的文件.
- .classpath下的名为log4j2-test.xml的文件.
- .classpath下名为log4j2.json 或者log4j2.jsn的文件.
- .classpath下名为log4j2.xml的文件.
