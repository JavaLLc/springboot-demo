package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ComponentScan({"com.example.demo.controller"})//指定扫描controller层
public class YanzhengmaApplication {

	public static void main(String[] args) {
		SpringApplication.run(YanzhengmaApplication.class, args);
	}
}
