package com.example.demo.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 计算耗时
 **/
@Aspect
@Service
@Order(3) // 多个aop执行顺序
@Slf4j
public class TimeAOP {
	
	@Pointcut("execution(* com.example.demo.controller..*.*(..))")
	public void performance() {

	}

	/**
	 * 计算耗时
	 * @param jp
	 * @return
	 * @throws Throwable
	 */
	@Around(value = "execution(* com.example.demo.controller..*.*(..))")
	public Object Around(ProceedingJoinPoint jp) throws Throwable {
		// 计算方法运行时间
		Long time1 = System.currentTimeMillis();
		Object object = jp.proceed();
		Long time2 = System.currentTimeMillis();
		Long runTime = time2 - time1;

		// 获取类名
		Class<?> cls = jp.getTarget().getClass();
		String clsName = cls.getName();
		// 获取方法签名
		Signature signature = jp.getSignature();
		String methodName = signature.getName();

		log.info("{}.{}方法-运行时间:{}",clsName,methodName,runTime);
		
		return object;
	}
}
