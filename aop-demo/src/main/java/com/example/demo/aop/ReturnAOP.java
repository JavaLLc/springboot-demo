package com.example.demo.aop;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

/**
 * 改变返回值
 **/
@Aspect
@Service
@Order(1) // 多个aop执行顺序
public class ReturnAOP {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Pointcut("execution(* com.example.demo.controller..*.*(..))")
	public void performance() {

	}

	/**
	 *  String toString();         //连接点所在位置的相关信息  
	    String toShortString();     //连接点所在位置的简短相关信息  
	    String toLongString();     //连接点所在位置的全部相关信息  
	    Object getThis();         //返回AOP代理对象  
	    Object getTarget();       //返回目标对象  
	    Object[] getArgs();       //返回被通知方法参数列表  
	    Signature getSignature();  //返回当前连接点签名  
	    SourceLocation getSourceLocation();//返回连接点方法所在类文件中的位置  
	    String getKind();        //连接点类型  
	    StaticPart getStaticPart(); //返回连接点静态部分  
	 * @param invocation
	 * @return
	 */
	@Before("performance()")
	public void silenceCellPhone(JoinPoint point) {
		logger.info("第一个切点的@Before执行");
	}

	/**
	 * 可以改变参数和返回值
	 * @param jp
	 * @return
	 * @throws Throwable
	 */
	@Around(value = "execution(* com.example.demo.controller..*.*(..))")
	public Object Around(ProceedingJoinPoint jp) throws Throwable {
		// 获取参数
		Object[] args = jp.getArgs();
		if (args != null && args.length > 1) {
			// 修改目标方法的第一个参数
			args[0] = "【增加的前缀】" + args[0];
			logger.info("改变参数" + args[0]);
		}
		// 以改变后的参数去执行目标方法,并保存目标方法执行后的返回值
		Object rvt = jp.proceed(args);
		logger.info("执行目标方法之后,结束Around模拟事务");
		
		// 改变返回值
		if ("登录成功".equals(rvt.toString())) {
			logger.info("改变返回值--登录666");
			return "登录666";
		}
		return rvt;
	}
}
