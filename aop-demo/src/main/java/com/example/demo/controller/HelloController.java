package com.example.demo.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.anno.LoggerManage;

/**
 * @author itguang
 * @create 2017-12-09 8:52
 **/

@RestController
public class HelloController {


    @RequestMapping("/hello")
    @LoggerManage(logDescription = "hello")
    public String hello() {
    	int a = 0;
    	for(int i =0;i<=100000;i++) {
    		a++;
    	}
        return "hello"+a;
    }

    /**
     * PathVariable URL中占位符参数绑定到控制器处理方法的入参
     * @return
     */
    @RequestMapping("/login/{username}/{password}")
    @LoggerManage(logDescription = "登录")
    public String login(@PathVariable("username") String username,
                        @PathVariable("password") String password ) {
        return "登录成功";
    }

}
