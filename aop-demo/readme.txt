
自定义注解+aop实现日志记录
spring-aop

   AOP配置元素 | 描述 
  ------------ | -------------
  `<aop:advisor>` | 定义AOP通知器
  `<aop:after>`  | 定义AOP后置通知（不管该方法是否执行成功）
  `<aop:after-returning>` | 在方法成功执行后调用通知
  `<aop:after-throwing>` | 在方法抛出异常后调用通知
  `<aop:around>` | 定义AOP环绕通知
  `<aop:aspect>` | 定义切面
  `<aop:aspect-autoproxy>` | 定义`@AspectJ`注解驱动的切面
  `<aop:before>` | 定义AOP前置通知
  `<aop:config>` | 顶层的AOP配置元素，大多数的<aop:*>包含在<aop:config>元素内
  `<aop:declare-parent>` | 为被通知的对象引入额外的接口，并透明的实现
  `<aop:pointcut>` | 定义切点
  
  实现AOP的切面主要有以下几个要素：

使用@Aspect注解将一个java类定义为切面类
使用@Pointcut定义一个切入点，可以是一个规则表达式，比如下例中某个package下的所有函数，也可以是一个注解等。
根据需要在切入点不同位置的切入内容，5种类型的通知 
使用@Before在切入点开始处切入内容
使用@After在切入点结尾处切入内容
使用@AfterReturning在切入点return内容之后切入内容（可以用来对处理返回值做一些加工处理）
使用@Around在切入点前后切入内容，并自己控制何时执行切入点自身的内容
使用@AfterThrowing用来处理当切入内容部分抛出异常之后的处理逻辑


@Around注解用于修饰Around增强处理，Around增强处理是功能比较强大的增强处理，
它近似于Before增强处理和AfterReturing增强处理的总结，Around增强处理既可在执行目标方法之前增强动作，
也可在执行目标方法之后织入增强的执行。与Before增强处理、AfterReturning增强处理不同的是，
Around增强处理可以决定目标方法在什么时候执行，如何执行，甚至可以完全阻止目标方法的执行。

当定义一个Around增强处理方法时，该方法的第一个形参必须是ProceedJoinPoint类型（至少含有一个形参），
在增强处理方法体内，调用ProceedingJoinPoint参数的procedd()方法才会执行目标方法——
这就是Around增强处理可以完全控制方法的执行时机、如何执行的关键；
如果程序没有调用ProceedingJoinPoint参数的proceed()方法，则目标方法不会被执行。


