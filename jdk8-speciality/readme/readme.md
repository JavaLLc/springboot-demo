# 项目介绍 #
##1.这是JDK8新特性的案例学习  ##
```
多个方法有相同的处理可以使用函数式接口
    （比如:都要参数进行判断,就可以把这个判断写成一个方法）:
    public  void getDemo(String msg, Consumer<String> fun) {
    	if (msg != null) 
    		fun.accept(msg);	
    }
    //把方法当作参数
    getDemo("测试", m -> demo(m));
```