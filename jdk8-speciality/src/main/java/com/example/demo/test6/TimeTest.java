package com.example.demo.test6;

import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.time.*;
import java.util.Base64;

/**
 * Created by Administrator on 2017/10/17.
 */
public class TimeTest {

    /*
    * LocalDate
    * LocalDate是一个不可变的类，它表示默认格式(yyyy-MM-dd)的日期
    * 我们可以使用now()方法得到当前时间，也可以提供输入年份、月份和日期的输入参数来创建一个LocalDate实例。
    * 该类提供与java.sql.Date相同的功能
     */
    @Test
    public void localDateTest() {
        // 当前日期
        LocalDate today = LocalDate.now();
        System.out.println("当前日期=" + today);

        // 通过传入的参数创建LocalDate对象
        LocalDate firstDay_2017 = LocalDate.of(2017, Month.OCTOBER, 17);
        System.out.println("指定日期=" + firstDay_2017);

        // 根据有效输入创建日期，以下代码会抛异常，无效输入，2017年2月没有29日
        // LocalDate feb29_2017 = LocalDate.of(2017, Month.FEBRUARY, 29);

        // 获取不同时区的日期 "Asia/Kolkata"
        LocalDate todayShanghai = LocalDate.now(ZoneId.of("Asia/Shanghai"));
        System.out.println("当前上海标准日期=" + todayShanghai);

        // 从基准日期获取日期  例如： 01/01/1970
        LocalDate dateFromBase = LocalDate.ofEpochDay(365);
        System.out.println("基准日期后的第365天= " + dateFromBase);

        //2017年的第一百天
        LocalDate hundredDay2017 = LocalDate.ofYearDay(2017, 100);
        System.out.println("2014年的第一百天=" + hundredDay2017);
    }

    /*
    * LocalTime是一个不可变的类
    * 默认格式是hh:mm:ss.zzz。该类也提供了时区支持，可以传入小时、分钟和秒等输入参数创建实例
     */
    @Test
    public void localTimeTest() {
        // 当前时间  14:20:33.253
        LocalTime time = LocalTime.now();
        System.out.println("当前时间=" + time);

        // 指定时间
        LocalTime specificTime = LocalTime.of(12, 20, 25, 40);
        System.out.println("指定时间=" + specificTime);


        // 获取不同时区的时间 "Asia/Kolkata"
        LocalTime timeKolkata = LocalTime.now(ZoneId.of("Asia/Kolkata"));
        System.out.println("印度当前时间=" + timeKolkata);

        // 从基准日期获取时间 01/01/1970
        LocalTime specificSecondTime = LocalTime.ofSecondOfDay(3600);
        System.out.println("3600秒后的日期= " + specificSecondTime);
    }

    /*
    * LocalDateTime
    * LocalDateTime是一个不可变的日期-时间对象
    * 默认格式是yyyy-MM-dd-HH-mm-ss.zzz。
    * 它提供了一个工厂方法，通过接收LocalDate和LocalTime输入参数，来创建LocalDateTime实例。
     */
    @Test
    public void localDateTimeTest() {
        // 当前日期时间 2017-10-17T14:29:57.772
        LocalDateTime today = LocalDateTime.now();
        System.out.println("当前日期时间=" + today);

        // 传入LocalDate , LocalTime
        today = LocalDateTime.of(LocalDate.now(), LocalTime.now());
        System.out.println("当前日期时间TWO" + today);

        // 指定日期时间
        LocalDateTime specificDate = LocalDateTime.of(2018, Month.JANUARY, 1, 9, 10, 30);
        System.out.println("指定日期时间" + specificDate);


        //印度当前日期时间
        LocalDateTime todayKolkata = LocalDateTime.now(ZoneId.of("Asia/Kolkata"));
        System.out.println("印度当前日期时间" + todayKolkata);

        // 基准日期 01/01/1970
        LocalDateTime dateFromBase = LocalDateTime.ofEpochSecond(3600, 1000000, ZoneOffset.UTC);
        System.out.println("基准时间后3600秒1000000纳秒= " + dateFromBase);
    }

    /*
    *  Base64
    *  Base64类同时还提供了对URL、MIME友好的编码器与解码器
    *  Base64.getUrlEncoder() / Base64.getUrlDecoder(),
    *  Base64.getMimeEncoder() / Base64.getMimeDecoder()
     */
    @Test
    public void test() {
        final String text = "JDK8新特性";
        //编码
        final String encoded = Base64
                .getEncoder()
                .encodeToString(text.getBytes(StandardCharsets.UTF_8));
        System.out.println("编码后--" + encoded);

        //解码
        final String decoded = new String(
                Base64.getDecoder().decode(encoded),
                StandardCharsets.UTF_8);
        System.out.println("解码后--" + decoded);
    }

}
