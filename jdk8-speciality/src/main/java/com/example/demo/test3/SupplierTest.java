package com.example.demo.test3;/**
 * Created by Administrator on 2017/10/15.
 */

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * @author 李伦昶
 * @create 2017-10-15 17:26
 */

/*
*2.Supplier<T> 供给型接口
*	T get();
**/
public class SupplierTest {

    @Test
    public void test1() {
        List<Integer> list = getList(7, () -> (int) (Math.random() * 100));
        //遍历
        System.out.println(list);
    }

    //需求:生成n个整数放入集合
    public List<Integer> getList(int num, Supplier<Integer> s) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            list.add(s.get());
        }
        return list;
    }
}
