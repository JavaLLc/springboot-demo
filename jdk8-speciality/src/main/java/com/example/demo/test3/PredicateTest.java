package com.example.demo.test3;/**
 * Created by Administrator on 2017/10/15.
 */

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author 李伦昶
 * @create 2017-10-15 19:48
 */

/*
*4.Predicate<T> 断言型接口
*   boolean test(T t);
**/
public class PredicateTest {

    @Test
    public void test1() {
        List<String> list = Arrays.asList("a.txt", "bd.exe", "hg.txt", "fd.mp3");
        List<String> txt = getList(list, s -> s.endsWith("txt"));
        System.out.print(txt);
    }

    //需求:将字符串以"txt"结尾的添加到集合返回
    public List<String> getList(List<String> list, Predicate<String> pre) {
        List<String> lists = new ArrayList<>();
        list.forEach(str -> {
            if (pre.test(str)) {
                lists.add(str);
            }
        });
        return lists;
    }
}
