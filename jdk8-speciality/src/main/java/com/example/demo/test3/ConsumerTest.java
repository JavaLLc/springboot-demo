package com.example.demo.test3;/**
 * Created by Administrator on 2017/10/15.
 */

import org.junit.Test;

import java.util.function.Consumer;

/**
 * @author 李伦昶
 * @create 2017-10-15 17:08
 */

/*1.Consumer<T> 消费型接口
*	void accept(T t);
**/
public class ConsumerTest {

    @Test
    public void test1() {
        pay(10000, m -> System.out.print("消费" + m + "元"));
    }

    public void pay(double money, Consumer<Double> c) {
        c.accept(money);
    }
}
