package com.example.demo.test3;/**
 * Created by Administrator on 2017/10/15.
 */

import org.junit.Test;

import java.util.function.Function;

/**
 * @author 李伦昶
 * @create 2017-10-15 18:07
 */

/*
*3.Function<T,R> 函数式接口
*	R apply(T t)
**/
public class FunctionTest {

    @Test
    public void test1() {
        String str = getStr("\t\t\t字符串", s -> s.trim());
        System.out.print(str);
    }

    //需求:传入一个字符串去除首尾空格返回新的字符串
    public String getStr(String str, Function<String, String> fun) {
        return fun.apply(str);
    }
}
