package com.example.demo.test1;/**
 * Created by Administrator on 2017/10/14.
 */

/**
 * @author 李伦昶
 * @create 2017-10-14 11:20
 */
/**
 * 许我们给接口添加非抽象的方法实现，
 * 只需要使用 default关键字即可
 * 这个特征又叫做扩展方法
 * */
public interface Person {
    //未实现的方法
    void eat();

    //已经实现的方法
    default void sleep() {
        System.out.print("睡觉");
    }
}
