package com.example.demo.test1;/**
 * Created by Administrator on 2017/10/14.
 */

/**
 * @author 李伦昶
 * @create 2017-10-14 11:26
 */
public class Test {

    public static void main(String[] args) {
        Student s = new Student();
        //调用Student中已经实现的方法
        s.eat();
        //调用父类的方法
        s.sleep();
    }
}
