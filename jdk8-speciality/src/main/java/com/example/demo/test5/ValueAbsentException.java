package com.example.demo.test5;

/**
 * Created by Administrator on 2017/10/16.
 */
public class ValueAbsentException extends Throwable {

    public ValueAbsentException() {
        super();
    }

    public ValueAbsentException(String msg) {
        super(msg);
    }

    public String getMessage() {
        return "No value present in the Optional instance";
    }
}
