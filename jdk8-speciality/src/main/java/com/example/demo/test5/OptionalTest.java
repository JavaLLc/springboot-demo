package com.example.demo.test5;

/**
 * Created by Administrator on 2017/10/16.
 */

import com.example.demo.test2.User;
import org.junit.Test;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Optional;

/**
 * Java 8 Optional类
 * 主要对空指针做处理
 */
public class OptionalTest {

    /*
    *of方法通过工厂方法创建Optional类。
    *创建对象时传入的参数不能为null。
    *否则抛出NullPointerException
     */
    @Test
    public void test1() {
        //调用工厂方法创建Optional实例
        Optional<String> name = Optional.of("Sanaulla");
        //传入参数为null，抛出NullPointerException.
        //Optional<String> someNull = Optional.of(null);
    }

    /*
    *ofNullable与of方法相似
    * 唯一的区别是可以接受参数为null的情况
     */
    @Test
    public void test2() {
        Optional<String> someNull = Optional.ofNullable(null);
    }

    /*
    * isPresent
    * 如果值存在返回true，否则返回false
     */
    @Test
    public void filterTest3() {
        Optional<String> name = Optional.of("Sanaulla");
        Optional<String> someNull = Optional.ofNullable(null);

        //isPresent方法用来检查Optional实例中是否包含值
        System.out.println(name.isPresent()); //true
        System.out.println(someNull.isPresent()); //false
    }

    /*
    * get
    * 如果Optional有值则将其返回，否则抛出NoSuchElementException
     */
    @Test
    public void test4() {
        Optional<String> name = Optional.of("Optional");
        if (name.isPresent()) {
            String s = name.get();
            System.out.print(s);
        }
    }

    /*
    * ifPresent
    * 如果Optional实例有值则为其调用Consumer(消费型接口)，否则不做处理
     */
    @Test
    public void test5() throws Exception {
        Optional<String> name = Optional.of("zg");
        //name.ifPresent(s->System.out.print(s));
        name.ifPresent(System.out::print);
    }


    /*
    * orElse
    * 如果有值则将其返回，否则返回指定的其它值
     */
    @Test
    public void test6() {
        Optional<String> someNull = Optional.ofNullable(null);
        String s = someNull.orElse("今天天气很好");
        System.out.print(s);
    }

    /*
    * orElseGet
    * orElseGet与orElse方法类似，区别在于得到的默认值。
    * orElse方法将传入的字符串作为默认值
    * orElseGet方法可以接受Supplier接口的实现用来生成默认值
     */
    @Test
    public void test7() {
        Optional<User> someNull = Optional.ofNullable(null);
        User user = someNull.orElseGet(() -> new User("zs", 18));
        System.out.print(user);
    }

    /*
    * orElseThrow
    * 如果有值则将其返回，否则抛出supplier接口创建的异常
     */
    @Test
    public void test8() {
        Optional<String> someNull = Optional.ofNullable(null);
        try {
            //orElseThrow与orElse方法类似。与返回默认值不同，
            //orElseThrow会抛出lambda表达式或方法生成的异常
            String s = someNull.orElseThrow(ValueAbsentException::new);
        } catch (Throwable ex) {
            //输出: No value present in the Optional instance
            System.out.println(ex.getMessage());
        }

    }

    /*
    * map
    * 如果有值，则对其执行调用mapping函数得到返回值。
    * 如果返回值不为null，则创建包含mapping返回值的Optional作为map方法返回值，否则返回空Optional。
     */
    @Test
    public void test9() {
        Optional<String> someNull = Optional.ofNullable("null");
        //map方法执行传入的lambda表达式参数对Optional实例的值进行修改。
        //为lambda表达式的返回值创建新的Optional实例作为map方法的返回值。
        Optional<String> upperName = someNull.map(String::toUpperCase);
        System.out.println(upperName.orElse("No value found"));
    }

    /*
    * flatMap
    * flatMap方法与map方法类似，区别在于mapping函数的返回值不同。
    * map方法的mapping函数返回值可以是任何类型T，而flatMap方法的mapping函数必须是Optional
     */
    @Test
    public void test10() {
        Optional<String> someNull = Optional.ofNullable("guo");
        //map方法中的lambda表达式返回值可以是任意类型，在map函数返回之前会包装为Optional。
        //但flatMap方法中的lambda表达式返回值必须是Optionl实例。
        Optional<String> upperName = someNull.flatMap((value) -> Optional.of(value.toUpperCase()));
        System.out.println(upperName.orElse("No value found"));

    }

    /*
    * filter
    * filter个方法通过传入限定条件对Optional实例的值进行过滤
     */
    @Test
    public void test11() {
        Optional<String> anotherName = Optional.of("Sana");
        Optional<String> shortName = anotherName.filter((value) -> value.length() > 6);
        //输出：name长度不足6字符
        System.out.println(shortName.orElse("The name is less than 6 characters"));
    }
}
