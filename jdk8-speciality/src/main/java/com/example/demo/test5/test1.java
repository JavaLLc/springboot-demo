package com.example.demo.test5;

import com.example.demo.test2.User;
import org.junit.Test;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Administrator on 2017/10/16.
 */
/*
* Stream API
* 步骤:
* 创建Stream流  --- 中间操作(过滤、排序等) --- 终端操作
* **/
public class test1 {

    /*
    * 创建Stream的方式
     */
    @Test
    public void test() {
        //1.通过集合创建
        List<String> list = new ArrayList<>();
        list.add("ghgjh");
        //创建Stream
        Stream<String> stream1 = list.stream();
        //一系列的中间操作 : 不会进行任何操作
        Stream<String> stream11 = stream1.filter((x) -> x.length() > 3);
        //终端操作 ：一次性执行操作
        stream11.forEach(System.out::print);

        //2.通过Arrays的静态方法stream
        String[] strs = new String[3];
        Stream<String> stream2 = Arrays.stream(strs);

        //3.通过Stream的静态方法of  可以传入一个对象,或多个对象(可变参数)
        Stream<String> stream3 = Stream.of("fgg", "hjfd", "jdi");
    }

    //需求:打印年龄大于18的User
    //JDK8之前
    @Test
    public void beforeTest() {

        List<User> users = Arrays.asList(
                new User("zs", 15),
                new User("ls", 20),
                new User("ww", 7)
        );

        for (User u : users) {
            if (u.getAge() > 18) {
                System.out.print(u);
            }
        }

    }

    //Lambda表达式
    @Test
    public void lambdaTest() {

        List<User> users = Arrays.asList(
                new User("zs", 15),
                new User("ls", 20),
                new User("ww", 7)
        );

        Predicate<User> pro = u -> u.getAge() > 18;
        users.forEach(user -> {
            if (pro.test(user)) {
                System.out.print(user);
            }
        });

    }

    //Lambda表达式+stream
    @Test
    public void streamTest() {

        List<User> users = Arrays.asList(
                new User("zs", 15),
                new User("ls", 20),
                new User("ww", 7)
        );

        users.stream().
                filter(u -> u.getAge() > 18).   //过滤
                forEach(System.out::print); //遍历

        users.parallelStream().
                filter(u -> u.getAge() > 18).   //过滤
                forEach(System.out::print); //遍历

    }

    /*Stream -- map 1对1映射与flatMap1对多映射
    *把 input Stream 的每一个元素，映射成 output Stream 的另外一个元素。
    **/
    @Test
    public void mapTest() {

        List<String> inList = Arrays.asList("zhong", "ruan", "guo", "ji");
        //转大写
        inList.stream().
                map(String::toUpperCase).  //input::output
                forEach(System.out::println); //遍历

        //返回字符串长度
        inList.stream().
                map(str -> str.length()).  //input::output
                forEach(System.out::println); //遍历


        List<List<Integer>> inputStream = Arrays.asList(
                Arrays.asList(1),
                Arrays.asList(2, 3),
                Arrays.asList(4, 5, 6)
        );

        //flatMap 把 input Stream 中的层级结构扁平化，将最底层元素抽出来放到一起
        //最终 output 的新 Stream 里面已经没有 List 了，都是直接的数字。
        inputStream.stream().
                filter(a -> a.size() > 2).
                flatMap(lists -> lists.stream()).
                forEach(System.out::print);

    }

    //filter 过滤
    //forEach 遍历
    @Test
    public void filterTest() {
        List<String> list = Arrays.asList("zhong", "ruan", "guo", "ji");
        list.stream().
                filter(s -> s.length() > 3).
                forEach(System.out::print);
    }

    /*  findFirst 返回 Stream 的第一个元素，或者空
    *   返回值类型：Optional
    *  Optional 目的是尽可能避免 NullPointerException
    **/
    @Test
    public void findFirstTest() {

        String strA = " abcd ", strB = null;
        System.out.println(strA);
        System.out.println("");
        System.out.println(strB);
        System.out.println(strA.length());  //6
        System.out.println("".length());    //0
        // System.out.println(strB.length());  //空指针异常
        // Java 8
        Optional.ofNullable(strB).ifPresent(System.out::println);

        List<String> list = Arrays.asList("zhong", "ruan", "guo", "ji");

        Optional<String> s = list.stream(). //返回值为Optional类型
                map(String::toUpperCase).
                findFirst();
    }

    /*
    * reduce
    * 主要作用是把 Stream 元素组合起来
    * 它提供一个起始值，然后依照运算规则，和前面 Stream 的第一个、第二个、第 n 个元素组合
     */
    @Test
    public void reduceTest() {
        // 字符串连接
        String concat = Stream.of("A", "B", "C", "D").reduce("初始值", String::concat);
        // 求最小值，minValue = -3.0
        double minValue = Stream.of(-1.5, 1.0, -3.0, -2.0).reduce(Double.MAX_VALUE, Double::min);
        // 求和,有起始值
        int sumValue = Stream.of(1, 2, 3, 4).reduce(0, Integer::sum);
        // 求和,无起始值 的reduce()，由于可能没有足够的元素，返回的是 Optional
        int sumValue2 = Stream.of(1, 2, 3, 4).reduce(Integer::sum).get();
        //过滤，字符串连接
        String concat2 = Stream.of("abc", "BC", "a", "ffdssa", "e", "F").
                filter(x -> x.length() >= 3).
                reduce("", String::concat);
        System.out.print(concat + "--" + minValue + "--" + sumValue + "--" + sumValue2 + "--" + concat2);
    }

    /*
    * limit/skip
    * limit 返回 Stream 的前面 n 个元素；skip 则是扔掉前 n 个元素
     */
    @Test
    public void limitTest1() {
        List<String> list = Arrays.asList("zhong", "ruan", "guo", "ji");
        list.stream().
                limit(3).  // 返回前面三个元素 "zhong", "ruan", "guo"
                skip(2).  //  扔掉前面两个元素  "guo"
                forEach(System.out::print); //遍历
    }

    /*
    * sorted
    * 对 Stream 的排序
    * sorted()自然排序  sorted(Comparator<? super T> comparator)定制排序
     */
    @Test
    public void sortedTest() {
        List<User> users = new ArrayList();
        for (int i = 10; i >= 5; i--) {
            User user = new User("name", i * 2);
            users.add(user);
        }
        users.stream().
                limit(3).   //选择前三个元素
                sorted((u1, u2) -> Integer.compare(u1.getAge(), u2.getAge())).    //排序
                forEach(System.out::println);
    }

    /*
    * min 和 max /distinct(去重复)
    * 功能也可以通过对 Stream 元素先排序，再 findFirst 来实现，但前者的性能会更好
     */
    @Test
    public void maxTest() {
        List<Integer> list = Arrays.asList(7, 58, 19, 7, 19);
        Integer max = list.stream().
                max(Integer::compare).      //传人比较器
                get();                      //获取对象
        System.out.print(max);

        list.stream()
                .distinct()                 //去重复
                .forEach(System.out::println); //遍历
    }

    /*
    * Match
    * allMatch：Stream 中全部元素符合传入的 predicate，返回 true
    * anyMatch：Stream 中只要有一个元素符合传入的 predicate，返回 true
    * noneMatch：Stream 中没有一个元素符合传入的 predicate，返回 true
     */
    @Test
    public void matchTest2() {
        List<Integer> list = Arrays.asList(12, 58, 19, 7);
        boolean b = list.stream().
                allMatch(a -> a > 6);
        System.out.print(b);
    }

    //自己生成流:无限--必须利用 limit 之类的操作限制 Stream 大小
    @Test
    public void infiniteTest() {
        //创建无限流
        //迭代  (初始值,Function<T, T>)  需求:生成一个等差数列
        Stream<Integer> stream4 = Stream.iterate(0, (x) -> x + 2);
        //stream4.limit(10).forEach(System.out::println);

        //生产 (Supplier<T> s)  需求:生成7个随机数
        Stream<Integer> stream5 = Stream.generate(() -> (int) (Math.random() * 100));
        stream5.limit(7).forEach(System.out::println);
    }

    /*
    * Collectors
    * 用 Collectors 来进行 reduction 操作
    * 主要作用是辅助进行各类有用的 reduction 操作，如转变输出为 Collection
     */
    @Test
    public void collectorsTest() {
        //无限流生成对象
        Stream<User> stream = Stream.generate(() -> new User("张三" + (int) (Math.random() * 10) + "号", (int) (Math.random() * 20)))
                .limit(10);//取10个
//        //按年龄分组
//        Map<Integer, List<User>> map = stream.collect(Collectors.groupingBy(User::getAge));
//        //遍历
//        ergodic(map);

        /*
        * partitioningBy 其实是一种特殊的 groupingBy
        * 依照条件测试的是否两种结果来构造返回的数据结构
         */
        //按照未成年人和成年人归组
        Map<Boolean, List<User>> map1 = stream.collect(Collectors.partitioningBy((u) -> u.getAge() < 18));
        System.out.println("Children number: " + map1.get(true).size());
        System.out.println("Adult number: " + map1.get(false).size());
    }

    @Test
    public void collectorsTest2() {
        //无限流生成对象
        Stream<User> stream = Stream.generate(() -> new User("张三" + (int) (Math.random() * 10) + "号", (int) (Math.random() * 20)))
                .limit(10);//取10个
        //最终操作-转化为集合
        List<User> list = stream.collect(Collectors.toList());
        System.out.println(list);
    }

    //遍历的方法
    public static void ergodic(Map<Integer, List<User>> map) {
        //获取迭代器
        Iterator it = map.entrySet().iterator();
        //迭代遍历
        while (it.hasNext()) {
            Map.Entry<Integer, List<User>> user = (Map.Entry) it.next();
            System.out.println(user.getKey() + " = " + user.getValue() + "个数=" + user.getValue().size());
        }
    }

    /*
    * stream() − 为集合创建串行流。
    * parallelStream() − 为集合创建并行流。
    * Parallel Streams , 并行流提高性能
    * 流可以是顺序的也可以是并行的。顺序流的操作是在单线程上执行的，而并行流的操作是在多线程上并发执行的。
     */
    /**
     * 结语:
     * 1.不是数据结构
     * 2.它没有内部存储，它只是用操作管道从 source（数据结构、数组、generator function、IO channel）抓取数据。
     * 3.它也绝不修改自己所封装的底层数据结构的数据。例如 Stream 的 filter 操作
     *    会产生一个不包含被过滤元素的新 Stream，而不是从 source 删除那些元素。
     * 4.所有 Stream 的操作必须以 lambda 表达式为参数
     * 5.不支持索引访问
     * 6.你可以请求第一个元素，但无法请求第二个或最后一个。
     * 7.很容易生成数组或者 List
     * 8.惰性化
     * 9.很多 Stream 操作是向后延迟的，一直到它弄清楚了最后需要多少数据才会开始。
     * 10.中间操作永远是惰性化的。
     * 11.并行能力
     * 12.当一个 Stream 是并行化的，就不需要再写多线程代码，所有对它的操作会自动并行进行的。
     * 13.可以是无限的
     * 14.集合有固定大小，Stream 则不必。limit(n) 和 findFirst()操作可以对无限的 Stream 进行运算并很快完成。
     */

}
