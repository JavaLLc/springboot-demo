#Optional
1.创建Optional对象
empty() 方法用于创建一个没有值的Optional对象
Optional<String> emptyOpt = Optional.empty();

of() 方法使用一个非空的值创建Optional对象：
String str = "Hello World";
Optional<String> notNullOpt = Optional.of(str);

ofNullable() 方法接收一个可以为null的值：
Optional<String> nullableOpt = Optional.ofNullable(str);
--------------------------------------------------------
2.判断Null
isPresent()
如果创建的对象没有值，调用isPresent()方法会返回false，调用get()方法抛出NullPointerException异常
-----------------------------------------------------------------------------------------------
3.获取对象
get()
------
4.使用map提取对象的值
如果我们要获取User对象中的roleId属性值，常见的方式是直接获取：
String roleId = null;
if (user != null) {
 roleId = user.getRoleId();
}

使用Optional中提供的map()方法可以以更简单的方式实现：
Optional<User> userOpt = Optional.ofNullable(user);
Optional<String> roleIdOpt = userOpt.map(User::getRoleId);
-----------------------------------------------------------------------------------------------
5.使用orElse方法设置默认值
orElse()：如果有值就返回，否则返回一个给定的值作为默认值；
orElseGet()：与orElse()方法作用类似，区别在于生成默认值的方式不同。该方法接受一个Supplier<? extends T>函数式接口参数，用于生成默认值；
orElseThrow()：与前面介绍的get()方法类似，当值为null时调用这两个方法都会抛出NullPointerException异常，区别在于该方法可以指定抛出的异常类型。
--------------------- 
6.使用filter()方法过滤
filter()方法可用于判断Optional对象是否满足给定条件，一般用于条件过滤：

Optional<String> optional = Optional.of("wangyi@163.com");
optional = optional.filter(str -> str.contains("164"));
如果filter()方法中的Lambda表达式成立，filter()方法会返回当前Optional对象值，否则，返回一个值为空的Optional对象。
-----------------------------------------------------------------------------------------------
7.正确示例
orElse()方法的使用

return str != null ? str : "Hello World"
上面的代码表示判断字符串str是否为空，不为空就返回，否则，返回一个常量。使用Optional类可以表示为：

return strOpt.orElse("Hello World")


简化if-else
User user = ...
if (user != null) {
 String userName = user.getUserName();
 if (userName != null) {
 return userName.toUpperCase();
 } else {
 return null;
 }
} else {
 return null;
}

上面的代码可以简化成：
User user = ...
Optional<User> userOpt = Optional.ofNullable(user);
return userOpt.map(User::getUserName)
 .map(String::toUpperCase)
 .orElse(null);








