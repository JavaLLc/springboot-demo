package com.example.demo.test5;

import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2017/10/17.
 */

/**
 * stream与parallelStream
 */
public class ParallelStreams {

    int max = 1000_000;
    List<String> values;

    public ParallelStreams(){
        //创建一个包含唯一元素的大容器：
        values = new ArrayList<String>();
        for(int i=max; i>0; i--){
            //UUID目的是让分布式系统中的所有元素，都能有唯一的辨识资讯，
            UUID uuid = UUID.randomUUID();
            values.add(uuid.toString());
        }
    }

    //测试排序这些元素需要多长时间。
    //采用顺序流进行排序
    @Test
    public void sequentialSort(){
        /*
        * System.currentTime(),精度是毫秒，返回值是从1970.1.1的零点开始计算
        * System.nanoTime() 输出的精度是纳秒级别 ，这个值值可以是任意的
        */
        long t0 = System.nanoTime();

        long count = values.stream()
                            .sorted() //排序
                            .count(); //计数

        System.err.println("个数 = " + count);

        long t1 = System.nanoTime();
        //转化为毫秒
        long millis  = TimeUnit.NANOSECONDS.toMillis(t1 - t0);
        System.out.println(String.format("sequential sort took: %d ms", millis));//804
    }

    //采用并行流进行排序
    @Test
    public void parallelSort(){

        long t0 = System.nanoTime();

        long count = values.parallelStream().sorted().count();
        System.err.println("个数 = " + count);

        long t1 = System.nanoTime();
        long millis  = TimeUnit.NANOSECONDS.toMillis(t1 - t0);
        System.out.println(String.format("parallel sort took: %d ms", millis)); //439
    }
}
