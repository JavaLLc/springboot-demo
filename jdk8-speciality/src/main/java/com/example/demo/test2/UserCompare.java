package com.example.demo.test2;/**
 * Created by Administrator on 2017/10/15.
 */

/**
 * @author 李伦昶
 * @create 2017-10-15 11:27
 */
@FunctionalInterface
public interface UserCompare<T> {
    //比较的方法
    public Boolean compare(T t);
}
