package com.example.demo.test2;/**
 * Created by Administrator on 2017/10/15.
 */

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 李伦昶
 * @create 2017-10-15 10:56
 */

public class BeforeTest {

    @Test
    public void test() {
        // Java 8之前：
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Before Java8, too much code for too little to do");
            }
        }).start();
    }

    @Test
    public void test1() {
        //数组转List集合
        List<User> users = Arrays.asList(
                new User("c", 10),
                new User("g", 19),
                new User("f", 22)
        );

        //调用方法
        List<User> list = getUserList(users, new UserCompare<User>() {
            @Override
            public Boolean compare(User user) {
                return user.getAge() >= 18;
            }
        });
        System.out.print(list);
    }

    /*
     * @param users     存储user的集合
     * @param compare 过滤比较的接口
     * @return 刷选后新的list集合
     * */
    public List<User> getUserList(List<User> users, UserCompare<User> compare) {
        List<User> list = new ArrayList<>();
        for (User u : users) {
            if (compare.compare(u)) {
                list.add(u);
            }
        }
        return list;
    }

}
