package com.example.demo.test2;/**
 * Created by Administrator on 2017/10/15.
 */

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 李伦昶
 * @create 2017-10-15 11:48
 */

public class LambdaTest {

    @Test
    public void test() {
        //Java 8方式：
        new Thread(() -> System.out.println("In Java8, Lambda expression rocks !!")).start();
    }

    @Test
    public void test1() {
        //数组转List集合
        List<User> users = Arrays.asList(
                new User("a", 10),
                new User("b", 19),
                new User("c", 22)
        );

        //调用方法
        //只有一条语句时,return和大括号可以不写,编译器可以通过上下玩推断类型(泛型)
        // List<User> list = getUserList(users, (e) -> e.getAge() >= 18);
        List<User> list = getUserList(users, (e) -> {
            return e.getAge() >= 18;
        });
        List<User> list2 = getUserList(users, user ->
             user.getAge() >= 18
        );
        //遍历打印
        //list.forEach(System.out::println);
        //遍历
        list.forEach(u -> System.out.println(u.getName()));

    }

    //年龄大于18
    public List<User> getUserList(List<User> users, UserCompare<User> compare) {
        List<User> list = new ArrayList<>();
        for (User u : users) {
            if (compare.compare(u)) {
                list.add(u);
            }
        }
        return list;
    }

    //使用Stream API
    @Test
    public void test2() {
        //数组转List集合
        List<User> users = Arrays.asList(
                new User("a", 10),
                new User("b", 19),
                new User("c", 22)
        );
        users.stream()
                .filter((e) -> e.getAge() >= 18)
                .map(User::getName)
                .forEach(System.out::println);
    }
}
