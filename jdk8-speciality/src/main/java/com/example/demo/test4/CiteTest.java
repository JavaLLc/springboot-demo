package com.example.demo.test4;/**
 * Created by Administrator on 2017/10/15.
 */

import com.example.demo.test2.User;
import org.junit.Test;

import java.util.Comparator;
import java.util.function.BiPredicate;
import java.util.function.Supplier;

/*
 * @author 李伦昶
 * @create 2017-10-15 22:19
 **/
/*
* 四．方法引用与构造器引用
* 方法引用三种语法格式
1.对象::实例方法名
2.类::静态方法名
3.类::实例方法名
* **/
public class CiteTest {

    //1.对象::实例方法名
    @Test
    public void test1() {
        User u = new User();
        u.setName("aaa");

        //Supplier<String> name = ()->u.getName();
        Supplier<String> s = u::getName;
        String name = s.get();
        System.out.print(name);
    }

    //2.类::静态方法名
    @Test
    public void test2() {
        //比较器
        Comparator<Integer> com = (x, y) -> Integer.compare(x, y);
        Comparator<Integer> com2 = Integer::compare;
    }

    //3.类::实例方法名
    //当前面参数x作为对象调用方法,后面的参数y作为方法参数时使用
    @Test
    public void test3() {
        //判断两个字符串是否相等
        //BiPredicate<T,U>
        //boolean test(T t, U u);
        BiPredicate<String, String> bc = (x, y) -> x.equals(y);
        boolean b = bc.test("a", "b");

        BiPredicate<String, String> bc2 = String::equals;
        boolean b1 = bc2.test("a", "a");

        System.out.print(b + "---" + b1);
    }

    //构造器引用,参数个数和参数类型必须与构造器的参数个数和类型一致
    @Test
    public void test5() {
        //返回User对象
        Supplier<User> su = () -> new User();
        User user = su.get();

        Supplier<User> su2 = User::new;
        User user2 = su2.get();
    }

}
