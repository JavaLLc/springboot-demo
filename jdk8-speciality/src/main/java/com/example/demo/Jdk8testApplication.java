package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jdk8testApplication {

	public static void main(String[] args) {
		SpringApplication.run(Jdk8testApplication.class, args);
	}
}
