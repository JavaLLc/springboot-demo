package com.llc.demo1;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 通过注解进行赋值（结合了工厂方法模式）
 *
 * @author
 */
@Documented //注解包含在javadoc
@Inherited//注解可以被继承
@Target({ElementType.FIELD, ElementType.METHOD})//作用目标
@Retention(RetentionPolicy.RUNTIME)//注解会在class字节码文件中存在，在运行时可以通过反射获取到
public @interface Init {
    public String value() default "";
}