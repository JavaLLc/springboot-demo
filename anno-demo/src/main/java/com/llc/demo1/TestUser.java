package com.llc.demo1;

import lombok.Getter;
import lombok.ToString;

import java.util.Optional;

/**
 * @author LLC
 */
@Getter
@ToString
public class TestUser {

    private String userName;
    private String password;

    @Init("llc")
    public void setUserName(String userName) {
        this.userName = userName;
    }
    @Init("123456")
    public void setPassword(String password) {
        this.password = password;
    }


    public static void main(String[] args) {
        TestUser user = UserFactory.create(new TestUser(), TestUser.class);
        TestUser testUser = Optional.ofNullable(user).orElse(new TestUser());
        System.out.print(testUser.toString());
    }
}
