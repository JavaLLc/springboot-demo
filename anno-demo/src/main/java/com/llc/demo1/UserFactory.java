package com.llc.demo1;

import java.lang.reflect.Method;


/**
 * UserFactory.java
 *
 * @author Llc
 */
public class UserFactory<T> {

    public static <T> T create(T t, Class<T> tClass) {

        // 获取User类中所有的方法（getDeclaredMethods也行）
        Method[] methods = tClass.getMethods();
        try {
            for (Method method : methods) {
                // 如果此方法有注解，就把注解里面的数据赋值到user对象
                if (method.isAnnotationPresent(Init.class)) {
                    Init init = method.getAnnotation(Init.class);
                    method.invoke(t, init.value());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return t;
    }
}