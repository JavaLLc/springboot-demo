###apiDoc(官网:http://apidocjs.com/)
#####1.项目根目录下新建apidoc.json文件
```
描述了项目对外提供接口的概要信息如名称、版本、描述、文档打开时浏览器显示标题和接口缺省访问地址
例如:
{
  "name": "接口示例",
  "version": "1.0.0",
  "description": "某接口文档",
  "title": "标题",
  "url" : "https://www.jianshu.com/p/34eac66b47e3"
}
```
#####2.注解说明
```
@api {method} path [title]:HTTP接口调用方法、路径及名称
@apiVersion version :版本号
@apiName name :名称
@apiGroup name :分组
@apiHeader [(group)] [{type}] [field=defaultValue] [description]:请求头参数
@apiParam [(group)] [{type}] [field=defaultValue] [description]:请求参数
@apiSuccess [(group)] [{type}] field [description]返回数据描述
@apiSuccessExample [{type}] [title]
     example
     接口成功返回样例
@apiError [(group)] [{type}] field [description]:接口失败描述
@apiErrorExample [{type}] [title]
     example
  接口失败返回样例
@apiDefine name [title]
                     [description]
  类似于宏定义，可以被引用
@apiUse name:使用@apiDefine定义的描述
@apiIgnore将无法解析块。如果您在源代码中留下过时或未完成的方法并且您不希望将其发布到文档中，那么它很有用。
@apiPrivate 将API定义为私有,不会显示在文档
等等,详细查看官网
```
#####3.生成文档
```
1.安装node.js 略
2.安装apiDoc
npm install apidoc -g
3.到项目根目录,cmd,生成文档
apidoc -i src/ -o apidoc/
在apidoc目录查看index.html即可
```
#####4.分组@apiGroup兼容中文
```
1.在目录
C:\Users\lenovo\AppData\Roaming\npm\node_modules\apidoc\node_modules\apidoc-core\lib\workers
找到 api_group.js
注释:group = group.replace(/[^\w]/g, '_');
```
#####5.apidoc.json的中文乱码问题
```
编码改为utf-8即可
```




