package com.llc.controller;

import org.springframework.web.bind.annotation.*;

/**
 * apiDoc案例
 * @author LLC
 * @date 2019年6月27日
 * @Description:
 */
@RestController
@RequestMapping(value = "/apiDocDemo")
public class UserController {

	/**
	 * @api {POST} /register:注册 注册用户
	 * @apiGroup 分组1
	 * @apiVersion 1.0.0
	 * @apiDescription 用于注册用户(API方法的详细描述)
	 * @apiParam {String} account 用户账户名
	 * @apiParam {String} password 密码
	 * @apiParam {String} mobile 手机号
	 * @apiParam {String} [recommend] 邀请码
	 * @apiParamExample {json} 请求样例:
	 * {account=sodlinken,password=112233,mobile=13739573137}
	 * @apiSuccess (200) {String} msg 信息
	 * @apiSuccess (200) {int} code 0 代表无错误 1代表有错误
	 * @apiSuccessExample {json} 返回样例:
	 * {"code":"0","msg":"注册成功"}
	 */

	/**
	 * @api {GET} /users/:id 获取用户信息
	 * @apiGroup 分组1
	 * @apiVersion 1.0.0
	 * @apiDescription 获取用户信息
	 * @apiSuccess (200) {String} msg 信息
	 * @apiSuccess (200) {int} code 0 代表无错误 1代表有错误
	 * @apiSuccess (200) {String} name 真实姓名
	 * @apiSuccess (200) {String} mobile 手机号
	 * @apiSuccess (200) {String} birthday 生日
	 * @apiSuccess (200) {String} email 邮箱
	 * @apiSuccess (200) {String} summary 简介
	 * @apiSuccess (200) {String} recommendCode 我的推荐码
	 * @apiSuccess (200) {String} idCardNo 身份证号
	 * @apiSuccess (200) {String} memberState 会员状态 0普通用户 1VIP 2账户冻结
	 * @apiSuccess (200) {String} address 家庭住址
	 * @apiSuccess (200) {String} money 账户现金
	 * @apiSuccessExample {json} 返回样例:
	 * {
	 *   "code": 0,
	 *   "msg": "",
	 *   "name": "真实姓名",
	 *   "mobile": 15808544477,
	 *   "birthday": "1990-03-05",
	 *   "email": "slocn@gamil.com",
	 *   "summary": "简介",
	 *   "recommendCode": "我的推荐码",
	 *   "idCardNo": "身份证号",
	 *   "memberState": 1,
	 *   "address": "家庭住址",
	 *   "money": "30.65"
	 * }
	 */


	/**
	 * @api {GET} /demo/:all 详细示例
	 * @apiDescription API方法的详细描述
	 * @apiError error-name 错误描述
	 * @apiErrorExample {json} 失败响应示例:
	 *     HTTP/1.1 404 Not Found
	 *     {
	 *       "code": "UserNotFound"
	 *     }
	 * @apiExample {curl} 使用方法示例:
	 *     http://localhost/8081/user
	 * @apiGroup 分组2
	 * @apiHeader (Group1) {String} Content-Type 请求头参数
	 * @apiHeaderExample {json} 请求头参数示例:
	 * 	  {
	 *      "Content-Type": "application/json"
	 *    }
	 *
	 * @apiName 定义方法文档块的名称
	 * @apiParam (200) {String} [name] 真实姓名
	 * @apiParam (200) {String} [mobile] 手机号
	 * @apiParam (200) {String} [birthday] 生日
	 * @apiParam (200) {String} [email] 邮箱
	 * @apiParam (200) {String} [summary] 简介
	 * @apiParam (200) {String} [idCardNo] 身份证号
	 * @apiParam (200) {String} [address] 家庭住址
	 * @apiParamExample {json} 参数请求示例:
	 * {
	 * id:1
	 * }
	 * @apiPermission 输出权限名称
	 * @apiSampleRequest http://localhost/8081/user
	 * @apiSuccess (Group1) {String} 200 成功返回参数
	 * @apiSuccessExample {json} 成功响应示例:
	 * HTTP/1.1 200 OK
	 * {
	 * code:200,
	 * msg:"成功"
	 * }
	 * @apiVersion 1.0.0
	 */
}
