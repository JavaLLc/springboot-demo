package com.llc;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.llc.entity.UserEntity;
import com.llc.jpa.IUserRepository;
import com.llc.mapper.IUserDao;

import lombok.extern.log4j.Log4j2;

@RunWith(SpringRunner.class)
@SpringBootTest
@Log4j2
public class JpaMybatisDemoApplicationTests {

	@Autowired
	private IUserRepository userRepository;

	@Autowired
	private IUserDao userDao;

	@Test
	public void jpaDemo() {
		Optional<UserEntity> findById = userRepository.findById(1);
		UserEntity entity = findById.get();
		log.info(entity);
	}

	@Test
	public void jpaDemo2() {
		List<UserEntity> list = userRepository.findByUserName("张三");
		log.info(list.get(0));
	}

	@Test
	public void mybatisDemo() {
		UserEntity entity = userDao.selectById(1);
		log.info(entity);
	}

	@Test
	public void mybatisDemo2() {
		UserEntity entity = userDao.selectOne();
		log.info(entity);
	}
}
