package com.llc.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@ConfigurationProperties(prefix = "jpamybatis.datasource.demo")
@Getter
@Setter
public class DataSourceProperties {
	private String url;
	private String username;
	private String password;
	private String driverClassName;
	private String type;
	private DruidProperties druid;

}
