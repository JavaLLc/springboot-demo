package com.llc.config;

import lombok.Getter;
import lombok.Setter;

/**
 * druid
 * @author LLC
 * @date 2019年6月6日
 * @Description:
 */
@Getter
@Setter
public class DruidProperties {
	private String initialSize;
	private String minIdle;
	private String maxActive;
	private String queryTimeout;
	private String transactionQueryTimeout;
	private String removeAbandonedTimeout;
	private String filterClassNames;
	private String filters;
	private String maxOpenPreparedStatements;
}
