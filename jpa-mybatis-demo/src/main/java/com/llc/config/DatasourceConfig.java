package com.llc.config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import com.alibaba.druid.filter.Filter;
import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.alibaba.druid.wall.WallConfig;
import com.alibaba.druid.wall.WallFilter;

/**
 * 配置数据源
 * @author LLC
 * @date 2019年6月6日
 * @Description:
 */
@SuppressWarnings({"rawtypes","unchecked"})
@Configuration
public class DatasourceConfig {

	private Logger logger = LoggerFactory.getLogger(DatasourceConfig.class);

	@Autowired
	private DataSourceProperties dataSourceProperties;

	
	@Primary
	@Bean(name = "jpaMybatisDataSource")
	@Qualifier("jpaMybatisDataSource")
	public DataSource jpaMybatisDataSource() {
		// 设置监控过滤器
		List<Filter> filterList = new ArrayList<>();
		filterList.add(this.wallFilter());
		filterList.add(this.statFilter());
		// 设置链接池配置
		DruidDataSource dataSource = new DruidDataSource();
		dataSource.setDriverClassName(dataSourceProperties.getDriverClassName()); // 驱动类
		dataSource.setUrl(dataSourceProperties.getUrl()); // 链接地址
		dataSource.setUsername(dataSourceProperties.getUsername()); // 用户名
		dataSource.setPassword(dataSourceProperties.getPassword()); // 密码
		dataSource.setProxyFilters(filterList); // 设置监控过滤器
		if (!StringUtils.isBlank(dataSourceProperties.getDruid().getInitialSize())) {
			dataSource.setInitialSize(Integer.parseInt(dataSourceProperties.getDruid().getInitialSize())); // 设置初始化物理连接池的个数
		}
		if (!StringUtils.isBlank(dataSourceProperties.getDruid().getMaxActive())) {
			dataSource.setMaxActive(Integer.parseInt(dataSourceProperties.getDruid().getMaxActive())); // 设置最大的连接数
		}
		if (!StringUtils.isBlank(dataSourceProperties.getDruid().getMinIdle())) {
			dataSource.setMinIdle(Integer.parseInt(dataSourceProperties.getDruid().getMinIdle())); // 设置最小的连接数
		}
		// 设置预知性要启用PSCache，必须配置大于0，当大于0时，poolPreparedStatements自动触发修改为true。
		// 在Druid中，不会存在Oracle下PSCache占用内存过多的问题，
		if (!StringUtils.isBlank(dataSourceProperties.getDruid().getMaxOpenPreparedStatements())) {
			dataSource.setMaxOpenPreparedStatements(Integer.parseInt(dataSourceProperties.getDruid().getMaxOpenPreparedStatements()));
		} else {
			dataSource.setMaxOpenPreparedStatements(200);
		}
		if (!StringUtils.isBlank(dataSourceProperties.getDruid().getQueryTimeout())) {
			dataSource.setQueryTimeout(Integer.parseInt(dataSourceProperties.getDruid().getQueryTimeout()));
		}
		if (!StringUtils.isBlank(dataSourceProperties.getDruid().getTransactionQueryTimeout())) {
			dataSource.setTransactionQueryTimeout(Integer.parseInt(dataSourceProperties.getDruid().getTransactionQueryTimeout()));
		}
		if (!StringUtils.isBlank(dataSourceProperties.getDruid().getRemoveAbandonedTimeout())) {
			dataSource.setRemoveAbandonedTimeout(Integer.parseInt(dataSourceProperties.getDruid().getRemoveAbandonedTimeout()));
		}
		dataSource.setValidationQuery("SELECT 1");
		logger.info("------Load users datasource-------");
		logger.info("drivername:" + dataSourceProperties.getDriverClassName());
		logger.info("url:" + dataSourceProperties.getUrl());
		logger.info("username:" + dataSourceProperties.getUsername());
		logger.info("-----Load users datasource success!");
		return dataSource;
	}

	@Bean
	@Qualifier("dsJdbcTemplate")
	public JdbcTemplate dsJdbcTemplate() {
		return new JdbcTemplate(this.jpaMybatisDataSource());
	}

	@PreDestroy
	public void closejpaMybatisDataSource() throws Exception {
		jpaMybatisDataSource().getConnection().close();
	}

	/**
	 * 注册druid,进行数据源监控
	 *
	 * @return 注册的实体类
	 */
	@Bean
	public ServletRegistrationBean druidServlet() {
		return new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
	}

	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		filterRegistrationBean.setFilter(new WebStatFilter());
		filterRegistrationBean.addUrlPatterns("/*");
		filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
		return filterRegistrationBean;
	}

	@Bean
	WallFilter wallFilter() {
		WallFilter wallFilter = new WallFilter();
		wallFilter.setConfig(this.wallConfig());
		return wallFilter;
	}

	@Bean
	WallConfig wallConfig() {
		WallConfig wallConfig = new WallConfig();
		wallConfig.setMultiStatementAllow(true);
		return wallConfig;
	}

	@Bean
	StatFilter statFilter() {
		return new StatFilter();
	}

}
