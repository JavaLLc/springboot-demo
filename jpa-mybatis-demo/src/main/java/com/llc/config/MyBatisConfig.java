package com.llc.config;

import java.io.File;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;

/**
 * @author LLC
 * @date 2019年6月6日
 * @Description:主数据库mybatis数据源的配置
 */
@Configuration
@MapperScan(basePackages = { "com.llc.mapper" }, sqlSessionFactoryRef = "mybatisSqlSessionFactory")
public class MyBatisConfig {

	@Autowired
	@Qualifier("jpaMybatisDataSource")
	private DataSource jpaMybatisDataSource;

	@Bean
	@Qualifier("mybatisSqlSessionFactory")
	public SqlSessionFactory mybatisSqlSessionFactory() throws Exception {
		Resource[] mapperLocations = new PathMatchingResourcePatternResolver()
				.getResources("classpath*:dao" + File.separator + "*.xml");
		MybatisSqlSessionFactoryBean factoryBean = new MybatisSqlSessionFactoryBean();
		factoryBean.setDataSource(jpaMybatisDataSource); // 设置数据源
		factoryBean.setMapperLocations(mapperLocations); // 设置xml文件目录
		factoryBean.setTypeAliasesPackage("com.llc,entity");
		return factoryBean.getObject();
	}

	@Bean
	@Qualifier("mybatisSqlSessionTemplate")
	public SqlSessionTemplate mybatisSqlSessionTemplate() throws Exception {
		SqlSessionTemplate template = new SqlSessionTemplate(mybatisSqlSessionFactory());// 使用上面配置的Factory
		return template;
	}

	@Bean
	@Qualifier("jpaMybatisDataSourceTransactionManager")
	public DataSourceTransactionManager jpaMybatisDataSourceTransactionManager() {
		DataSourceTransactionManager manager = new DataSourceTransactionManager(jpaMybatisDataSource);
		return manager;
	}

}