package com.llc.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.llc.entity.UserEntity;

/**
 * @author LLC
 * 注解Repository:标注数据访问组件，即DAO组件
 */
@Repository
public interface IUserRepository extends JpaRepository<UserEntity,Integer>,JpaSpecificationExecutor<UserEntity> {
	List<UserEntity> findByUserName(String userName);
}
