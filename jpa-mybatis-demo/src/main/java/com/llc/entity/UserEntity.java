package com.llc.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author LLC
 * @since 2019-05-30
 */
//jpa
@Entity
@Table(name = "user")
//mybatis plus
@TableName("user")
@Getter
@Setter
@ToString
public class UserEntity extends Model<UserEntity> {

	private static final long serialVersionUID = 1L;

	// mybatis plus
	@TableId(value = "id", type = IdType.AUTO)
	// jpa
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	// jpa
	@Column(name = "username")
	// mybatis plus
	@TableField(value = "username", exist = true)
	private String userName;

	private String phone;

	private String password;

	private String salt;

	private String head;

	// jpa
	@Column(name = "login_count")
	// mybatis plus
	@TableField(value = "login_count", exist = true)
	private Integer loginCount;

	// jpa
	@Column(name = "register_date")
	// mybatis plus
	@TableField(value = "register_date", exist = true)
	private Date registerDate;

	// jpa
	@Column(name = "last_login_date")
	// mybatis plus
	@TableField(value = "last_login_date", exist = true)
	private Date lastLoginDate;

}
