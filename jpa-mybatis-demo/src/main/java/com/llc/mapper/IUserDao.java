package com.llc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.llc.entity.UserEntity;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LLC
 * @since 2019-05-30
 */
public interface IUserDao extends BaseMapper<UserEntity> {
	UserEntity selectOne();
}
