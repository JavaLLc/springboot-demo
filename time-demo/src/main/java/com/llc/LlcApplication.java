package com.llc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling//查找定时任务
public class LlcApplication {
	public static void main(String[] args) {
		SpringApplication.run(LlcApplication.class, args);
	}
}
