package com.llc.demo;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Calendar
 * Created by Administrator on 2017/10/18.
 */
public class CalendarTest {

        public static void main(String[] args) {
            //设置-时分秒
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 16); // 控制时
            calendar.set(Calendar.MINUTE, 35);    // 控制分
            calendar.set(Calendar.SECOND, 3);    // 控制秒
            //得出首次执行时间
            Date time = calendar.getTime();     // 得出执行任务的时间,此处为今天的09：30：03

            Timer timer = new Timer();
            //执行定时器
            timer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    System.out.println("-------设定要指定任务--------");
                }
            }, time, 1000 * 3);// 这里设定将延时每3S固定执行,单位毫秒
        }
}
