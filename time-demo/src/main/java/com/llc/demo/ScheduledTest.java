package com.llc.demo;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Spring自带
 * Created by Administrator on 2017/10/12.
 */
@Component
public class ScheduledTest {
    //http://cron.qqe2.com/
    //从两秒开始每三秒执行一次
    @Scheduled(cron="2/3 * * * * ? ")
    public void executeUploadTask() {
        //执行任务
        System.out.println("定时任务");
    }
}
