package com.llc.demo;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * ScheduledExecutorService
 * Created by Administrator on 2017/10/12.
 */
public class ScheduledExecutorServiceTest {

    public static void main(String[] args) {
        //线程
        Runnable runnable = () -> System.out.println("Hello !!");

        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        /**
         * 执行的任务(线程)
         * 第二个参数为首次执行的延时时间，
         * 第三个参数为定时执行的间隔时间，
         * 第四个参数为TimeUnit对象指定时间单位
         */
        service.scheduleAtFixedRate(runnable, 2, 3, TimeUnit.SECONDS);
    }

}
