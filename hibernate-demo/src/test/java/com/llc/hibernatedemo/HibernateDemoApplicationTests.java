package com.llc.hibernatedemo;

import com.llc.hibernatedemo.dao.UserDao;
import com.llc.hibernatedemo.po.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class HibernateDemoApplicationTests {

    @Autowired
    private UserDao userDao;

    @Test
    public void contextLoads() {
        User user = new User();
        user.setUsername("张三");
        Example<User> example = Example.of(user);
        List<User> all = userDao.findAll(example);
        all.forEach(System.out::println);
    }

    @Test
    public void add() {
        User user = new User();
        user.setUsername("李四");
        user.setPassword("123");
        User save = userDao.save(user);
        log.info(save + "");
    }

    @Test
    public void select() {
        List<User> list = userDao.findByUsername("张三");
        log.info(list + "");
    }

    @Test
    public void delete() {
        userDao.deleteById((long)2);
    }

    @Test
    public void select2() {
        User user = userDao.findByIdAndUsername((long)1,"张三");
        log.info(user + "");
    }

}
