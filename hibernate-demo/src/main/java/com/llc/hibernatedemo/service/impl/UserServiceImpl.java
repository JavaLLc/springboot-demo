package com.llc.hibernatedemo.service.impl;

import com.llc.hibernatedemo.dao.UserDao;
import com.llc.hibernatedemo.po.User;
import com.llc.hibernatedemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserDao userDao;

    @Override
    public List<User> selectUserList() {
        Sort sort = new Sort(Sort.Direction.ASC, "id");
        return userDao.findAll(sort);
    }

    @Override
    public User selectUserById(Long id) {
        return null;
    }

    @Override
    public List<User> selectUserByName(String name) {
        return null;
    }
}
