package com.llc.hibernatedemo.service;

import java.util.List;

import com.llc.hibernatedemo.po.User;

public interface UserService {
    public List<User> selectUserList();
    public User selectUserById(Long id);
    public List<User> selectUserByName(String name);
}
