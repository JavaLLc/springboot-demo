package com.llc.hibernatedemo.po;

import com.llc.hibernatedemo.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;


/**
 * @author LLC
 */
@Entity
@Table(name = "user")
@Getter
@Setter
@ToString
public class User extends BaseEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
}
