package com.llc.hibernatedemo.dao;

import com.llc.hibernatedemo.po.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author LLC
 * 注解Repository:标注数据访问组件，即DAO组件
 */
@Repository
public interface  UserDao extends JpaRepository<User,Long> {

    List<User> findByUsername(String userName);

    User findByIdAndUsername(Long id,String userName);

}
