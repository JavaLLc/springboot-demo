package com.llc.hibernatedemo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

/**
 * @author LLC
 * 注解MappedSuperclass表示抽取公共字段
 * 使用在父类上面，是用来标识父类
 */
@MappedSuperclass
@Getter
@Setter
public class BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;
    @Column(name = "status")
    protected Integer status;
    @Column(name = "createtime")
    protected Date createtime;
    @Column(name = "updatetime")
    protected Date updatetime;
}
