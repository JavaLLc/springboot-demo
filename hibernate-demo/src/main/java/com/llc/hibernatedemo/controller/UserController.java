package com.llc.hibernatedemo.controller;

import com.llc.hibernatedemo.po.User;
import com.llc.hibernatedemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author LLC
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/findAll")
    public List<User> findUserList() {
        List<User> list = userService.selectUserList();
        return list;
    }
}