package com.llc.hibernatedemo.controller;

/**
 * @author  LLC
 * @date 2019-05-01
 * 一百亿乘法运算:考虑进位
 * 拆分一位位相乘再相加
 */
public class Mul {

    public static String mul(int[] a, int  b) {
        //千位相乘
        int[] qian = numMul(a[2],b);
        //万位相乘
        int[] wan = numMul(a[1],b);
        //亿位相乘
        int[] yi = numMul(a[0],b);

        String q = arrToString(qian, 0);
        String w = arrToString(wan, 1);
        String y = arrToString(yi, 2);

        System.out.println(q);
        System.out.println(w);
        System.out.println(y);

        String qw = Add.toAdd(q, w);
        String qwy = Add.toAdd(y, qw);

        return qwy;
    }

    public static String arrToString(int[] a,int b){
        int [] c = new int [3];
        if(b == 0){
            return Add.add(a, c);
        }else if(b == 1){
            a[0] = a[1];
            a[1] = a[2];
            a[2] = 0;
            return Add.add(a, c);
        }else{
            if(a[1]!=0){
                System.out.println("计算结果超出百亿");
                return null;
            }else{
                a[0] = a[2];
                a[2] = 0;
                return Add.add(a, c);
            }
        }

    }

    //两个数相乘
    public static int[] numMul(int a, int b) {
        String c = a * b + "";
        int[] arr = Add.getArr(c);
        return arr;
    }

    //乘法运算
    public static String toMul(String a,String b){
        int [] arr = null;
        String c = "";
        if(a.length()>=b.length()){
            arr = Add.getArr(a);
            c = b;
        }else{
            arr = Add.getArr(b);
            c = a;
        }
        int[] arr2 = Add.getArr(c);

        String mul1 = mul(arr, arr2[2]);
        arr[0] = arr[1];
        arr[1] = arr[2];
        String mul2 = mul(arr, arr2[1]);
        return Add.toAdd(mul1,mul2);
    }

    public static void main(String[] args) {
        String a = "10002";
        String b = "20006";
        System.out.println(toMul(a,b));
    }

}
