package com.llc.hibernatedemo.controller;

/**
 * @author  LLC
 * @date 2019-05-01
 * 一百亿加法运算:考虑进1
 */
public class Add {

    /**
     * 拆分字符串为数组
     * @param a
     * @return
     */
    public static int[] getArr(String a) {
        int len = a.length();
        int[] arr = new int[3];
        if (len >= 12) {
            return null;
        } else if (len >= 9) {
            arr[0] = Integer.valueOf(a.substring(0, len - 8));
            arr[1] = Integer.valueOf(a.substring(len - 8, len - 4));
            arr[2] = Integer.valueOf(a.substring(len - 4, len));
        } else if (len >= 5) {
            arr[1] = Integer.valueOf(a.substring(0, len - 4));
            arr[2] = Integer.valueOf(a.substring(len - 4, len));
        } else {
            arr[2] = Integer.valueOf(a);
        }
        return arr;
    }

    public static String add(int[] a, int[] b) {
        String result = "";
        //千位相加
        int[] qian = numAdd2(a[2], b[2]);
        int[] wan = new int[3];
        int[] yi = new int[3];
        result = Sub.coverage(qian[2]);
        //万位相加
        if (a[1] != 0 || b[1] != 0) {
            wan = numAdd3(a[1], b[1], qian[1]);
            if(wan[2]==0){
                result = "0000" + result;
            }else{
                result = wan[2] + result;
            }
        }else{
            result = "0000" + result;
        }
        //亿位相加
        if (a[0] != 0 || b[0] != 0) {
            yi = numAdd3(a[0], b[0], wan[1]);
            if(yi[1]==0){
                result = yi[2] + result;
            }else{
                result = yi[1] + "" + yi[2] + result;
            }
        }
        return result;
    }
    //两个数相加
    public static int[] numAdd2(int a, int b) {
        String c = a + b + "";
        int[] arr = getArr(c);
        return arr;
    }
    //三个数相加
    public static int[] numAdd3(int a, int b, int c) {
        String d = a + b + c + "";
        int[] arr = getArr(d);
        return arr;
    }

    //加法运算
    public static String toAdd(String a,String b){
        int[] arr1 = getArr(a);
        int[] arr2 = getArr(b);
        String add = add(arr1, arr2);
        return add;
    }

    public static void main(String[] args) {
        String a = "12300000000";
        String b = "0";
        System.out.println(toAdd(a,b));
    }

}
