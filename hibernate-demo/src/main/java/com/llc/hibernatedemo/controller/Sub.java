package com.llc.hibernatedemo.controller;

/**
 * @author  LLC
 * @date 2019-05-01
 * 一百亿减法运算:考虑退1;等于0不显示
 */
public class Sub {

    /**
     * 补零
     * @return
     */
    public static String coverage(int a){
        String c = "";
        if(a<10){
            c = "000" + a;
        }else if(a<100){
            c = "00" + a;
        }else if(a<1000){
            c = "0" + a;
        }else{
            c = "" + a;
        }
        return c;
    }

    public static String sub(int[] a, int[] b) {
        String result = "";
        //千位相减
       int qian = a[2] - b[2];
       int wan = 0;
       int yi = 0;
        //万位相减
        if (a[1] != 0 || b[1] != 0) {
            if(qian<0){
                wan = a[1] - b[1] -1;
                qian = qian * -1;
            }else{
                wan = a[1] - b[1];
            }
        }
        //亿位相减
        if (a[0] != 0 || b[0] != 0) {
            if(wan<0){
                yi = a[0] - b[0] -1;
                wan = wan * -1;
            }else{
                yi = a[0] - b[0];
            }
        }
        //补零
        String q = coverage(qian);
        String w = coverage(wan);
        //为零不显示
        if(yi!=0){
            result = yi + "";
        }else{
            w = Integer.valueOf(w) + "";
        }

        if(wan!=0){
            result = result + w;
        }
        if(qian!=0){
            if(wan==0&&w.length()!=4){
                q = Integer.valueOf(q) + "";
            }
            result = result + q;
        }
        return result;
    }

    public static String toSub(String a,String b){
        int[] arr1 = Add.getArr(a);
        int[] arr2 = Add.getArr(b);
        String sub = "";
        if(arr1[0]>arr2[0]||(arr1[0]==arr2[0]&&arr1[1]>arr2[1])||(arr1[0]==arr2[0]&&arr1[1]==arr2[1]&&arr1[2]>arr2[2])){
            sub = sub(arr1, arr2);
            return sub;
        }else{
            sub = sub(arr2, arr1);
            return "-" + sub;
        }
    }

    public static void main(String[] args) {
        String a = "12345678910";
        String b = "12345678900";
        System.out.println(toSub(b,a));
    }

}
