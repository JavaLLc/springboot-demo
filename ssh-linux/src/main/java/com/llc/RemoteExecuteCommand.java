package com.llc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.commons.lang.StringUtils;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;
import lombok.NoArgsConstructor;

/**
 * @author LLC
 * @date 2019年6月13日
 * @Description:远程执行linux的shell
 */
@NoArgsConstructor
public class RemoteExecuteCommand {

	private static String DEFAULTCHART = "UTF-8";
	private Connection conn;
	private String ip;
	private String userName;
	private String userPwd;

	public RemoteExecuteCommand(String ip, String userName, String userPwd) {
		this.ip = ip;
		this.userName = userName;
		this.userPwd = userPwd;
	}

	/**
	 * 远程登录linux的主机
	 * @return
	 *  登录成功返回true，否则返回false
	 */
	public Boolean login() {
		boolean flg = false;
		try {
			conn = new Connection(ip);
			conn.connect();// 连接
			flg = conn.authenticateWithPassword(userName, userPwd);// 认证
		} catch (IOException e) {
			e.printStackTrace();
		}
		return flg;
	}

	/**
	 * 关闭连接
	 * @param conn
	 * @param session
	 */
	public void close(Connection conn, Session session) {
		if (conn != null) {
			conn.close();
		}
		if (session != null) {
			session.close();
		}
	}

	/**
	 * 远程执行shll脚本或者命令
	 * @param cmd
	 * 		即将执行的命令
	 * @return
	 * 		命令执行完后返回的结果值
	 */
	public String execute(String cmd) {
		String result = "";
		Session session = null;
		try {
			if (login()) {
				session = conn.openSession();// 打开一个会话
				session.execCommand(cmd);// 执行命令
				result = processStdout(session.getStdout(), DEFAULTCHART);
				// 如果为得到标准输出为空，说明脚本执行出错了
				if (StringUtils.isBlank(result)) {
					result = processStdout(session.getStderr(), DEFAULTCHART);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			close(conn, session);
		}
		return result;
	}

	/**
	 * 远程执行shll脚本或者命令
	 * @param cmd
	 * 		即将执行的命令
	 * @return
	 * 		命令执行成功后返回的结果值，如果命令执行失败，返回空字符串，不是null
	 */
	public String executeSuccess(String cmd) {
		String result = "";
		Session session = null;
		try {
			if (login()) {
				session = conn.openSession();// 打开一个会话
				session.execCommand(cmd);// 执行命令
				result = processStdout(session.getStdout(), DEFAULTCHART);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			close(conn, session);
		}
		return result;
	}

	/**
	* 解析脚本执行返回的结果集
	* @param in 输入流对象
	* @param charset 编码
	* @return
	* 		以纯文本的格式返回
	*/
	private String processStdout(InputStream in, String charset) {
		InputStream stdout = new StreamGobbler(in);
		StringBuffer buffer = new StringBuffer();
		try {
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new InputStreamReader(stdout, charset));
			String line = null;
			while ((line = br.readLine()) != null) {
				buffer.append(line + "\n");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return buffer.toString();
	}

	public static void main(String[] args) {
		RemoteExecuteCommand rec = new RemoteExecuteCommand("192.168.5.133", "root", "123456");
		// 执行命令
		System.out.println(rec.execute("cd /home && ls"));

		// 执行脚本
		// rec.execute("sh /usr/local/tomcat/bin/statup.sh");
		// 这个方法与上面最大的区别就是，上面的方法，不管执行成功与否都返回，
		// 这个方法呢,如果命令或者脚本执行错误将返回空字符串
		// rec.executeSuccess("ifconfig");

	}
}
