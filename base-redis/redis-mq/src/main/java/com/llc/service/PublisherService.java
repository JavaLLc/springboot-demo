package com.llc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.llc.config.SubscriberConfig;

@Service
public class PublisherService {

	@Autowired
	private StringRedisTemplate redisTemplate;

	public String sendMessage(String msg) {
		try {
			redisTemplate.convertAndSend(SubscriberConfig.PATTERNTOPIC, msg);
			return "消息发送成功了";
		} catch (Exception e) {
			e.printStackTrace();
			return "消息发送失败了";
		}
	}
}
