package com.llc.consumer;

import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class RedisConsumer2{

	public void receiveMessage(String message) {
        log.info("收到的mq消息2:" + message);
    }
}
