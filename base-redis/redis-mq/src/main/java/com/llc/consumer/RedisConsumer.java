package com.llc.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class RedisConsumer implements MessageListener {

	@Autowired
	private StringRedisTemplate redisTemplate;

	@Override
	public void onMessage(Message message, byte[] pattern) {
		RedisSerializer<String> valueSerializer = redisTemplate.getStringSerializer();
		String msg = valueSerializer.deserialize(message.getBody());
		log.info("收到的mq消息:" + msg);
	}
}
