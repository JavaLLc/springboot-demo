package com.llc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.llc.service.PublisherService;

@SpringBootApplication
@RestController
public class RedisMqApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedisMqApplication.class, args);
	}

	@Autowired
	private PublisherService publisherService;

	@RequestMapping("{msg}")
	public String sendMessage(@PathVariable("msg") String msg) {
		return publisherService.sendMessage(msg);
	}
}
