package com.llc.config;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

import com.llc.consumer.RedisConsumer;
import com.llc.consumer.RedisConsumer2;

/**
 * 消息订阅者配置类
 * @author LLC
 * @date 2019年6月13日
 * @Description:
 */
@Configuration
@AutoConfigureAfter({ RedisConsumer.class, RedisConsumer2.class })
public class SubscriberConfig {

	public static final String PATTERNTOPIC = "TOPIC_USERNAME";

	/**
	 * 消息监听适配器，注入接受消息方法，输入方法名字 反射方法
	 * @param RedisConsumer
	 * @return
	 */
	@Bean
	public MessageListenerAdapter getMessageListenerAdapter(RedisConsumer RedisConsumer) {
		return new MessageListenerAdapter(RedisConsumer);
	}

	/**
	@Bean
	public MessageListenerAdapter getMessageListenerAdapter2(RedisConsumer2 RedisConsumer) {
		// 当没有继承MessageListener时需要写方法名字
		return new MessageListenerAdapter(RedisConsumer, "receiveMessage");
	}
	*/

	/**
	 * 创建消息监听容器
	 *
	 * @param redisConnectionFactory
	 * @param messageListenerAdapter
	 * @return
	 */
	@Bean
	public RedisMessageListenerContainer getRedisMessageListenerContainer(RedisConnectionFactory redisConnectionFactory,
			MessageListenerAdapter messageListenerAdapter) {
		RedisMessageListenerContainer redisMessageListenerContainer = new RedisMessageListenerContainer();
		redisMessageListenerContainer.setConnectionFactory(redisConnectionFactory);
		redisMessageListenerContainer.addMessageListener(messageListenerAdapter, new PatternTopic(PATTERNTOPIC));
		return redisMessageListenerContainer;
	}
}
