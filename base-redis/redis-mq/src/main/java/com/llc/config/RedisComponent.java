package com.llc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

/**
 * StringRedisTemplate继承RedisTemplate
 * StringRedisTemplate只支持字符串类型
 * @author LLC
 * @date 2019年6月13日
 * @Description:
 */
@Component
public class RedisComponent {
	
	@Bean
    public StringRedisTemplate getRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
        return new StringRedisTemplate(redisConnectionFactory);
    }
	
}
