package com.llc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.llc.conf.RedisComponent;

@Controller
public class EchartsController {

	@Autowired
	private RedisComponent redisComponent;

	@ResponseBody
	@RequestMapping(value = "/hello")
	public String demo() {
		System.out.println("请求");
		redisComponent.set("hello", "哈喽");
		System.out.println(redisComponent.get("hello"));
		return "哈喽";
	}
}