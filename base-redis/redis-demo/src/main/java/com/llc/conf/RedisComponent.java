package com.llc.conf;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

/**  
 * <p>Description: redis组件</p>
 * @author cj
 * @email chenjing@richinfo.cn
 * @date 2018年4月12日 
 */ 
@Component
public class RedisComponent {
	
	@Autowired
	private RedisTemplate<Object, Object> redisTemplate;
	
	@Bean
    public RedisTemplate<Object, Object> redisTemplateInit(RedisTemplate<Object, Object> redisTemplate) {
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<Object>(Object.class));
        return this.redisTemplate = redisTemplate;
    }
	
    private Logger logger = Logger.getLogger(RedisComponent.class);
    
    /**
     * 判断键存在
     * <p>add by CJ 2018年4月9日</p>
     * @param key
     * @return
     */
    public boolean exists(final String key) {
        return redisTemplate.hasKey(key);
    }
    
    public void remove(final String key) {
        if (exists(key)) {
            redisTemplate.delete(key);
        }
    }

    public Object get(final String key) {  
        Object result = null;  
        ValueOperations<Object, Object> operations = redisTemplate.opsForValue();  
        result = operations.get(key);  
        return result;  
    }

    public boolean set(final String key, Object value) {
        boolean result = false;
        try {
            ValueOperations<Object, Object> operations = redisTemplate.opsForValue();
            operations.set(key, value);
            result = true;
        } catch (Exception e) {
        	logger.error("写入缓存异常:"+e.getMessage(), e);
        }
        return result;
    }  

    public boolean set(final String key, Object value, Long expireTime) {  
        boolean result = false;  
        try {  
            ValueOperations<Object, Object> operations = redisTemplate.opsForValue();  
            operations.set(key, value);  
            redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);  
            result = true;  
        } catch (Exception e) {  
        	logger.error("写入缓存并设置时间异常:"+e.getMessage(), e);
        }  
        return result;
    }  

    public Long getListLength(String key){
    	try {
			ListOperations<Object, Object> listOperations = redisTemplate.opsForList();
			return listOperations.size(key);
		} catch (Exception e) {
			logger.error("--查询redis列表的栈长时出错："+e.getMessage(), e);
			return 5l;
		}
    }
    
    /**
     * 压栈、入队
     * <p>add by CJ 2018年4月9日</p>
     * @param key
     * @param logObject
     * @param flag （L：压栈；R：入队）
     * @return
     */
    public Long pushList(String key, Object logObject,String flag){
    	Long result = -1l;
    	try {
    		ListOperations<Object, Object> listOperations = redisTemplate.opsForList();
        	if("L".equals(flag)){
        		result =listOperations.leftPush(key, logObject);
        	}else {
        		result = listOperations.rightPush(key, logObject);
        	}
		} catch (Exception e) {
			logger.error("--redis中存入list列表出错："+e.getMessage(), e);
		}
    	return result;
    	
    }

    /**
     * 出栈、出队（带清除redis中值）
     * <p>add by CJ 2018年4月9日</p>
     * @param key
     * @param flag （L：出栈（左/顶）；R：出队（右/底））
     * @return
     */
    public Object popList(String key,String flag){
    	Object result = null;
    	try {
    		ListOperations<Object, Object> listOperations = redisTemplate.opsForList();
    		if("L".equals(flag)){
    			result = listOperations.leftPop(key);
    		}else {
    			result = listOperations.rightPop(key);
    		}
		} catch (Exception e) {
			logger.error("--redis中获取list列表出错："+e.getMessage(), e);
		}
    	return result;
    };
    
    /**
     * 获取指定范围内的数据
     * <p>add by CJ 2018年4月9日</p>
     * @param key
     * @param start
     * @param end
     * @return
     */
    public List<Object> apointRangList(String key, Long start, Long end){
    	List<Object> apointRangObject = null;
    	try {
    		ListOperations<Object, Object> listOperations = redisTemplate.opsForList();
    		apointRangObject = listOperations.range(key, start, end);
		} catch (Exception e) {
			logger.error("--redis中获取指定范围内list列表出错："+e.getMessage(), e);
		}
    	return apointRangObject;
    }

    public void trim(String key, long start, long end) {
    	ListOperations<Object, Object> listOperations = redisTemplate.opsForList();
    	listOperations.trim(key, start, end);
    }
}
