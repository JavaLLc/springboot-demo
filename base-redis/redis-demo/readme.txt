springboot集成redis
1.依赖
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-web</artifactId>
</dependency>
2.工具类
RedisComponent
@Autowired
private RedisTemplate<Object, Object> redisTemplate;
