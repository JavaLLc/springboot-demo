package com.llc.service.impl;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.llc.entity.UserEntity;
import com.llc.service.ICacheService;

import lombok.extern.log4j.Log4j2;

/**
 * cacheNames:设置ehcache的名称，这个名称必须在ehcache.xml已配置
 * @author LLC
 * @date 2019年6月12日
 * @Description:
 */
@Service
@Log4j2
@CacheConfig(cacheNames = "cacheStrategy")
public class CacheServiceImpl implements ICacheService {

	/**
	 * #p0为方法参数名 0代表参数的索引
	 * @param name
	 * @return
	 */
	@Cacheable(key = "#p0")
	public UserEntity findUserByName(String name) {
		log.info(name);
		System.out.println("缓存中没有则会执行这个方法, 去数据库中查询");
		UserEntity user = new UserEntity();
		user.setName(name);
		return user;
	}

	/**
	 * 方法名
	 */
	@CachePut(key = "#root.methodName")
	public UserEntity save(UserEntity user) {
		System.out.println("更新缓存");
		user.setName("llc");
		return user;
	}

	@CacheEvict(key = "#p0")
	public boolean deleteByName(String name) {
		System.out.println("清空缓存操作,将会删除缓存");
		return true;
	}

}
