package com.llc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.llc.entity.UserEntity;
import com.llc.service.ICacheService;

import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class CacheServiceController {

	@Autowired
	private ICacheService cacheService;

	@GetMapping(value = "/find")
	public UserEntity findByName(String name) {
		log.info(name);
		UserEntity entity = cacheService.findUserByName(name);
		return entity;
	}

	@GetMapping(value = "/add")
	public String save() {
		log.info("新增操作");
		UserEntity entity = new UserEntity();
		entity.setId(1L);
		entity.setName("张三");
		entity.setAge(18);
		cacheService.save(entity);
		return "success";
	}

	@GetMapping(value = "/delete")
	public String delete(String name) {
		log.info("删除操作" + name);
		cacheService.deleteByName(name);
		return "success";
	}

}
