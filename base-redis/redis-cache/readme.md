## redis+cache(参考:https://www.cnblogs.com/ashleyboy/p/9595584.html)
#####1.使用
```
在启动类添加注解@EnableCaching
在service层添加缓存的位置
@CacheConfig(cacheNames = "cacheStrategy") // 读取缓存配置
@Cacheable(key = "#p0") // 启用缓存
@CachePut(key = "#p0") // 更新缓存
@CacheEvict(key = "#p0") // 删除缓存
```
#####2.测试
```
1.访问http://localhost:8081/find?name=hello
打印:缓存中没有则会执行这个方法, 去数据库中查询
再次访问http://localhost:8081/find?name=hello
没有再打印,表示缓存成功

2.访问http://localhost:8081/add 此时方法名save作为key
访问http://localhost:8081/find?name=save
发现没有打印那句话,说明缓存成功

再访问http://localhost:8081/delete?name=save 删除缓存
再次访问http://localhost:8081/find?name=save
打印：缓存中没有则会执行这个方法, 去数据库中查询
说明缓存删除成功

在redis客户端可以查看相应的key与值
```




