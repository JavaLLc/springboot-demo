package com.llc.advice;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.llc.vo.ResultVo;

import lombok.extern.log4j.Log4j2;

/**
 * 自定义返回数据类型
 * 统一返回格式
 * @author LLC
 * @date 2019年6月5日
 * @Description:
 */
@Log4j2
@RestControllerAdvice
public class ControllerResponseHandler implements ResponseBodyAdvice<Object> {

	/**
	 * 判断哪些需要拦截 true表示拦截
	 */
	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		log.info("方法名:" + returnType);
		return true;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		log.info(body);
		if (body != null && body instanceof ResultVo) {
			return body;
		} else {
			return new ResultVo("0", body, "系统异常");
		}
	}

}
