package com.llc.advice;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.llc.exception.MyException;

/**
 * @Date: 2019-06-01
 * @Description: 全局捕获异常和自定义全局捕获异常
 * 当定义了多个捕捉异常方法,匹配度高的优先级高
 */
//不指定包默认加了@Controller和@RestController注解的都能控制
//@ControllerAdvice与@RestControllerAdvice关系类似@Controller和@RestController
@ControllerAdvice
//@ControllerAdvice(basePackages ="com.llc.controller")
public class MyControllerAdvice {

	/**
	 * 全局异常处理，反正异常返回统一格式
	 * @param ex
	 * @return
	 */
	@ResponseBody
	@ExceptionHandler(value = Exception.class)
	public Map<String, Object> exceptionHandler(Exception ex) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", 500);
		map.put("mag", ex.getMessage());
		// 异常日志入库,省略
		return map;
	}

	/**
	* 拦截捕捉自定义异常 MyException.class
	* @param ex
	* @return
	*/
	@ResponseBody
	@ExceptionHandler(value = MyException.class)
	public Map<String, Object> myExceptionHandler(MyException ex) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", ex.getCode());
		map.put("message", ex.getMessage());
		map.put("method", ex.getMethod());
		map.put("descinfo", ex.getDescinfo());
		// 异常日志入库,省略
		return map;
	}
}
