package com.llc.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.llc.exception.MyException;

@RestController
public class AdviceController {

	@RequestMapping("/hello")
	public String demo() {
		int i = 1 / 0;
		return "hello" + i;
	}

	/**
	 * 自定义异常
	 * @return
	 */
	@RequestMapping("/hello2")
	public String demo2() {
		String str = "fail";
		if ("fail".equals(str)) {
			throw new MyException("1001", "fail", "/AdviceController/demo2", "自定义异常");
		}
		return "hello";
	}

}
