package com.llc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControlleradviceDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControlleradviceDemoApplication.class, args);
	}

}
