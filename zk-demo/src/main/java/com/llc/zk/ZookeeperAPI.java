package com.llc.zk;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.KeeperState;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

/**
 * Znode是ZooKeeper集合的核心组件，ZooKeeper API提供了一小组方法使用ZooKeeper集合来操纵znode的所有细节
 * @author lenovo
  *         连接到ZooKeeper集合。ZooKeeper集合为客户端分配会话ID。
  *         定期向服务器发送心跳。否则，ZooKeeper集合将过期会话ID，客户端需要重新连接。
  *         只要会话ID处于活动状态，就可以获取/设置znode。
  *         所有任务完成后，断开与ZooKeeper集合的连接。如果客户端长时间不活动，则ZooKeeper集合将自动断开客户端。
 * 
  *         方法: 
 *         connect - 连接到ZooKeeper集合
 *         create- 创建znode
 *         exists- 检查znode是否存在及其信息
 *         getData - 从特定的znode获取数据
 *         setData - 在特定的znode中设置数据
 *         getChildren - 获取特定znode中的所有子节点
 *         delete - 删除特定的znode及其所有子项
 *         close - 关闭连接
 */
public class ZookeeperAPI {

	final CountDownLatch connectedSignal = new CountDownLatch(1);
	final static CountDownLatch connectedSignal2 = new CountDownLatch(1);

	/**
	 *   连接到ZooKeeper集合
	 *  connectionString - ZooKeeper集合主机。127.0.0.1:2181
		sessionTimeout - 会话超时（以毫秒为单位）。
		watcher - 实现“监视器”界面的对象。ZooKeeper集合通过监视器对象返回连接状态。
	 */
	public ZooKeeper connect(String host) throws IOException, InterruptedException {
		ZooKeeper zk = new ZooKeeper(host, 5000, new Watcher() {
			public void process(WatchedEvent we) {
				if (we.getState() == KeeperState.SyncConnected) {
					connectedSignal.countDown();
				}
			}
		});
		connectedSignal.await();
		return zk;
	}

	/**
	 *  创建节点
	 * @param zk-ZooKeeper集合
	 * @param path-Znode路径。例如，/myapp1
	 * @param data-要存储在指定znode路径中的数据
	 */
	public static void create(ZooKeeper zk, String path, byte[] data) throws KeeperException, InterruptedException {
		// 要创建的节点的访问控制列表-静态接口 ZooDefs.Ids 来获取一些基本的acl列表
		// createMode -节点的类型，即临时，顺序或两者
		Stat stat = znode_exists(zk, path);// 是否存在
		if (stat == null) {
			System.out.println("创建新节点");
			/**
			 * 有四种类型的znode： 
			1、PERSISTENT-持久化目录节点 
			客户端与zookeeper断开连接后，该节点依旧存在 
			
			2、PERSISTENT_SEQUENTIAL-持久化顺序编号目录节点 
			客户端与zookeeper断开连接后，该节点依旧存在，只是Zookeeper给该节点名称进行顺序编号 
			
			3、EPHEMERAL-临时目录节点 
			客户端与zookeeper断开连接后，该节点被删除 
			
			4、EPHEMERAL_SEQUENTIAL-临时顺序编号目录节点 
			客户端与zookeeper断开连接后，该节点被删除，只是Zookeeper给该节点名称进行顺序编号 
			 */
			zk.create(path, data, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
		}
	}

	/**
	 *  查询节点是否存在
	 * @param zk
	 * @param path-Znode路径
	 */
	public static Stat znode_exists(ZooKeeper zk, String path) throws KeeperException, InterruptedException {
		// watcher - 布尔值，用于指定是否监视指定的znode
		return zk.exists(path, true);
	}

	/**
	 * 获取数据
	 * getData(String path, Watcher watcher, Stat stat) 
	 * -watcher 监视器类型的回调函数。当指定的znode的数据改变时，ZooKeeper集合将通过监视器回调进行通知。这是一次性通知
	 * -stat 返回znode的元数据
	 * @param zk
	 * @param path
	 */
	public static String znode_getData(ZooKeeper zk, String path)
			throws KeeperException, InterruptedException, Exception {
		byte[] b = zk.getData(path, new Watcher() {
			@SuppressWarnings("incomplete-switch")
			@Override
			public void process(WatchedEvent we) {
				if (we.getType() == Event.EventType.None) {
					switch (we.getState()) {
					case Expired:
						connectedSignal2.countDown();
						break;
					}
				} else {
					try {
						byte[] bn = zk.getData(path, false, null);
						String data = new String(bn, "UTF-8");
						System.out.println(data);
						connectedSignal2.countDown();
					} catch (Exception ex) {
						System.out.println(ex.getMessage());
					}
				}
			}
		}, null);
		String data = new String(b, "UTF-8");
		return data;
	}

	/**
	 * 更新节点数据
	 * @param zk
	 * @param path
	 * @param data
	 */
	public static void update(ZooKeeper zk, String path, byte[] data) throws KeeperException, InterruptedException {
		zk.setData(path, data, zk.exists(path, true).getVersion());
	}

	/**
	 *	getChildren(String path, Watcher watcher)
	 *	获取特定znode的所有子节点
	 * @param zk
	 * @param path
	 */
	public static List<String> getChildren(ZooKeeper zk, String path) throws KeeperException, Exception {
		List<String> children = zk.getChildren(path, false);
		for (int i = 0; i < children.size(); i++) {
			System.out.println(children.get(i));
		}
		return children;
	}

	/**
	 * 删除Znode
	 * @param zk
	 * @param path
	 */
	public static void delete(ZooKeeper zk, String path) throws KeeperException, InterruptedException {
		Stat stat = znode_exists(zk, path);// 是否存在
		if (stat != null) {
			System.out.println("删除节点");
			zk.delete(path, stat.getVersion());
		}
	}

	/**
	 * 关闭连接
	 * @param zk
	 * @throws InterruptedException
	 */
	public static void close(ZooKeeper zk) {
		try {
			if (zk != null) {
				zk.close();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		// 集合主机
		// String host = "192.168.5.133:2181";
		String host = "192.168.5.133:2181";
		String path = "/llc";
		byte[] data = "存放的数据".getBytes();

		ZookeeperAPI demo = new ZookeeperAPI();
		ZooKeeper zk = null;
		try {
			// 1.连接
			zk = demo.connect(host);
			// 2.创建
			create(zk, path, data);
			// 3.是否存在
			Stat exists = znode_exists(zk, path);
			if (exists != null) {
				System.out.println("节点存在且版本号为- " + exists.getVersion());
				// 4.获取数据
				String getData = znode_getData(zk, path);
				System.out.println("获得节点数据为:" + getData);

				// 5.更新节点数据
				byte[] newData = "新的数据".getBytes();
				update(zk, path, newData);
				connectedSignal2.await();
				String getData2 = znode_getData(zk, path);
				System.out.println("更新后获得节点数据为:" + getData2);

				// 6.获得子节点名称
				List<String> list = getChildren(zk, path);
				System.out.println("子节点:" + list);

				// 7.删除节点(有子节点的节点无法删除,需要先删除子目录再删除根目录)
				delete(zk, path);

			} else {
				System.out.println("节点不存在");
			}

		} catch (

		Exception e) {
			e.printStackTrace();
		} finally {
			// 8.关闭连接
			close(zk);
		}
	}
}
