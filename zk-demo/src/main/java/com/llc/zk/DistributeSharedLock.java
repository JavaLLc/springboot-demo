package com.llc.zk;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

/**
 * 分布式锁
 * 
获取分布式锁的总体思路
a、在获取分布式锁的时候在locker节点下创建临时顺序节点，释放锁的时候删除该临时节点。
b、客户端调用createNode方法在locker下创建临时顺序节点，然后调用getChildren(“locker”)来获取locker下面的所有子节点，注意此时不用设置任何Watcher。
c、客户端获取到所有的子节点path之后，如果发现自己创建的子节点序号最小，那么就认为该客户端获取到了锁。
d、如果发现自己创建的节点并非locker所有子节点中最小的，说明自己还没有获取到锁，此时客户端需要找到比自己小的那个节点，然后对其调用exist()方法，同时对其注册事件监听器。
e、之后，让这个被关注的节点删除，则客户端的Watcher会收到相应通知，此时再次判断自己创建的节点是否是locker子节点中序号最小的，如果是则获取到了锁，如果不是则重复以上步骤继续获取到比自己小的一个节点并注册监听。
 * @author lenovo Ctrl+Shift+o快速导入包
 */
public class DistributeSharedLock implements Watcher {
	// 原生API
	ZooKeeper zooKeeper = null;
	// 定义根节点
	private String root = "/locks";
	// 当前获取节点
	private String currentZnode;
	// 当前等待节点
	private String waitZnode;
	// 发令枪
	private CountDownLatch countDownLatch;
	// 连接地址,集群逗号连接
	private static final String CONNECTION_STRING = "192.168.5.133:2181";
	// 超时时间
	private static final int SESSION_TIMEOUT = 5000;

	/**
	 * 构造函数
	 * 
	 * @param config
	 */
	public DistributeSharedLock(String config) {
		try {
			zooKeeper = new ZooKeeper(config, SESSION_TIMEOUT, this);
			// 查看root节点是否存在
			Stat stat = zooKeeper.exists(root, false);
			// 不存在则创建根节点
			if (stat == null) {
				zooKeeper.create(root, new byte[0], ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (KeeperException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取锁
	 */
	public void lock() {
		if (tryLock()) {
			System.out.println("线程-" + Thread.currentThread().getName() + "拥有锁");
			return;
		}
		try {
			// 等待并获取锁
			waitLock(waitZnode, SESSION_TIMEOUT);
		} catch (KeeperException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 释放锁
	 */
	public void unLock() {
		System.out.println("线程" + Thread.currentThread().getName() + "释放锁");
		try {
			zooKeeper.delete(currentZnode, -1);
			currentZnode = null;
			zooKeeper.close();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (KeeperException e) {
			e.printStackTrace();
		}
	}

	private boolean tryLock() {
		// /locks/lock_0000000001
		String spliteStr = "lock_";
		try {
			currentZnode = zooKeeper.create(root + "/" + spliteStr, new byte[0], ZooDefs.Ids.OPEN_ACL_UNSAFE,
					CreateMode.EPHEMERAL_SEQUENTIAL);
			System.out.println("Thread " + Thread.currentThread().getName() + " create success: " + currentZnode);
			// 获取根节点下所有子节点
			List<String> childNodes = zooKeeper.getChildren(root, false);
			// 排序
			Collections.sort(childNodes);
			// 如果当前节点的值是最小值,则获得锁
			if (currentZnode.equals(root + "/" + childNodes.get(0))) {
				return true;
			}

			// 否则,监听比自己小的节点
			String subNode = currentZnode.substring(currentZnode.lastIndexOf("/") + 1);// lock_0000000003
			waitZnode = childNodes.get(Collections.binarySearch(childNodes, subNode) - 1);// lock_0000000002
		} catch (KeeperException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return false;
	}

	// 等待
	private boolean waitLock(String lower, long waitTime) throws KeeperException, InterruptedException {
		Stat stat = zooKeeper.exists(root + "/" + lower, true);
		// 存在比自己小的节点,开始等待
		if (stat != null) {
			System.out.println("线程-" + Thread.currentThread().getName() + "-等待: " + root + "/" + lower);
			this.countDownLatch = new CountDownLatch(1); // 实例化计数器，让当前的线程等待
			this.countDownLatch.await(waitTime, TimeUnit.MILLISECONDS);
			this.countDownLatch = null;
		}

		return true;
	}

	/**
	 * 比当前节点小的临时节点删除时触发,计数器-1
	 * 
	 * @param watchedEvent
	 */
	@Override
	public void process(WatchedEvent watchedEvent) {
		if (this.countDownLatch != null) { // 如果计数器不为空话话，释放计数器锁
			this.countDownLatch.countDown();
		}
	}

	public static void main(String[] args) {
		ExecutorService executorService = Executors.newCachedThreadPool();
		final Semaphore semaphore = new Semaphore(10);
		for (int i = 0; i < 10; i++) {
			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					try {
						semaphore.acquire();
						DistributeSharedLock sharedLock = new DistributeSharedLock(CONNECTION_STRING);
						sharedLock.lock();
						// 业务代码
						Thread.sleep(3000);
						sharedLock.unLock();
						semaphore.release();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
			executorService.execute(runnable);
		}
		executorService.shutdown();
	}
}
