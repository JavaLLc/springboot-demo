package com.llc.zk;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.exception.ZkInterruptedException;
import org.I0Itec.zkclient.exception.ZkNoNodeException;
import org.I0Itec.zkclient.exception.ZkNodeExistsException;
import org.I0Itec.zkclient.serialize.SerializableSerializer;

import com.llc.entity.ServerData;

/**
 * Master选举
 * @author lenovo 
 * 	  读取数据
 *         byte[] readData(String path); 
 *         byte[] readData(String path, boolean returnNullIfPathNotExists); 
 *         byte[] readData(String path, Stat stat);
 *         
 *         写入数据
 *         Stat writeData(String path, byte[] data); 
 *         Stat writeData(String path,
 *         byte[] data, 
 *         int expectedVersion); 
 *         
 *         创建节点
 *         void createPersistent(String path); 
 *         void createPersistent(String path, boolean createParents); 
 *         void createPersistent(String path,byte[] data); 
 *         String createPersistentSequential(String path, byte[] data); 
 *         void createEphemeral(final String path); 
 *         void createEphemeral(final String path, final byte[] data); 
 *         String createEphemeralSequential(final String path, final byte[] data);
 *         String create(final String path, byte[] data, final CreateMode mode);
 *         
 *         删除节点
 *         boolean delete(final String path); 
 *         boolean deleteRecursive(String path); 
 *         
 *         查询节点
 *         boolean exists(final String path); 
 *         List<String> getChildren(String path); 
 *         long getCreationTime(String path); int countChildren(String path);
 *         
 *         
 *         订阅事件
 *        zkclient的强大之处不在于基本zookeeper api操作，而在于事件监听机制，也就是zookeeper的watches。
 *        Zookeeper的watcher在使用上存在一次性、session过期等难点，因此zkclient对这些问题进行了封装和屏蔽。

zkclient一共定义了三种事件：
接口：com.github.zkclient.IZkStateListener  接口的定义实现了事件发生时，如果进行处理，如进行打印消息。。
　　　　public void handleStateChanged(KeeperState state) throws Exception;
　　　　public void handleNewSession() throws Exception;
 *    IZkStateListener 定义了两种事件，一种是连接状态的改变，例如由未连接改变成连接上，连接上改为过期等；
 *        另一种创建一个新的session（连接）， 通常是由于session失效然后新的session被建立时触发。一般此时需要开发者重新创建临时节点（Ephemeral Nodes）。


接口：com.github.zkclient.IZkDataListener
　　　　public void handleDataChange(String dataPath, byte[] data) throws Exception;
　　　　public void handleDataDeleted(String dataPath) throws Exception;
 *        IZkDataListener 也定义了两种事件，一种是节点数据的变化，另一种是节点被删除。



接口：com.github.zkclient.IZkChildListener
　　　　public void handleChildChange(String parentPath, List currentChildren) throws Exception;
 *        IZkChildListener 定义了一种事件，描述子节点变化了，这时候获取到的是新的子节点列表。如果此节点被删除，那么子节点列表是null（非空列表）。

 *        IZkClient能够非常方便的订阅（subscribe）这三种事件：
 *        void subscribeStateChanges(IZkStateListener listener); // listener 进行消息处理
 *        void subscribeDataChanges(String path, IZkDataListener listener);
 *        List subscribeChildChanges(String path, IZkChildListener listener);
 *         而zkclient最强大之处在于，当发送session失效时能够自动重新订阅这些事件，而不需要开发者重新订阅。
 */
public class MasterVote {
	// zkclient客户端
	private ZkClient zkClient;

	// 争抢的master节点
	private static final String MASTER_NODE = "/master";

	// 连接地址
	private static final String CONNECTION_STRING = "192.168.5.133:2181";

	// 超时时间
	private static final int SESSION_TIMEOUT = 5000;

	// 争抢master的服务器节点信息
	private ServerData serverData;

	// 争抢到master节点的服务器信息
	private ServerData masterData;

	// 服务器开启状态
	private volatile boolean running = false;

	// master节点监听事件
	IZkDataListener dataListener;

	/**
	 * 创建一个定长线程池，支持定时及周期性任务执行
	 */
	private ScheduledExecutorService scheService = Executors.newScheduledThreadPool(1);

	/**
	 * 	初始化
	 * @param zkclient zk客户端
	 * @param serverData
	 */
	public MasterVote(ZkClient zkclient, ServerData serverData) {
		this.zkClient = zkclient;
		this.serverData = serverData;
		// 监听节点:定义了两种事件，一种是节点数据的变化，另一种是节点被删除
		dataListener = new IZkDataListener() {
			// 节点数据的变化
			@Override
			public void handleDataChange(String s, Object o) throws Exception {
				System.out.println("节点数据被改变了");
			}

			// 节点被删除
			@Override
			public void handleDataDeleted(String s) throws Exception {
				System.out.println("节点被删除了");
			}
		};
	}

	/**
	 * 开始争抢master
	 */
	public void start() {
		if (running) {
			throw new RuntimeException("服务器在启动状态!");
		}
		running = true;
		// 触发监听器-只监听节点数据的变化
		zkClient.subscribeDataChanges(MASTER_NODE, dataListener);
		// 争抢master节点
		takeMaster();
	}

	/**
	 * 停止争抢master
	 */
	public void stop() {
		if (!running) {
			throw new RuntimeException("服务器在停止状态");
		}
		running = false;
		zkClient.subscribeDataChanges(MASTER_NODE, dataListener);
		releaseMaster();
	}

	/**
	 * 争抢master节点的具体实现
	 */
	private void takeMaster() {
		if (!running) {
			throw new RuntimeException("服务器没有启动");
		}
		System.out.println(serverData.getServerName() + "开始抢master节点");
		try {
			// 创建master节点成功,该服务器为master,记录master节点信息
			zkClient.createEphemeral(MASTER_NODE, serverData);
			masterData = serverData;
			System.out.println(masterData.getServerName() + "成功抢到master!");
			// 每五秒释放一次
			scheService.schedule(new Runnable() {
				@Override
				public void run() {
					releaseMaster();
				}
			}, 10, TimeUnit.SECONDS);
		}
		// ZkNode节点已经存在,说明master节点已经存在
		catch (ZkNodeExistsException e) {
			// 读取当前master节点的信息
			ServerData currentMasterData = zkClient.readData(MASTER_NODE);
			// 读取的过程中master节点已经被释放
			if (currentMasterData == null) {
				takeMaster();
			} else {
				masterData = currentMasterData;
			}
		}
	}

	/**
	 * 释放master节点
	 */
	private void releaseMaster() {
		if (checkMaster()) {
			zkClient.delete(MASTER_NODE);
			System.out.println("释放===================================");
		}
	}

	/**
	 * 校验当前的服务器是否是master
	 */
	private boolean checkMaster() {
		try {
			// 读取当前master节点数据
			ServerData currentMasterData = zkClient.readData(MASTER_NODE);
			// masterData = currentMasterData;
			// master的服务名等于当前争抢的服务器名
			if (currentMasterData.getServerName().equals(masterData.getServerName())) {
				System.out.println("校验当前的服务器是否是master-是");
				return true;
			} else {
				return false;
			}
		} catch (ZkNoNodeException e) {
			return false;
		}
		// 因网络打断出现的异常
		catch (ZkInterruptedException e) {
			return checkMaster();
		} catch (Exception e) {
			return false;
		}
	}

	public static void main(String[] args) {
		// 线程池
		ExecutorService service = Executors.newCachedThreadPool();
		// 信号量-控制的是线程并发的数量
		final Semaphore semaphore = new Semaphore(10);
		for (int i = 0; i < 7; i++) {
			final int idx = i;
			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					try {
						semaphore.acquire();
						// 初始化
						ZkClient zk = new ZkClient(CONNECTION_STRING, SESSION_TIMEOUT, SESSION_TIMEOUT,
								new SerializableSerializer());
						// 定义争抢的服务器信息
						ServerData serverData = new ServerData(idx, "服务器-" + idx);
						// 初始化-监听
						MasterVote mv = new MasterVote(zk, serverData);
						// 开始争抢
						mv.start();
						// 释放线程
						semaphore.release();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
			// 执行
			service.execute(runnable);
		}
		// 线程池关闭
		service.shutdown();
	}
}