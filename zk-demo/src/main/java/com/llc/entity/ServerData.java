package com.llc.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ServerData implements Serializable {

	private static final long serialVersionUID = 1L;
	private int serverId;
	private String serverName;

}
