package com.llc.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Console;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.math.MathUtil;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.swing.clipboard.ClipboardUtil;
import cn.hutool.core.text.StrBuilder;
import cn.hutool.core.text.StrSpliter;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.HexUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.RuntimeUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.poi.excel.BigExcelWriter;
import cn.hutool.poi.excel.ExcelUtil;
import lombok.Getter;

/**
 * 一些常用工具类的使用
 * @author LLC
 * @date 2019年6月10日
 * @Description:
 */
@Getter
public class HutoolDemo {

	public static void main(String[] args) {
		// 字符串 hasBlank则会把不可见字符(空格)也算做空
		String str = " ";
		System.out.println("是否为空" + StrUtil.hasBlank(str));
		System.out.println("是否为空" + StrUtil.hasEmpty(str));
		System.out.println("是否为空" + StrUtil.isBlank(str));
		System.out.println("是否为空" + StrUtil.isEmpty(str));

		// 16进制工具-HexUtil 常用于：1、图片的字符串表现形式 2、加密解密 3、编码转换
		String str2 = "我是一个字符串";
		String hex = HexUtil.encodeHexStr(str2, CharsetUtil.CHARSET_UTF_8);
		System.out.println(hex);
		// 解码后与str相同
		System.out.println(HexUtil.decodeHexStr(hex));

		// 剪贴板工具-ClipboardUtil
		ClipboardUtil.setStr("设置剪贴板内容-字符串");
		System.out.println(ClipboardUtil.getStr());

		// 执行系统命令
		System.out.println(RuntimeUtil.execForStr("ipconfig"));

		// 数字工具-NumberUtil
		System.out.println(NumberUtil.toStr(13.000000));

		// 数组工具-ArrayUtil
		int[] a = {};
		int[] b = null;
		System.out.println(ArrayUtil.isEmpty(a));
		System.out.println(ArrayUtil.isEmpty(b));

		// 随机工具-RandomUtil
		System.out.println(RandomUtil.randomNumbers(7));

		// 网络工具-NetUtil
		System.out.println(NetUtil.getIpByHost("www.baidu.com"));

		// 唯一ID工具-IdUtil
		System.out.println(IdUtil.randomUUID());
		System.out.println(IdUtil.simpleUUID());

		// 压缩工具-ZipUtil
		// ZipUtil.zip("D:/gen");

		// 正则工具-ReUtil
		System.out.println(ReUtil.getFirstNumber("kk17ko259"));

		// 控制台打印封装-Console
		Console.log("打印");

		// 字段验证器-Validator
		Console.log(Validator.isEmail("loolly@gmail.com"));

		//Bean工具-BeanUtil Bean转为Map:BeanUtil.beanToMap Bean转Bean:BeanUtil.copyProperties
		
		//Map工具-MapUtil
		Map<Object, Object> colorMap1 = MapUtil.of(new String[][] {
		     {"a", "#1"},{"b", "#2"},{"c", "#3"}
		});
		Map<Object, Object> colorMap2 = MapUtil.of(new String[][] {
		     {"a", "#1"},{"b", "#2"}
		});
		List<Map<Object, Object>> list = new ArrayList<>();
		list.add(colorMap1);
		list.add(colorMap2);
		//行列转换
		Map<Object, List<Object>> map = MapUtil.toListMap(list);
		Console.log(map);
		
		//CSV文件处理工具-CsvUtil
		//指定路径和编码 写出
		//CsvUtil.getWriter("D:/testWrite.csv", CharsetUtil.CHARSET_UTF_8).write(new String[] {"a1", "b1", "c1"}, new String[] {"a2", "b2", "c2"}, new String[] {"a3", "b3", "c3"});
		
		//可复用字符串生成器-StrBuilder
		Console.log(StrBuilder.create().append("a").append("b"));
		
		//字符串切割-StrSpliter
		//参数：被切分字符串，分隔符逗号，0表示无限制分片数，去除两边空格，忽略空白项
		Console.log(StrSpliter.split("a, ,efedsfs,   ddf", ',', 0, true, true));
		
		//数学相关-MathUtil 5*4*3*2*1
		Console.log(MathUtil.arrangementCount(5));
		
		//线程工具-ThreadUtil
		//ThreadUtil.execAsync(()->{System.out.println("异步执行");});
		
		//图片工具-ImgUtil
		//翻转
		//ImgUtil.flip( FileUtil.file("D:/tj.png"), FileUtil.file("D:/tj-new.png"));
		
		//加密解密工具-SecureUtil
		Console.log(SecureUtil.md5("md5摘要"));
		
		//切面代理工具-ProxyUtil
		
		//Excel大数据生成-BigExcelWriter
		List<?> row1 = CollUtil.newArrayList("aa", "bb", "cc", "dd", DateUtil.date(), 3.22676575765);
		List<?> row2 = CollUtil.newArrayList("aa1", "bb1", "cc1", "dd1", DateUtil.date(), 250.7676);
		List<?> row3 = CollUtil.newArrayList("aa2", "bb2", "cc2", "dd2", DateUtil.date(), 0.111);
		List<?> row4 = CollUtil.newArrayList("aa3", "bb3", "cc3", "dd3", DateUtil.date(), 35);
		List<?> row5 = CollUtil.newArrayList("aa4", "bb4", "cc4", "dd4", DateUtil.date(), 28.00);
		List<List<?>> rows = CollUtil.newArrayList(row1, row2, row3, row4, row5);
		BigExcelWriter writer= ExcelUtil.getBigWriter("D:/cs.xlsx");
		// 一次性写出内容，使用默认样式
		//writer.write(rows);
		// 关闭writer，释放内存
		//writer.close();
		
		
	}
}
