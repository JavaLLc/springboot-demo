#SpringBoot SpringBoot集成RabbitMQ 邮件发送
-------------------------------------------------------------

###lombok提供的注解:
 *  @Data：注解在类上；提供类所有属性的 getting 和 setting 方法，此外还提供了equals、canEqual、hashCode、toString 方法 
 * 	@Setter：注解在属性上；为属性提供 setting 方法 
 * 	@Getter：注解在属性上；为属性提供 getting 方法 
 * 	@SneakyThrows：无需在签名处显式抛出异常 
 * 	@Log4j：注解在类上；为类提供一个 属性名为log 的 log4j 日志对像 
 * 	@Slf4j: 同上 
 * 	@NoArgsConstructor：注解在类上；为类提供一个无参的构造方法 
 * 	@AllArgsConstructor：注解在类上；为类提供一个全参的构造方法
-------------------------------------------------------------
 
@ConfigurationProperties: 把配置文件的信息,读取并自动封装成实体类，
使用@ConfigurationProperties,可以把同类的配置信息自动封装成实体类
-------------------------------------------------------------

测试类:背包1-0问题，算法

