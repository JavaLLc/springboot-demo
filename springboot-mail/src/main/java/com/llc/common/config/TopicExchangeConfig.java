package com.llc.common.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.llc.common.constant.RabbitConstant;

/**
 */
@Configuration
public class TopicExchangeConfig {
	/**
	 * 声明队列
	 * @return
	 */
	@Bean
	public Queue emailQueue() {
		// true表示持久化该队列
		return new Queue(RabbitConstant.EMAIL_QUEUE, true);
	}

	@Bean
	public Queue httpRequestQueue() {
		return new Queue(RabbitConstant.MESSAGE_QUEUE, true);
	}

	/**
	 * 声明交换机
	 *
	 * @return
	 */
	@Bean
	public TopicExchange topicExchange() {
		return new TopicExchange(RabbitConstant.CONTROL_EXCHANGE);
	}

	/**
	 * 绑定
	 *
	 * @return
	 */
	@Bean
	public Binding bindingEmail() {
		// *表示一个词,#表示零个或多个词
		return BindingBuilder.bind(emailQueue()).to(topicExchange()).with(RabbitConstant.EMAIL_ROUTING_KEY);
	}

	@Bean
	public Binding bindingHttpRequest() {
		return BindingBuilder.bind(httpRequestQueue()).to(topicExchange()).with(RabbitConstant.MESSAGE_ROUTING_KEY);
	}

}
