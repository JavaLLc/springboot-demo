package com.llc.common.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 *  @Data：注解在类上；提供类所有属性的 getting 和 setting 方法，此外还提供了equals、canEqual、hashCode、toString 方法 
 * 	@Setter：注解在属性上；为属性提供 setting 方法 
 * 	@Getter：注解在属性上；为属性提供 getting 方法 
 * 	@SneakyThrows：无需在签名处显式抛出异常 
 * 	@Log4j：注解在类上；为类提供一个 属性名为log 的 log4j 日志对像 
 * 	@Slf4j: 同上 
 * 	@NoArgsConstructor：注解在类上；为类提供一个无参的构造方法 
 * 	@AllArgsConstructor：注解在类上；为类提供一个全参的构造方法
 * 
 * 把配置文件的信息,读取并自动封装成实体类，
 * 使用@ConfigurationProperties,可以把同类的配置信息自动封装成实体类
 */
@Configuration
@Slf4j
@Data // 自动生成get/set方法
@ConfigurationProperties(prefix = "spring.rabbitmq")
public class RabbitConfig {

	private String host;
	private String port;
	private String username;
	private String password;
	private String virtualHost;
	private String connectionTimeout;
	private boolean publisherConfirms;
	private boolean publisherReturns;

	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
		connectionFactory.setAddresses(host + ":" + port);
		connectionFactory.setUsername(username);
		connectionFactory.setPassword(password);
		connectionFactory.setVirtualHost(virtualHost);
		connectionFactory.setConnectionTimeout(Integer.parseInt(connectionTimeout));
		connectionFactory.setChannelCacheSize(1);// 连接数

		// 如果要进行消息回调,则这里必须要设置为true
		connectionFactory.setPublisherConfirms(publisherConfirms);
		connectionFactory.setPublisherReturns(publisherReturns);

		return connectionFactory;
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public RabbitTemplate rabbitTemplate() {
		log.info("");
		RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());
		rabbitTemplate.setEncoding("UTF-8");
		/**
		 * 使用return-callback时必须设置mandatory为true，或者在配置中设置mandatory-expression的值为true，
		 * 可针对每次请求的消息去确定’mandatory’的boolean值，只能在提供’return -callback’时使用
		 */
		rabbitTemplate.setMandatory(true);
		// 确认消息是否到达broker服务器，也就是只确认是否正确到达exchange中即可，只要正确的到达exchange中，broker即可确认该消息返回给客户端ack。
		/*
		 * rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
		 * log.info("", correlationData); log.info("", ack); log.info("", cause); if
		 * (ack) { log.info("交换机接收信息成功,id:{}", correlationData.getId()); } else {
		 * log.error("交换机接收信息失败:{}", cause); } });
		 */

		return rabbitTemplate;
	}
}
