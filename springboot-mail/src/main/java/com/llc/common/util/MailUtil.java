package com.llc.common.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.llc.entity.Mail;

import lombok.extern.slf4j.Slf4j;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * 发送邮件工具类
 * @author lenovo
 *
 */
@Component
@Slf4j
public class MailUtil {

	@Autowired
	private JavaMailSender mailSender; // 自动注入的Bean

	@Value("${spring.mail.username}")
	private String sender; // 读取配置文件中的参数

	/**
	 * 发送邮件测试方法
	 * --发送给自己
	 */
	public void sendMail() {
		try {
			SimpleMailMessage mimeMessage = new SimpleMailMessage();
			mimeMessage.setFrom(sender);
			mimeMessage.setTo(sender);
			mimeMessage.setSubject("SpringBoot集成JavaMail实现邮件发送");
			mimeMessage.setText("SpringBoot集成JavaMail实现邮件发送正文");
			mailSender.send(mimeMessage);
			log.info("发送邮件测试-发送成功");
		} catch (Exception e) {
			log.error("发送邮件测试-发送失败", e);
		}

	}

	/**
	 * 发送简易邮件
	 * @param Mail
	 */
	public void sendMail(Mail Mail) {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
		try {
			helper.setFrom(sender);
			helper.setTo(Mail.getReceiver());
			helper.setSubject(Mail.getSubject());
			helper.setText(Mail.getText());
			mailSender.send(mimeMessage);
			log.info("发送简易邮件-发送成功");
		} catch (MessagingException e) {
			log.error("发送简易邮件-发送失败", e);
		}
	}

	/**
	 * 发送邮件-邮件正文是HTML
	 * @param Mail
	 */
	public void sendMailHtml(Mail Mail) {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
		try {
			helper.setFrom(sender);
			helper.setTo(Mail.getReceiver());
			helper.setSubject(Mail.getSubject());
			helper.setText(Mail.getText(), true);
			mailSender.send(mimeMessage);
			log.info("发送HTML邮件-发送成功");
		} catch (MessagingException e) {
			log.error("发送HTML邮件-发送失败", e);
		}
	}

	/**
	 * 发送邮件-附件邮件
	 * @param Mail
	 */
	public void sendMailAttachment(Mail Mail) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
			helper.setFrom(sender);
			helper.setTo(sender);
			helper.setSubject(Mail.getSubject());
			helper.setText(Mail.getText(), true);
			// 增加附件名称和附件
			helper.addAttachment(Mail.getAttachmentFilename(), Mail.getFile());
			mailSender.send(mimeMessage);
			log.info("发送附件邮件-发送成功");
		} catch (MessagingException e) {
			log.error("发送附件邮件-发送失败", e);
		}
	}

	/**
	 * 内联资源（静态资源）邮件发送
	 * 由于邮件服务商不同，可能有些邮件并不支持内联资源的展示
	 * 在测试过程中，新浪邮件不支持，QQ邮件支持
	 * 不支持不意味着邮件发送不成功，而且内联资源在邮箱内无法正确加载
	 * @param Mail
	 */
	public void sendMailInline(Mail Mail) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
			helper.setFrom(sender);
			helper.setTo(sender);
			helper.setSubject(Mail.getSubject());

			/*
			 * 内联资源邮件需要确保先设置邮件正文，再设置内联资源相关信息
			 */
			helper.setText(Mail.getText(), true);
			helper.addInline(Mail.getContentId(), Mail.getFile());

			mailSender.send(mimeMessage);
			log.info("发送内联资源邮件-发送成功");
		} catch (MessagingException e) {
			log.error("发送内联资源邮件-发送失败", e);
		}
	}

	/**
	 * 模板邮件发送
	 * @param Mail
	 */
	public void sendMailTemplate(Mail Mail) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
			helper.setFrom(sender);
			helper.setTo(sender);
			helper.setSubject(Mail.getSubject());
			helper.setText(Mail.getText(), true);
			mailSender.send(mimeMessage);
			log.info("发送模板邮件-发送成功");
		} catch (MessagingException e) {
			log.error("发送模板邮件-发送失败", e);
		}
	}

	/**
	 *
	 * 发送失败
	
	 因为各种原因，总会有邮件发送失败的情况，比如：邮件发送过于频繁、网络异常等。在出现这种情况的时候，我们一般会考虑重新重试发送邮件，会分为以下几个步骤来实现：
	
	 1、接收到发送邮件请求，首先记录请求并且入库。
	 2、调用邮件发送接口发送邮件，并且将发送结果记录入库。
	 3、启动定时系统扫描时间段内，未发送成功并且重试次数小于3次的邮件，进行再次发送
	 异步发送
	
	 很多时候邮件发送并不是我们主业务必须关注的结果，比如通知类、提醒类的业务可以允许延时或者失败。
	 这个时候可以采用异步的方式来发送邮件，加快主交易执行速度，在实际项目中可以采用MQ发送邮件相关参数，
	 监听到消息队列之后启动发送邮件。
	 *
	 */
}
