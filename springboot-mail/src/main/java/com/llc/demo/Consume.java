package com.llc.demo;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.llc.common.constant.RabbitConstant;
//import com.llc.common.util.MailUtil;
import com.llc.entity.Mail;

import lombok.extern.slf4j.Slf4j;

/**
 * 消费者
 * @author lenovo
 *
 */
@Component
@Slf4j
public class Consume {

	//@Autowired
	//private MailUtil mailUtil;

	/**
	 * 监听mq队列的消息
	 * @param msg
	 */
	@RabbitListener(queues = RabbitConstant.EMAIL_QUEUE)
	@RabbitHandler
	public void processEmail(String msg) {
		try {
			long start = System.currentTimeMillis();
			log.info("准备开始发送邮件......" + msg);
			Mail mail = Mail.getMailBean();
			mail.setSubject("测试一下");
			mail.setReceiver("xxxxx@qq.com");
			mail.setText("测试内容llc" + msg);
			//mailUtil.sendMail(mail);
			log.info("发送邮件消费时间: " + (System.currentTimeMillis() - start));
		} catch (Exception e) {
			log.error("邮件发送失败了!", e);
		}
	}

}
