package com.llc.demo;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.llc.common.constant.RabbitConstant;

import lombok.extern.slf4j.Slf4j;

/**
 * 生成者
 * @author lenovo
 *
 */
@Component
@Slf4j
public class Produce {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	public void send(String msg) {
		long start = System.currentTimeMillis();

		rabbitTemplate.convertAndSend(RabbitConstant.CONTROL_EXCHANGE, RabbitConstant.EMAIL_ROUTING_KEY, msg);
		log.info("rabbitmq消息已经发送到交换机, 等待交换机接受..." + msg);

		log.info("mq消费时间: " + (System.currentTimeMillis() - start));
	}
}
