package com.llc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.llc.demo.Produce;

@SpringBootApplication
@RestController
public class SpringbootMailApplication {

	@Autowired
	private Produce produce;

	@RequestMapping("/send")
	public void go() {
		produce.send(" -0327");
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringbootMailApplication.class, args);
	}

}
