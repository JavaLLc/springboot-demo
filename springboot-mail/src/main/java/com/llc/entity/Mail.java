package com.llc.entity;

import lombok.Data;
import org.springframework.core.io.FileSystemResource;

/**
 * 邮件封装对象
 * @author lenovo
 *
 */
@Data
public class Mail {
	/**
	 * 邮件主题
	 */
	private String subject;

	/**
	 * 收件人
	 */
	private String receiver;

	/**
	 * 邮件内容
	 */
	private String text;

	/**
	 * 附件
	 */
	private FileSystemResource file;

	/**
	 * 附件名称
	 */
	private String attachmentFilename;

	/**
	 * 内容ID，用于发送静态资源邮件时使用
	 */
	private String contentId;

	public static Mail getMailBean() {
		return new Mail();
	}

}
