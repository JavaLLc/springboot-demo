package com.llc;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 背包0-1
 */
@Getter
@Setter
@ToString
public class Demo {

    private String scheme;//方案
    private Integer weight;//重量
    private Integer price;//价钱
    private static final int SIGN = 150;//最大重量
    private static List<Demo> list = new ArrayList<>();//方案集合

    private Demo(String scheme,Integer weight,Integer price){
        this.scheme = scheme;
        this.weight = weight;
        this.price = price;
    }

    public static void main(String[] args) {
        //物品编号
        int [] ids = {1,2,3,4,5,6,7};
        //重量及对应价钱
        int [] ws = {35,30,60,50,40,10,25};
        int [] ps = {10,40,30,50,35,40,30};

        //1.计算最大取出数
        int num = countNum(ws);
        //2.获取组合情况
        List<String> situatioList;
        situatioList = Demo2.getList(ids.length, num);

        //3.计算符合条件的组合值
        countAllSituation(situatioList,ws,ps);

        //排序
        Comparator<Demo> comparator = Comparator.comparing(Demo::getPrice);
        //打印
        //situatioList.forEach(System.out::println);
        list.stream()
                .max(comparator)
                .ifPresent(System.out::println);
    }

    /**
     * 计算符合条件的组合值
     * @param situatioList
     */
    private static void countAllSituation(List<String> situatioList,int [] ws,int [] ps) {
        for(String s : situatioList){
            int wsNum = 0;
            int psNum = 0;
            String[] split = s.split("");
            for(String ss : split){
                Integer i = Integer.valueOf(ss);
                wsNum += ws[i-1];
                psNum += ps[i-1];
            }
            //符合的情况添加到方案
            if(wsNum<=SIGN){
                list.add(new Demo(s,wsNum,psNum));
            }
        }
    }

    /**
     * //计算最大取出数
     * @return
     */
    private static int  countNum(int [] ws){
        final int[] sum = {0,0};
        int [] clone_ws = new int[ws.length];
        System.arraycopy(ws, 0, clone_ws, 0, ws.length);
        Arrays.sort(clone_ws);
        for (int s : clone_ws) {
            if (sum[0] <= SIGN) {
                sum[0] += s;
                sum[1] += 1;
            } else {
                sum[1] -= 1;
                //System.out.print(sum[0] + "-------------" + sum[1]);
                break;
            }
        }
        return sum[1];
    }


}
