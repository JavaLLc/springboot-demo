package com.llc;

import java.util.ArrayList;
import java.util.List;

/**
 * 求排列数组合数
 */
public class Demo2 {

    private static int M = 7;//最大组合数
    private static int[] a= new int[]{1,2,3,4,5,6,7};//物品编号
    private static int[] b = new int[7];

    private static List<String> list = new ArrayList<>();

    public static void main(String[] args){
        int [] ids = {1,2,3,4,5,6,7};

        for(int i = 1;i<= 5 ;i++){
            M = i;
            C(ids.length,i);
        }
        //C(N,M);
        //list.forEach(System.out::println);
        System.out.println(list.size());
    }

    /**
     * 从count个数取num个数获取组合
     * * @param count
     * @param num
     * @return
     */
    public static List<String> getList(int count,int num){
        for(int i = 1;i<= num ;i++){
            M = i;
            C(count,i);
        }
        return list;
    }


    /**
     * 获取所有组合情况
     * @param m
     * @param n
     */
    static void C(int m,int n){
        int i,j;
        for(i=n;i<=m;i++) {
            b[n-1] = i-1;
            if(n>1)
                C(i-1,n-1);
            else {
                StringBuffer sb = new StringBuffer();
                for(j=0;j<=M-1;j++){
                    //System.out.print(a[b[j]] + "  ");
                    sb.append(a[b[j]]);
                }
                list.add(sb.toString());
                //System.out.println();
            }
        }
    }
}

