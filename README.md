# base-springboot-demo

#### 介绍
```
基于springboot的java技术或功能的demo
有些自己写的,有些网上复制的,提供简单入门学习
```
####子项目
```
1.jd8新特性:jdk8-speciality
2.log4j2:log4j2-demo
3.mail邮件发送(集成mq):springboot-mail
4.验证码:yanzhengma
5.定时器:time-demo
6.AOP:aop-demo
7.redis案例:base-redis
8.自定义注解(做一个了解):anno-demo
9.JAXB-XML:jaxb-xml
10.过滤器:filter-demo
11.拦截器:interceptor-demo
12.监听器:listener-demo
13.线程池:async-demo
14.多数据源:datasource-demo
15.poi(Excel导出导入)(java执行js脚本语言:在test中):poi-demo
16.Druid-sql监控:druid-demo
17.七牛云:qiniu-demo
18.jpa/hibernate :hibernate-demo
19.mybatis-plus :mybatis-plus-demo
20.全局异常处理/统一返回格式:controlleradvice-demo
21.常用的配置:config-demoSpringBoot拦截器】
22.一些简单的知识:simple-know
23.整合JPA与Mybatis:jpa-mybatis-demo
24.代码生成器:generate-core-demo包含多个子项目(代码生成器)
25.ssl(https):ssl-demo
26.swagger自动生成文档插件
27.自定义Starter:starter-demo
28.开源项目Hutool Java工具包的使用:hutool-demo
29.SpringBoot整合H2内存数据库:h2-demo
30.分布式文件系统:fastdfs-demo
31.zookeeper:zk-demo
32.多种MQ简单使用:mq-demo
33.ftp实现文件上传下载:ftp-demo
34.cache缓存:cache-demo
35.远程执行shll脚本或者命令:ssh-linux
36.各种锁-分布式锁另起一个demo:lock-demo
37.分布式锁:base-distributed-lock
38.基于redis的API接口幂等设计:api-idempotent
39.oauth2.0: oauth2.0-demo
40.测试框架TestNG:testng-demo
41.一些算法:arithmetic-demo
42.springcloud实例:springcloud-demo
43.shiro权限:base-shiro
44.apidoc生成接口文档:apidoc-demo
45.validator参数验证:validator-demo
46.fluent-validation参数验证:fluent-validation-demo
47.上传文件:upload-file-demo
48.quartz任务调度框架:quartz-demo
----------------------------------------
nginx
netty
-----------------------------------------
solr搜索
单点登陆
第三方登录
sharding-jdbc
Mycat
----------------------------------------
```
#### 软件架构
```
基于springboot整合java技术(redis,mq,zookeeper,fastdfs等)
springcloud一些组件学习
```
#### 安装教程
```
略
```
#### 使用说明
```
每个子项目下都有readme.md详细说明
```