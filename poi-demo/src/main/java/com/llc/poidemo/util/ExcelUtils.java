package com.llc.poidemo.util;

import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {

	private static final Logger logger = Logger.getLogger(ExcelUtils.class);

	/**
	 * 读取Excel文件，后缀名要求是xls格式。跳过首行 F * @param is 输入流
	 * 
	 * @param sheetIndex
	 *            读取的sheet页码下标，从0开始
	 * @param ignoreFirst
	 *            跳过首行
	 * @param keys
	 *            每个cell的名称
	 * @return
	 */
	public static List<Map<String, String>> readExcel(InputStream is, int sheetIndex, boolean ignoreFirst,
			String... keys) {
		List<Map<String, String>> resultData = new ArrayList<Map<String, String>>();
		Workbook workbook = null;
		try {
			workbook = Workbook.getWorkbook(is);
			if (sheetIndex < -1) {
				sheetIndex = 0;
			}
			Sheet sheet = workbook.getSheet(sheetIndex);
			// 索引越界
			if (sheet == null)
				return null;

			Map<String, String> rowMap = null;
			int i = 0;
			if (ignoreFirst) {
				i = 1;
			}
			for (; i < sheet.getRows(); i++) {
				Cell[] items = sheet.getRow(i);
				rowMap = new HashMap<String, String>();
				for (int j = 0; j < items.length; j++) {
					if (keys.length - j > 0) {
						rowMap.put(keys[j], items[j].getContents().trim());
					} else {
						rowMap.put("other" + j, items[j].getContents().trim());
					}
				}
				resultData.add(rowMap);
			}
		} catch (Exception e) {
			logger.error("Excel处理过程异常，异常信息：" + e.getMessage());
		}
		return resultData;
	}

	public static List<Map<String, String>> readExcel2(InputStream is, int sheetIndex, boolean ignoreFirst,
			String... keys) {
		List<Map<String, String>> resultData = new ArrayList<Map<String, String>>();
		XSSFWorkbook xssfWorkbook = null;
		try {
			xssfWorkbook = new XSSFWorkbook(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (sheetIndex < -1) {
			sheetIndex = 0;
		}

		// 获取每一个工作薄
		for (int numSheet = 0; numSheet < xssfWorkbook.getNumberOfSheets(); numSheet++) {
			XSSFSheet xssfSheet = xssfWorkbook.getSheetAt(numSheet);
			if (xssfSheet == null) {
				continue;
			}
			Map<String, String> map = null;
			// 获取当前工作薄的每一行
			int rowNum = 0;
			if (ignoreFirst) {
				rowNum = 1;
			}
			for (; rowNum <= xssfSheet.getLastRowNum(); rowNum++) {
				XSSFRow xssfRow = xssfSheet.getRow(rowNum);
				if (xssfRow != null) {
					map = new HashMap<String, String>();
					XSSFCell name = xssfRow.getCell(0);
					XSSFCell mobile = xssfRow.getCell(1);

					if (keys.length != 2) {
						map.put("username", getCellValueByCell(name));
						map.put("mobile", getCellValueByCell(mobile));
					} else {
						map.put("username", getCellValueByCell(name));
						map.put("mobile", getCellValueByCell(mobile));
					}

					resultData.add(map);
				}
			}
		}
		return resultData;
	}

	private static String getCellValueByCell(XSSFCell cell) {
		// 判断是否为null或空串
		if (cell == null || cell.toString().trim().equals("")) {
			return "";
		}
		String cellValue = "";
		int cellType = cell.getCellType();

		switch (cellType) {
		case 1: // 字符串类型
			cellValue = cell.getStringCellValue().trim();
			break;
		case 0: // 数值类型
			cellValue = String.valueOf((long)cell.getNumericCellValue());
			break;
		default: // 其它类型
			cellValue = "";
			break;
		}
		return cellValue;
	}

	/**
	 * 读取Excel文件，后缀名要求是xls格式。跳过首行
	 * 
	 * @param file
	 *            文件对象
	 * @param sheetIndex
	 *            读取的sheet页码下标，从0开始
	 * @param ignoreFirst
	 *            跳过首行
	 * @param keys
	 *            每个cell的名称
	 * @return
	 * @throws FileNotFoundException
	 */
	public static List<Map<String, String>> readExcel(File file, int sheetIndex, boolean ignoreFirst, String... keys)
			throws FileNotFoundException {
		return readExcel(new FileInputStream(file), sheetIndex, ignoreFirst, keys);
	}

	public static void main(String[] args) {
		List<Map<String, String>> list = new ArrayList<>();
		Map<String, String> map = new HashMap<>();
		map.put("username", "kk");
		map.put("mobile", "138");
		list.add(map);
		String path = System.getProperty("user.dir");
		String[] sheet0 = { "姓名", "手机" };
		String[] keyName = { "username", "mobile" };
		System.out.println(path);
		writeExcelDiv(list, path, "特定用户表.xls", "特定用户", sheet0, keyName);
	}

	/**
	 * 
	 * @param list
	 *            数据
	 * @param path
	 *            路径
	 * @param name
	 *            表名 可为空，默认工作簿1
	 * @param sheetName
	 *            页名 可为空，默认sheet1
	 * @param sheet0
	 *            第一行 可为空
	 * @param keyName
	 *            map中的key
	 */
	public static void writeExcelDiv(List<Map<String, String>> list, String path, String name, String sheetName,
			String[] sheet0, String[] keyName) {
		if (list == null || path == null || keyName == null) {
			logger.info("导出发生错误");
			return;
		}
		if (sheetName == null) {
			sheetName = "sheet1";
		}
		if (name == null) {
			name = "工作簿1";
		}
		// 创建HSSFWorkbook对象(excel的文档对象)
		HSSFWorkbook wb = new HSSFWorkbook();
		// 建立新的sheet对象（excel的表单）
		HSSFSheet sheet = wb.createSheet(sheetName);
		if (sheet0 != null) {
			HSSFRow row1 = sheet.createRow(0);
			for (int j = 0; j < sheet0.length; j++) {
				row1.createCell(j).setCellValue(sheet0[j]);
			}
		}

		for (int i = 0; i < list.size(); i++) {
			HSSFRow row = sheet.createRow(i + 1);
			Map<String, String> map = list.get(i);
			for (int j = 0; j < keyName.length; j++) {
				row.createCell(j).setCellValue(map.get(keyName[j]));
			}
		}

		// 输出Excel文件
		FileOutputStream output = null;
		try {
			logger.info(path);
			output = new FileOutputStream(path + "//" + name);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			wb.write(output);
			output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

