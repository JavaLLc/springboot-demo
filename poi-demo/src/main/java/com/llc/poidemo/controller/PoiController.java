package com.llc.poidemo.controller;

import com.llc.poidemo.util.ExcelUtils;
import com.llc.poidemo.util.ExportExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
public class PoiController {

    /**
     * 导出excel
     * @param request
     * @param response
     */
    @RequestMapping("down")
    public void poiDemo(HttpServletRequest request, HttpServletResponse response){
        String fileName = "导出测试";// 文件名
        String columnNames[] = { "姓名", "手机号" };// 列名
        String k[] = { "username", "mobile" };// map中的key
        //数据
        Map<String, String> map = new HashMap<>();
        map.put("username", "张三");
        map.put("mobile", "13800138000");

        List<Map> m = new ArrayList<>();
        m.add(map);
        try {
            ExportExcelUtils.exportExcel(m, k, columnNames, response, fileName, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("uploading")
    public void readExcel(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.info("开始读取上传的表格");
        String[] keys = { "username", "mobile" };
        MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
        // 获取附件内容
        Map<String, MultipartFile> multipartFileMap = mRequest.getFileMap();
        MultipartFile file = multipartFileMap.get("rdsName");
        String fileName = file.getOriginalFilename();
        log.info("文件名:" + fileName);
        InputStream inputStream = file.getInputStream();
        List<Map<String, String>> excel = null;
        if (fileName.endsWith(".xls")) {
            excel = ExcelUtils.readExcel(inputStream, 0, true, keys);
        } else if (fileName.endsWith(".xlsx")) {
            excel = ExcelUtils.readExcel2(inputStream, 0, true, keys);
        }

        if (excel == null || excel.size() < 1) {
            log.info("表格读取数据为空");
        }

        log.info("读取完毕" + excel);
    }
}
