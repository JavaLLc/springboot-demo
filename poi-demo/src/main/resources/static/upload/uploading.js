
var sub_f = false;
/**
 * 选择文件回调
 * @param node
 * @returns
 */
function choose_file(node) {
    var file = node.files[0];
    if (file == null) {
        return;
    }
    sub_f = getPath(file);
    if (sub_f) {
        $(".rds_btu").css("display", "inline-block");
    } else {
        $(".rds_btu").css("display", "none");
        $(".myFileUpload").eq(1).val('');
    }
}

function getPath(file) {
    var size = file.size;
    if (size >= 300 * 1024) {
        console.log("文件太大");
        return false;
    }
    var name = file.name;
    var strs = name.split(".");
    var suffix = strs[strs.length - 1];
    console.log("文件名" + name);
    if (suffix == "xls" || suffix == "xlsx") {
        return true;
    }
    return false;
}

function to_submit() {
    console.log("提交前");
    if(sub_f){
        return true;
    }
    return false;
}
