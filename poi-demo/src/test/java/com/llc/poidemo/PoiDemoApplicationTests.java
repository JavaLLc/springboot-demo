package com.llc.poidemo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.script.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * java执行js脚本语言
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PoiDemoApplicationTests {

    /**
     *在Java中直接调用js代码
     */
    @Test
    public void contextLoads() {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");
        try{
            engine.eval("var a=3; var b=4;print(a+b);");
            // 不能调用浏览器中定义的js函数 // 错误，会抛出alert引用不存在的异常
            // engine.eval("alert(\"js alert\");");
        }catch(ScriptException e){
            e.printStackTrace();
        }
    }

    /**
     *在Java中绑定js变量
     */
    @Test
    public void contextLoads2() {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");
        //绑定变量
        engine.put("a", 4);
        engine.put("b", 3);
        Bindings bindings = engine.getBindings(ScriptContext.ENGINE_SCOPE);
        try {
            // 只能为Double，使用Float和Integer会抛出异常
            Double result = (Double) engine.eval("a+b");
            System.out.println("result = " + result);

            engine.eval("c=a+b");
            Double c = (Double)engine.get("c");
            System.out.println("c = " + c);
        } catch (ScriptException e) {
            e.printStackTrace();
        }
    }

    /**
     *调用js脚本文件
     */
    @Test
    public void contextLoads3() {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");
        String jsFileName = "F:\\IDE\\springbootLearning\\base-springboot-study\\poi-demo\\src\\test\\java\\com\\llc\\poidemo\\demo.js";   // 读取js文件
        FileReader reader = null;   // 执行指定脚本
        try {
            reader = new FileReader(jsFileName);
            engine.eval(reader);
            if(engine instanceof Invocable) {
                Invocable invoke = (Invocable)engine;    // 调用merge方法，并传入两个参数
                // c = llc(2, 3);
                Double c = (Double)invoke.invokeFunction("llc", 2, 3);
                System.out.println("c = " + c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
