###spring boot 添加整合ssl使得http变成https方法(测试用)
```
1.到jdk/bin目录,cmd
//其中-alias是证书的别名
//RSA是加密算法
//-keystore后是输出证书的路径所在及文件名

2.keytool -genkeypair -alias tomcat -keyalg RSA -keystore E:\tomcat.key
内容如截图

(注:类型为:JKS)
##SSL https证书配置, 正式项目中一般不会配置在这个路径中
server.ssl.key-store=classpath:tomcat.key
server.ssl.key-store-password=1234567
server.ssl.key-store-type=JKS

3.到指定生成路径即可找到文件
```

###测试
```
1.访问https://localhost:8443/
2.访问http://localhost:8081/会自动跳转到https://localhost:8443/
注:该证书只用于测试,访问时会提示不安全,点击高级继续访问
```
