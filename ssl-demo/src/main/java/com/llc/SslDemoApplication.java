package com.llc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SslDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SslDemoApplication.class, args);
	}

	@RequestMapping("/")
	public String hello() {
		return "hello,ssl";
	}
	
	@Autowired
    private StarterDemoService starterDemoService;

	@RequestMapping("/hello")
    public void testHelloWorld() {
		starterDemoService.sayHello();
    }
}
