package com.llc.qiniudemo.controller;

import com.llc.qiniudemo.service.FileService;
import com.llc.qiniudemo.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/webServer")
@Slf4j
public class WebServerController {

    @Resource
    private FileService fileService;


    /**
     * 上传图片文件七牛云
     * @param files
     * @return
     */
    @RequestMapping(value="/imgs", method = RequestMethod.POST)
    public Result uploadImg(@RequestParam("file") MultipartFile[] files) {

        // 返回类型可以自己定义
        Result result = null;

        // 验证非空
        if (StringUtils.isBlank(files[0].getOriginalFilename())) {
            result = Result.fail(500,"数据为空");
        } else {
            Map<String,List<String>> map = new HashMap<>();

            map = fileService.uploadImgs(files);

            List<String> resultList = map.get("result");
            log.info("图片上传返回结果:"+resultList);

            if ("error".equals(resultList.get(0))) {
                result = Result.fail();
            } else {
                result = Result.succeed(resultList);
            }
        }
        return result;
    }
}
