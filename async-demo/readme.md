#线程池
线程是稀缺资源，如果被无限制的创建，不仅会消耗系统资源，还会降低系统的稳定性，
合理使用线程池对线程进行统一分配、调优和监控：
1、降低资源消耗；
2、提高响应速度；
3、提高线程的可管理性。
---------------------------------------------------------

步骤如下：
1. 创建springboot工程； 
2. 创建Service层的接口和实现； 
3. 创建controller，开发一个http服务接口，里面会调用service层的服务； 
4. 创建线程池的配置； 
5. 将Service层的服务异步化，这样每次调用都会都被提交到线程池异步执行； 
6. 扩展ThreadPoolTaskExecutor，在提交任务到线程池的时候可以观察到当前线程池的情况；
---------------------------------------------------------

运行:
将这个springboot运行起来;
在浏览器输入：http://localhost:8080;
在浏览器用F5按钮快速多刷新几次;
在springboot的控制台看见日志.

可以看到controller的执行线程是”nio-8080-exec-8”，这是tomcat的执行线程，
而service层的日志显示线程名为“async-service-1”，在我们配置的线程池中执行了，
并且每次请求中，controller的起始和结束日志都是连续打印的，表明每次请求都快速响应了，
而耗时的操作都留给线程池中的线程去异步执行
-----------------------------------------------------

扩展ThreadPoolTaskExecutor
不清楚线程池当时的情况，有多少线程在执行，多少在队列中等待呢？
创建了一个ThreadPoolTaskExecutor的子类，在每次提交线程的时候都会将当前线程池的运行状况打印出来
---------------------------------------------------

处理流程
当一个任务被提交到线程池时，首先查看线程池的核心线程是否都在执行任务，否就选择一条线程执行任务，是就执行第二步。
查看核心线程池是否已满，不满就创建一条线程执行任务，否则执行第三步。
查看任务队列是否已满，不满就将任务存储在任务队列中，否则执行第四步。
查看线程池是否已满，不满就创建一条线程执行任务，否则就按照策略处理无法执行的任务。
--------------------- 
在ThreadPoolExecutor中表现为:
如果当前运行的线程数小于corePoolSize，那么就创建线程来执行任务（执行时需要获取全局锁）。
如果运行的线程大于或等于corePoolSize，那么就把task加入BlockQueue。
如果创建的线程数量大于BlockQueue的最大容量，那么创建新线程来执行该任务。
如果创建线程导致当前运行的线程数超过maximumPoolSize，就根据饱和策略来拒绝该任务。
--------------------- 

关闭线程池
调用shutdown或者shutdownNow，两者都不会接受新的任务，
而且通过调用要停止线程的interrupt方法来中断线程，有可能线程永远不会被中断，
不同之处在于shutdownNow会首先将线程池的状态设置为STOP，然后尝试停止所有线程
（有可能导致部分任务没有执行完）然后返回未执行任务的列表。
而shutdown则只是将线程池的状态设置为shutdown，然后中断所有没有执行任务的线程，并将剩余的任务执行完。
--------------------- 

配置线程个数
如果是CPU密集型任务，那么线程池的线程个数应该尽量少一些，一般为CPU的个数+1条线程。
如果是IO密集型任务，那么线程池的线程可以放的很大，如2*CPU的个数。
对于混合型任务，如果可以拆分的话，通过拆分成CPU密集型和IO密集型两种来提高执行效率；
如果不能拆分的的话就可以根据实际情况来调整线程池中线程的个数。
--------------------- 

监控线程池状态
常用状态：
taskCount：线程需要执行的任务个数。
completedTaskCount：线程池在运行过程中已完成的任务数。
largestPoolSize：线程池曾经创建过的最大线程数量。
getPoolSize获取当前线程池的线程数量。
getActiveCount：获取活动的线程的数量
--------------------- 


线程池的种类，区别和使用场景
newCachedThreadPool：
底层：返回ThreadPoolExecutor实例，corePoolSize为0；maximumPoolSize为Integer.MAX_VALUE；keepAliveTime为60L；unit为TimeUnit.SECONDS；workQueue为SynchronousQueue(同步队列)
通俗：当有新任务到来，则插入到SynchronousQueue中，由于SynchronousQueue是同步队列，因此会在池中寻找可用线程来执行，若有可以线程则执行，若没有可用线程则创建一个线程来执行该任务；若池中线程空闲时间超过指定大小，则该线程会被销毁。
适用：执行很多短期异步的小程序或者负载较轻的服务器
--------------------- 

newFixedThreadPool：
底层：返回ThreadPoolExecutor实例，接收参数为所设定线程数量nThread，corePoolSize为nThread，maximumPoolSize为nThread；keepAliveTime为0L(不限时)；unit为：TimeUnit.MILLISECONDS；WorkQueue为：new LinkedBlockingQueue<Runnable>() 无解阻塞队列
通俗：创建可容纳固定数量线程的池子，每隔线程的存活时间是无限的，当池子满了就不在添加线程了；如果池中的所有线程均在繁忙状态，对于新任务会进入阻塞队列中(无界的阻塞队列)
适用：执行长期的任务，性能好很多
--------------------- 

newSingleThreadExecutor:
底层：FinalizableDelegatedExecutorService包装的ThreadPoolExecutor实例，corePoolSize为1；maximumPoolSize为1；keepAliveTime为0L；unit为：TimeUnit.MILLISECONDS；workQueue为：new LinkedBlockingQueue<Runnable>() 无解阻塞队列
通俗：创建只有一个线程的线程池，且线程的存活时间是无限的；当该线程正繁忙时，对于新任务会进入阻塞队列中(无界的阻塞队列)
适用：一个任务一个任务执行的场景
--------------------- 

NewScheduledThreadPool:
底层：创建ScheduledThreadPoolExecutor实例，corePoolSize为传递来的参数，maximumPoolSize为Integer.MAX_VALUE；keepAliveTime为0；unit为：TimeUnit.NANOSECONDS；workQueue为：new DelayedWorkQueue() 一个按超时时间升序排序的队列
通俗：创建一个固定大小的线程池，线程池内线程存活时间无限制，线程池可以支持定时及周期性任务执行，如果所有线程均处于繁忙状态，对于新任务会进入DelayedWorkQueue队列中，这是一种按照超时时间排序的队列结构
适用：周期性执行任务的场景
--------------------- 