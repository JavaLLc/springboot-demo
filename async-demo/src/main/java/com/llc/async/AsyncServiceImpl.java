package com.llc.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncServiceImpl implements AsyncService {

    private static final Logger logger = LoggerFactory.getLogger(AsyncServiceImpl.class);

    /**
     * service层做的事情都提交到线程池中去处理
     * asyncServiceExecutor便是配置里的那个方法
     */
    @Override
    @Async("asyncServiceExecutor")
    public void executeAsync() {
        logger.info("开始执行异步任务");
        try{
            Thread.sleep(1000);
        }catch(Exception e){
            e.printStackTrace();
        }
        logger.info("结束执行异步任务");
    }
}