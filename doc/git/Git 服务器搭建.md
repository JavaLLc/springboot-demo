###git clone项目有2中模式：
```
1.https模式，账号密码匹配成功后，即可下载。
2.ssh模式，需要 创建本地的SSH Key ，然后将生成的 SSH Key 的公钥(id_rsa.pub)上传到git中，配置好公钥后，就能下载了。
```

###ssh模式
#####1、安装Git
```
$ yum install curl-devel expat-devel gettext-devel openssl-devel zlib-devel perl-devel
$ yum install git

接下来我们 创建一个git用户组和用户，用来运行git服务：
$ groupadd git
$ useradd git -g git
```

#####2、创建证书登录
```
收集所有需要登录的用户的公钥，公钥位于id_rsa.pub文件中，把我们的公钥导入到/home/git/.ssh/authorized_keys文件里，一行一个。
如果没有该文件创建它：
$ cd /home/git/
$ mkdir .ssh
$ chmod 755 .ssh
$ touch .ssh/authorized_keys
$ chmod 644 .ssh/authorized_keys
```

#####3、初始化Git仓库
```
首先我们选定一个目录作为Git仓库，假定是/home/gitrepo/runoob.git，在/home/gitrepo目录下输入命令：
$ cd /home
$ mkdir gitrepo
$ chown git:git gitrepo/
$ cd gitrepo

$ git init --bare runoob.git
显示:
Initialized empty Git repository in /home/gitrepo/runoob.git/
以上命令Git创建一个空仓库，服务器上的Git仓库通常都以.git结尾。然后，把仓库所属用户改为git：
$ chown -R git:git runoob.git
```

#####在本地一个文件夹,右键git bash here
```
默认生成在win7当前登录用户git的目录
ssh-keygen -t rsa -C git
连续3次回车，默认设置空密码，创建key
查看生成的公钥：cat ~/.ssh/id_rsa.pub
复制内容到服务器的/home/git/.ssh的authorized_keys里
即可实现一下操作
```

#####4、克隆仓库
```
$ git clone git@192.168.5.133:/home/gitrepo/runoob.git
Cloning into 'runoob'...
warning: You appear to have cloned an empty repository.
Checking connectivity... done.
```
#####5.推送拉取
```
远程仓库未创建
git init //初始化一只本地仓库
git status //查看状态
git add -A //提交所有文件改动
git remote add origin ssh地址 // 链接远程仓库步骤1
git push -u origin master //链接远程仓库步骤2


git add -A //提交所有文件改动
git status //查看状态
git commit -m "xx" //备注功能
git status //查看状态
git pull origin master //拉最新代码 （下载）
git push origin master //推送到远程 （上传）
------------------------------------------------------------

远程仓库已创建：
git clone git@192.168.5.133:/home/gitrepo/runoob.git
关联远程
git remote add origin ssh地址 // 链接远程仓库步骤1
git pull origin master //拉最新代码 （下载）
git add -A //提交所有文件改动
git commit -m "xx" //备注功能
git push origin master //推送到远程 （上传）
```

