package com.llc.constant;

public class RequestConstant {

	/**客户端url前缀*/
	public static final String CLIENTURL = "http://localhost:8081";
	/**服务端url前缀*/
	public static final String SERVERURL = "http://localhost:8082";

	/* 重定向到服务端地址开始 */
	/**2.获取授权码code*/
	public static final String SERVER_GET_CODE = "/responseCode";
	/**4.获取令牌*/
	public static final String SERVER_GET_TOKEN = "/responseAccessToken";
	/**6.获取资源*/
	public static final String SERVER_GET_RESOURCE = "/userInfo";
	/* 重定向到服务端地址结束 */

	/* 重定向到客户端地址开始 */
	/**1.请求授权码code*/
	public static final String CLIENT_REQUEST_CODE = "/requestServerCode";
	/**3.请求令牌*/
	public static final String CLIENT_REQUEST_TOKEN = "/callbackCode";
	/**5.请求资源*/
	public static final String CLIENT_REQUEST_RESOURCE = "/accessToken";
	/* 重定向到客户端地址结束 */

	/**
	 * 2
	 * @return
	 */
	public static String getServerGetCode() {
		return SERVERURL + SERVER_GET_CODE;
	}

	/**
	 * 4
	 * @return
	 */
	public static String getServerGetToken() {
		return SERVERURL + SERVER_GET_TOKEN;
	}

	/**
	 * 6
	 * @return
	 */
	public static String getServerGetResource() {
		return SERVERURL + SERVER_GET_RESOURCE;
	}

	/**
	 * 1
	 * @return
	 */
	public static String getClientRequestCode() {
		return CLIENTURL + CLIENT_REQUEST_CODE;
	}

	/**
	 * 3
	 * @return
	 */
	public static String getClientRequestToken() {
		return CLIENTURL + CLIENT_REQUEST_TOKEN;
	}

	/**
	 * 5
	 * @return
	 */
	public static String getClientRequestResource() {
		return CLIENTURL + CLIENT_REQUEST_RESOURCE;
	}

}
