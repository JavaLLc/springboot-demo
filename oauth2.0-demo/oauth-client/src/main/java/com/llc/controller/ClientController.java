package com.llc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthBearerClientRequest;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.client.response.OAuthResourceResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.llc.constant.RequestConstant;

import lombok.extern.log4j.Log4j2;

@Controller
@Log4j2
public class ClientController {
	
	private String clientId = "clientId";
	private String clientSecret = "clientkey";
	private String response_type = "code";

	/**
	 * 获取授权码
	 * @param request
	 * @param response
	 * @param attr
	 * @return
	 * @throws OAuthProblemException
	 */
	@RequestMapping("/requestServerCode")
	public String requestServerFirst(HttpServletRequest request, HttpServletResponse response, RedirectAttributes attr)
			throws OAuthProblemException {
		log.info("1.客户端请求授权码------");
		String serverGetCode = RequestConstant.getServerGetCode();
		String redirectUrl = RequestConstant.getClientRequestToken();
		String requestUrl = null;
		try {
			// 构建oauthd的请求.accessTokenUrl(设置请求服务地址)、clientId(客户端key)、response_type(响应类型code/token)、redirectUrl(重定向url)
			OAuthClientRequest accessTokenRequest = OAuthClientRequest.authorizationLocation(serverGetCode)
					.setResponseType(response_type).setClientId(clientId).setRedirectURI(redirectUrl)
					.buildQueryMessage();
			requestUrl = accessTokenRequest.getLocationUri();
			log.info("1.1.客户端重定向地址:" + requestUrl);
		} catch (OAuthSystemException e) {
			log.error("------客户端请求授权码异常--------requestServerFirst-------", e);
		}
		return "redirect:" + requestUrl;
	}

	/**
	 * 接受客户端返回的code，提交申请access token的请求
	 * @param request
	 * @return
	 * @throws OAuthProblemException
	 */
	@RequestMapping("/callbackCode")
	public Object toLogin(HttpServletRequest request) throws OAuthProblemException {
		log.info("3.客户端请求令牌------");
		String serverGetToken = RequestConstant.getServerGetToken();
		String clientRequestResource = RequestConstant.getClientRequestResource();
		String redirectUrl = RequestConstant.getClientRequestResource();
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String code = httpRequest.getParameter("code");
		log.info("3.1.授权码------" + code);
		OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
		try {
			OAuthClientRequest accessTokenRequest = OAuthClientRequest.tokenLocation(serverGetToken)
					.setGrantType(GrantType.AUTHORIZATION_CODE).setClientId(clientId).setClientSecret(clientSecret)
					.setCode(code).setRedirectURI(redirectUrl).buildQueryMessage();
			// 去服务端请求access token,并返回响应
			OAuthAccessTokenResponse oAuthResponse = oAuthClient.accessToken(accessTokenRequest, OAuth.HttpMethod.POST);
			// 获取服务端返回过来的access token
			String accessToken = oAuthResponse.getAccessToken();
			// 查看access token是否过期
			// Long expiresIn = oAuthResponse.getExpiresIn();
			log.info("3.2.客户端请求的token:" + accessToken);
			return "redirect:" + clientRequestResource + "?accessToken=" + accessToken;
		} catch (OAuthSystemException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 接受服务端传回来的access token，由此token去请求服务端的资源
	 * @param accessToken
	 * @return
	 */
	@RequestMapping("/accessToken")
	@ResponseBody
	public String accessToken(String accessToken) {
		log.info("5.客户端请求资源------");
		log.info("5.1.token:" + accessToken);
		String serverGetResource = RequestConstant.getServerGetResource();
		OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
		try {
			OAuthClientRequest userInfoRequest = new OAuthBearerClientRequest(serverGetResource)
					.setAccessToken(accessToken).buildQueryMessage();
			OAuthResourceResponse resourceResponse = oAuthClient.resource(userInfoRequest, OAuth.HttpMethod.GET,
					OAuthResourceResponse.class);
			String resource = resourceResponse.getBody();
			log.info("5.2.客户端获取资源返回:" + resource);
			return resource;
		} catch (OAuthSystemException e) {
			e.printStackTrace();
		} catch (OAuthProblemException e) {
			e.printStackTrace();
		}
		return null;
	}

}
