package com.llc.controller;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.as.request.OAuthAuthzRequest;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.error.OAuthError;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.ParameterStyle;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;
import org.apache.oltu.oauth2.rs.request.OAuthAccessResourceRequest;
import org.apache.oltu.oauth2.rs.response.OAuthRSResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.extern.log4j.Log4j2;

@Controller
@Log4j2
public class ServerController {

	/**
	 * 向客户端返回授权码
	 */

	@RequestMapping("/responseCode")
	public Object toShowUser(Model model, HttpServletRequest request) {
		log.info("2.服务端验证客户端信息返回授权码----------");
		try {
			// 构建OAuth授权请求 
			OAuthAuthzRequest oauthRequest = new OAuthAuthzRequest(request);
			log.info("2.1.客户端信息:clientId{}-redirectURI{}-responseType{}", oauthRequest.getClientId(),
					oauthRequest.getRedirectURI(), oauthRequest.getResponseType());
			// 验证(这里只做简单验证)
			if (oauthRequest.getClientId() != null && oauthRequest.getClientId() != "") {
				// 利用oauth授权请求设置responseType，目前仅支持CODE，另外还有TOKEN 
				// String responseType = oauthRequest.getParam(OAuth.OAUTH_RESPONSE_TYPE);
				// 进行OAuth响应构建
				OAuthASResponse.OAuthAuthorizationResponseBuilder builder = OAuthASResponse
						.authorizationResponse(request, HttpServletResponse.SC_FOUND);
				// 授权码 
				String authorizationCode = "authorizationCode";
				// 设置授权码
				builder.setCode(authorizationCode);
				// 得到到客户端重定向地址
				String redirectURI = oauthRequest.getParam(OAuth.OAUTH_REDIRECT_URI);
				// 构建响应
				final OAuthResponse response = builder.location(redirectURI).buildQueryMessage();
				String responceUri = response.getLocationUri();
				log.info("2.2.服务端重定向地址:" + responceUri);
				// 根据OAuthResponse返回ResponseEntity响应
				HttpHeaders headers = new HttpHeaders();
				try {
					headers.setLocation(new URI(response.getLocationUri()));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
				return "redirect:" + responceUri;
			}
		} catch (OAuthSystemException e) {
			e.printStackTrace();
		} catch (OAuthProblemException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 向客户端返回资源访问令牌accesstoken
	 */
	// 获取客户端的code码，向客户端返回access token
	@RequestMapping(value = "/responseAccessToken", method = RequestMethod.POST)
	public HttpEntity<Object> token(HttpServletRequest request) {
		log.info("4.服务端验证客户端信息及授权码返回令牌----------");
		OAuthIssuer oauthIssuerImpl = null;
		OAuthResponse response = null;
		// 构建OAuth请求 
		try {
			OAuthTokenRequest oauthRequest = new OAuthTokenRequest(request);
			String authCode = oauthRequest.getParam(OAuth.OAUTH_CODE);
			log.info("4.1.服务端接收客户端请求的code:" + authCode);
			String clientSecret = oauthRequest.getClientSecret();
			log.info("4.2.服务端接收客户端请求的clientSecret:" + clientSecret);
			// 验证信息(这里只做简单验证)
			if (clientSecret != null || clientSecret != "") {
				// 生成Access Token
				oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());
				final String accessToken = oauthIssuerImpl.accessToken();
				log.info("4.3.服务端生成令牌:" + accessToken);
				// 生成OAuth响应
				response = OAuthASResponse.tokenResponse(HttpServletResponse.SC_OK).setAccessToken(accessToken)
						.buildJSONMessage();
			}
			log.info("4.4.服务端响应信息:" + response.getBody());
			// 根据OAuthResponse生成ResponseEntity
			return new ResponseEntity<Object>(response.getBody(), HttpStatus.valueOf(response.getResponseStatus()));
		} catch (OAuthSystemException e) {
			e.printStackTrace();
		} catch (OAuthProblemException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 向客户端返回请求资源
	 */
	@RequestMapping("/userInfo")
	public HttpEntity<Object> userInfo(HttpServletRequest request) throws OAuthSystemException {
		log.info("6.服务端验证令牌返回资源------");
		try {
			// 获取客户端传来的OAuth资源请求
			OAuthAccessResourceRequest oauthRequest = new OAuthAccessResourceRequest(request, ParameterStyle.QUERY);
			// 获取Access Token 
			String accessToken = oauthRequest.getAccessToken();
			log.info("6.1.服务端接收客户端请求的令牌:" + accessToken);
			// 验证Access Token 
			// 如果不存在/过期了，返回未验证错误，需重新验证
			if (accessToken == null || "".equals(accessToken)) {
				OAuthResponse oauthResponse = OAuthRSResponse.errorResponse(HttpServletResponse.SC_UNAUTHORIZED)
						.setError(OAuthError.ResourceResponse.INVALID_TOKEN).buildHeaderMessage();
				HttpHeaders headers = new HttpHeaders();
				headers.add(OAuth.HeaderType.WWW_AUTHENTICATE,
						oauthResponse.getHeader(OAuth.HeaderType.WWW_AUTHENTICATE));
				return new ResponseEntity<Object>(headers, HttpStatus.UNAUTHORIZED);
			}

			// 返回资源
			String resource = accessToken + "---" + Math.random() + "----" + "这是资源";
			log.info("6.2.服务端返回资源:" + resource);
			return new ResponseEntity<Object>(resource, HttpStatus.OK);
		} catch (OAuthProblemException e) {
			e.printStackTrace();
			// 检查是否设置了错误码 
			String errorCode = e.getError();
			if (OAuthUtils.isEmpty(errorCode)) {
				OAuthResponse oauthResponse = OAuthRSResponse.errorResponse(HttpServletResponse.SC_UNAUTHORIZED)
						.buildHeaderMessage();
				HttpHeaders headers = new HttpHeaders();
				headers.add(OAuth.HeaderType.WWW_AUTHENTICATE,
						oauthResponse.getHeader(OAuth.HeaderType.WWW_AUTHENTICATE));
				return new ResponseEntity<Object>(headers, HttpStatus.UNAUTHORIZED);
			}
			OAuthResponse oauthResponse = OAuthRSResponse.errorResponse(HttpServletResponse.SC_UNAUTHORIZED)
					.setError(e.getError()).setErrorDescription(e.getDescription()).setErrorUri(e.getUri())
					.buildHeaderMessage();
			HttpHeaders headers = new HttpHeaders();
			headers.add(OAuth.HeaderType.WWW_AUTHENTICATE, oauthResponse.getHeader(OAuth.HeaderType.WWW_AUTHENTICATE));
			return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
		}
	}

}
