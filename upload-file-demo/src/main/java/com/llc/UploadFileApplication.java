package com.llc;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import com.llc.common.UploadProperties;
import com.llc.service.IUploadService;

@SpringBootApplication
@EnableConfigurationProperties(UploadProperties.class)
public class UploadFileApplication {

	public static void main(String[] args) {
		SpringApplication.run(UploadFileApplication.class, args);
	}

	/**
	 * 项目启动清空文件夹库,初始化
	 * @param storageService
	 * @return
	 */
	@Bean
	CommandLineRunner init(IUploadService storageService) {
		return (args) -> {
			storageService.deleteAll();
			storageService.init();
		};
	}
}
