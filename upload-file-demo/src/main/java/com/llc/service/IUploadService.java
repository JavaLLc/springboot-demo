package com.llc.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface IUploadService {

	/**
	 * 初始化:创建文件夹库(用于存储上传的文件)
	 */
	void init();

	/**
	 * 上传文件
	 * @param file
	 */
	void store(MultipartFile file);

	/**
	 * 加载已经上传的文件列表
	 * @return
	 */
	Stream<Path> loadAll();

	/**
	 * 把一个路径或路径片段的序列解析为一个绝对路径
	 * @param filename
	 * @return
	 */
	Path load(String filename);

	/**
	 * 下载文件
	 * @param filename
	 * @return
	 */
	Resource loadAsResource(String filename);

	/**
	 * 清空文件库(删除所有已经上传的文件)
	 */
	void deleteAll();

}
