package com.llc.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author LLC
 * @date 2019年6月10日
 * @Description:
 */
@Getter
@Setter
public class User {

	@ApiModelProperty(value = "用户名")
	private String username;
	@ApiModelProperty(value = "密码")
	private String password;

}
