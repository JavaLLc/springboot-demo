package com.llc.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.llc.entity.User;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 * @author LLC
 * @date 2019年6月10日
 * @Description:
 */
@RestController
public class HelloController {

	@GetMapping("/hello")
	public String hello(@ApiParam(value = "用户名") String username, @ApiParam(value = "密码") String password) {
		return username + "  " + password;
	}

	@PostMapping("/hello")
	@ApiOperation(value = "数据列表")
	public User postHello(User user1) {
		User user = new User();
		user.setUsername("admin");
		user.setPassword("123456");
		return user;
	}

	@PutMapping("/hello")
	public String putHello() {
		return "hello world";
	}

	@DeleteMapping("/hello")
	public String delHello() {
		return "hello world";
	}

}
