## 基于springboot swagger 自动生成文档插件

#####1. 引入依赖包
```
    <dependency>
        <groupId>io.springfox</groupId>
        <artifactId>springfox-swagger2</artifactId>
        <version>2.7.0</version>
    </dependency>
    <dependency>
        <groupId>io.springfox</groupId>
        <artifactId>springfox-swagger-ui</artifactId>
        <version>2.7.0</version>
    </dependency>
```

#####2. 直接在启动类添加注解`@EnableSwagger2`或者另起一个配置类,在配置上加`@EnableSwagger2`

#####3. 打开浏览器访问 `/swagger-ui.html` 就可访问相应的 接口文档

#####4. 自定义接口文档名称
```
    1) 定义接口方法名: 在接口方法上添加注解 `@ApiOperation(value = "数据列表")`
    2) 定义属性名称: 在请求数据的包装类中的属性中添加注解 `@ApiModelProperty(value = "用户名")`
    3) 直接注解在请求参数上 `@ApiParam(value = "用户名")`
```




