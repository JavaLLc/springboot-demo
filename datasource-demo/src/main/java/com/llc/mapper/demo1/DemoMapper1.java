package com.llc.mapper.demo1;

import com.llc.entity.User;
import org.springframework.stereotype.Repository;

@Repository//启动类有扫描,可不加。不加则会有警告,不影响运行
public interface DemoMapper1 {

    public User getUserById(Integer id);

}

