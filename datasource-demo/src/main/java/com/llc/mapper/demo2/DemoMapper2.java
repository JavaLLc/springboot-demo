package com.llc.mapper.demo2;

import com.llc.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface DemoMapper2 {

    public User getUserById(Integer id);

}