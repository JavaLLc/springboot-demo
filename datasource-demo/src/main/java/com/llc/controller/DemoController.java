package com.llc.controller;

import com.llc.entity.User;
import com.llc.mapper.demo1.DemoMapper1;
import com.llc.mapper.demo2.DemoMapper2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class DemoController {

    @Autowired
    private DemoMapper1 mapper1;

    @Autowired
    private DemoMapper2 mapper2;

    @RequestMapping("hello")
    public String demo(){
        User user = mapper1.getUserById(1);
        User user2 = mapper2.getUserById(1);
        log.info("db1:"+user);
        log.info("db2:"+user2);
        return "success";
    }
}
