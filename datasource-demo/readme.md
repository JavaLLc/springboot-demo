###多数据源
#####分包方式实现:
```
1.配置:
(注意:jdbc-url与driver-class-name
spring.datasource.test1是前缀,配置中用到)
## test1 database
spring.datasource.test1.jdbc-url=jdbc:mysql://localhost:3306/authority-control?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC&useSSL=false
spring.datasource.test1.username=root
spring.datasource.test1.password=1234567
spring.datasource.test1.driver-class-name=com.mysql.jdbc.Driver

## test2 database
spring.datasource.test2.jdbc-url=jdbc:mysql://localhost:3306/cms?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC&useSSL=false
spring.datasource.test2.username=root
spring.datasource.test2.password=1234567
spring.datasource.test2.driver-class-name=com.mysql.jdbc.Driver


2.数据源配置
配置中设置mapper与xml的位置
@Primary是设置默认数据源的,必须设置且只能设置一个
-------------------------------------------

3.xml与mapper分包实现
mapper接口加上@Repository注解
启动类有扫描,可不加。不加则会有警告,不影响运行
```

#####4.事务
```
在service层添加注解@Transactional开启事务
参考项目:
F:\IDE\springbootLearning\multipledatasources2
```

#####5.统一事务
```
springboot+jta+atomikos 分布式事物管理
参考项目:
F:\IDE\springbootLearning\multipledatasources3
```

#####6.AOP实现多数据源
```
参考项目:
F:\IDE\springbootLearning\multipledatasources4
1、AOP：
2、AbstractRoutingDataSource：
这个类是实现多数据源的关键，他的作用就是动态切换数据源，
实质：有多少个数据源就存多少个数据源在targetDataSources这个属性中，
（是AbstractRoutingDataSource的一个map类型的属性，其中value为每个数据源，key表示每个数据源的名字）
然后根据determineCurrentLookupKey（）这个方法获取当前数据源在map中的key值，
然后determineTargetDataSource（）方法中动态获取当前数据源，
如果当前数据源不存并且默认数据源也不存在就抛出异常。
```


