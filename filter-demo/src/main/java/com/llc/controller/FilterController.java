package com.llc.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FilterController {

    @RequestMapping("/success/info")
    @ResponseBody
    public String success(){
        return "success";
    }

    @RequestMapping("/fail")
    @ResponseBody
    public String fail(){
        return "fail";
    }
}
