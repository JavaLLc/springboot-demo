package com.llc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author LLC
 * @date 2019年6月10日
 * @Description:3.自动配置类
 */

@Configuration
@EnableConfigurationProperties(StarterDemoProperties.class)
@ConditionalOnClass(StarterDemoService.class)
@ConditionalOnProperty(prefix = "spring.starterdemo", value = "enabled", matchIfMissing = true)
public class StarterDemoAutoConfiguration {

	@Autowired
	private StarterDemoProperties properties;

	// 当容器中没有指定Bean的情况下，自动配置StarterDemoService类
	@ConditionalOnMissingBean(StarterDemoService.class)
	@Bean
	public StarterDemoService personService() {
		StarterDemoService starterDemoService = new StarterDemoService(properties);
		return starterDemoService;
	}

}
