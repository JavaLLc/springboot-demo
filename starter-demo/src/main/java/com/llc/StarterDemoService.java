package com.llc;

import lombok.extern.log4j.Log4j2;

/**
 * 
 * @author LLC
 * @date 2019年6月10日
 * @Description:2.核心服务类
 */
@Log4j2
public class StarterDemoService {
	
	private StarterDemoProperties properties;

	public StarterDemoService() {
	}

	public StarterDemoService(StarterDemoProperties properties) {
		this.properties = properties;
	}

	public void sayHello() {
		log.info("大家好，我叫: " + properties.getName() + ", 今年" + properties.getAge() + "岁");
	}
}
