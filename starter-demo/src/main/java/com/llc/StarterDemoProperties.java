package com.llc;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * @author LLC
 * @date 2019年6月10日
 * @Description:1.可以在application.properties中来配置参数覆盖掉默认的值
 */
@ConfigurationProperties(prefix = "spring.starterdemo")
@Getter
@Setter
public class StarterDemoProperties {
	// 姓名
	private String name;
	// 年龄
	private int age;
}
