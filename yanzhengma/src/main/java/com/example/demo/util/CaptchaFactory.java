package com.example.demo.util;

import java.awt.Color;
import java.util.Random;

import com.github.bingoohuang.patchca.color.ColorFactory;
//import com.github.bingoohuang.patchca.color.RandomColorFactory;
import com.github.bingoohuang.patchca.custom.ConfigurableCaptchaService;
import com.github.bingoohuang.patchca.word.RandomWordFactory;

/**
 * Patchca是Piotr Piastucki写的一个java验证码,简单但功能强大
 * 
 * @author lenovo
 *
 */
public class CaptchaFactory {

	private static ConfigurableCaptchaService cs = new ConfigurableCaptchaService();
	private static Random random = new Random();

	static {
		// 随机颜色
		// cs.setColorFactory(new RandomColorFactory());
		// 自定义颜色
		cs.setColorFactory(new ColorFactory() {
			@Override
			public Color getColor(int index) {
				int[] c = new int[3];
				int i = random.nextInt(3);
				for (int fi = 0; fi < 3; fi++) {
					if (fi == i) {
						c[fi] = random.nextInt(71);
					} else {
						c[fi] = random.nextInt(256);
					}
				}
				return new Color(c[0], c[1], c[2]);
			}
		});
		RandomWordFactory wf = new RandomWordFactory();
		// 验证码字符
		wf.setCharacters("23456789abcdefghigkmnpqrstuvwxyzABCDEFGHIGKLMNPQRSTUVWXYZ");
		// 最大长度
		wf.setMaxLength(4);
		// 最小长度
		wf.setMinLength(4);
		cs.setWordFactory(wf);
	}

	public static ConfigurableCaptchaService getInstance() {
		return cs;
	}
}
