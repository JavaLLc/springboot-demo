package com.example.demo.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.util.CaptchaFactory;
import com.github.bingoohuang.patchca.custom.ConfigurableCaptchaService;
import com.github.bingoohuang.patchca.filter.FilterFactory;
import com.github.bingoohuang.patchca.filter.predefined.CurvesRippleFilterFactory;
import com.github.bingoohuang.patchca.filter.predefined.DiffuseRippleFilterFactory;
import com.github.bingoohuang.patchca.filter.predefined.DoubleRippleFilterFactory;
import com.github.bingoohuang.patchca.filter.predefined.MarbleRippleFilterFactory;
import com.github.bingoohuang.patchca.filter.predefined.WobbleRippleFilterFactory;
import com.github.bingoohuang.patchca.utils.encoder.EncoderHelper;

/**
 * @author lenovo
 */
@Controller
@RequestMapping("/security")
public class ValidateController {

	/**
	 * 日志对象
	 */
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private static ConfigurableCaptchaService cs = CaptchaFactory.getInstance();
	private static List<FilterFactory> factories;

	static {
		// 波纹样式
		factories = new ArrayList<>();
		//曲线
		factories.add(new CurvesRippleFilterFactory(cs.getColorFactory()));
		//扩散-大理石波纹
		factories.add(new MarbleRippleFilterFactory());
		//平滑
		factories.add(new DoubleRippleFilterFactory());
		//摇摆
		factories.add(new WobbleRippleFilterFactory());
		//扩散
		factories.add(new DiffuseRippleFilterFactory());
	}

	@RequestMapping("/getImage.htm")
	public void getImage(HttpServletRequest request, HttpServletResponse response) {
		logger.info("获取验证码");
		try {
			int nextInt = new Random().nextInt(5);
			logger.info("随机数" + nextInt);
			cs.setFilterFactory(factories.get(nextInt));
			setResponseHeaders(response);
			//获取session
			HttpSession session = request.getSession();
			//获得验证码
			String token = EncoderHelper.getChallangeAndWriteImage(cs, "png", response.getOutputStream());
			session.setAttribute("EASYLIFE_CAPCHA", token);
			logger.info("当前的SessionID = " + session.getId() + "，  验证码 = " + token);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 设置响应Headers
	 * @param response
	 */
	private void setResponseHeaders(HttpServletResponse response) {
		response.setContentType("image/png");
		response.setHeader("Cache-Control", "no-cache, no-store");
		response.setHeader("Pragma", "no-cache");
		long time = System.currentTimeMillis();
		response.setDateHeader("Last-Modified", time);
		response.setDateHeader("Date", time);
		response.setDateHeader("Expires", time);
	}

	@RequestMapping("/yz")
	@ResponseBody
	public String whetherImgCaptchaMatch(HttpServletRequest request, HttpServletResponse response) {
		logger.info("验证验证码");
		String yzm = request.getParameter("yzm");
		logger.info("请求参数:" + yzm);

		HttpSession session = request.getSession();
		String token = session.getAttribute("EASYLIFE_CAPCHA").toString();
		logger.info("当前的SessionID = " + session.getId() + "，  验证码 = " + token);
		if(yzm.equals(token)){
			return "success";
		}else{
			return "fail";
		}
	}

}