package com.llc.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author LLC
 * @date 2019年6月1日
 * @Description:自定义异常
 */
@Getter
@Setter
@AllArgsConstructor    
public class MyException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	// 异常状态码
	private String code;
	// 异常信息
	private String message;
	// 描述
	private String descinfo;
	
	public MyException(String message,String descinfo) {
		this.message = message;
		this.descinfo = descinfo;
	}
}
