package com.llc.arithmetic.snow;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;

import java.net.Inet4Address;
import java.net.UnknownHostException;

/**
 * 雪花算法工具
 * @author LLC
 * @date 2019年6月17日
 * @Description:默认值
 */
public class SnowflakeUtil {

	// 获取默认的工作ID
	public static Long getWorkId() {
		try {
			String hostAddress = Inet4Address.getLocalHost().getHostAddress();
			int[] ints = StringUtils.toCodePoints(hostAddress);
			int sum = 0;
			for (int b : ints) {
				sum += b;
			}
			return (long) (sum % 32);
		} catch (UnknownHostException e) {
			// 如果获取失败，则使用随机数备用
			return RandomUtils.nextLong(0, 31);
		}
	}

	// 获取默认的数据中心
	public static Long getDataCenterId() {
		int[] ints = StringUtils.toCodePoints(SystemUtils.getHostName());
		int sum = 0;
		for (int i : ints) {
			sum += i;
		}
		return (long) (sum % 32);
	}

}
