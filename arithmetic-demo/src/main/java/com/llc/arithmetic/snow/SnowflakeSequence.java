package com.llc.arithmetic.snow;

import com.llc.exception.MyException;

/**
 * 雪花算法
 * @author LLC
 * @date 2019年6月17日
 * @Description:参考:https://blog.csdn.net/xiaopeng9275/article/details/72123709
 */
public class SnowflakeSequence {

	/**
	 * 开始时间截 (2019-05-02)
	 */
	private final long twepoch = 1556766860027L;

	/**
	 * 机器id所占的位数
	 */
	private final long workerIdBits = 5L;

	/**
	 * 数据标识id所占的位数
	 */
	private final long dataCenterIdBits = 5L;

	/**
	 * 支持的最大机器id，结果是31 (这个移位算法可以很快的计算出几位二进制数所能表示的最大十进制数)
	 */
	private final long maxWorkerId = -1L ^ (-1L << workerIdBits);

	/**
	 * 支持的最大数据标识id，结果是31
	 */
	private final long maxDataCenterId = -1L ^ (-1L << dataCenterIdBits);

	/**
	 * 序列在id中占的位数
	 */
	private final long sequenceBits = 12L;

	/**
	 * 机器ID向左移12位
	 */
	private final long workerIdShift = sequenceBits;

	/**
	 * 数据标识id向左移17位(12+5)
	 */
	private final long dataCenterIdShift = sequenceBits + workerIdBits;

	/**
	 * 时间截向左移22位(5+5+12)
	 */
	private final long timestampLeftShift = sequenceBits + workerIdBits + dataCenterIdBits;

	/**
	 * 生成序列的掩码，这里为4095 (0b111111111111=0xfff=4095)
	 */
	private final long sequenceMask = -1L ^ (-1L << sequenceBits);

	/**
	 * 工作机器ID(0~31)
	 */
	private long workerId;

	/**
	 * 数据中心ID(0~31)
	 */
	private long dataCenterId;

	/**
	 * 毫秒内序列(0~4095)
	 */
	private long sequence = 0L;

	/**
	 * 上次生成ID的时间截
	 */
	private long lastTimestamp = -1L;

	/**
	 * 构造函数
	 *
	 * @param workerId     工作ID [0~31]
	 * @param dataCenterId 数据中心ID [0~31]
	 */
	public SnowflakeSequence(long workerId, long dataCenterId) {
		// 设置默认值
		if (workerId < 0) {
			workerId = SnowflakeUtil.getWorkId();
		}
		if (dataCenterId < 0) {
			dataCenterId = SnowflakeUtil.getDataCenterId();
		}
		// 检查工作ID和数据中心的合法性
		if (workerId > maxWorkerId || workerId < 0) {
			throw new IllegalArgumentException(String.format("workerId不能大于%d或小于0", maxWorkerId));
		}
		if (dataCenterId > maxDataCenterId || dataCenterId < 0) {
			throw new IllegalArgumentException(String.format("dataCenterId不能大于%d或小于0", maxDataCenterId));
		}

		this.workerId = workerId;
		this.dataCenterId = dataCenterId;
	}

	/**
	 * 获得下一个ID
	 * synchronized线程安全
	 * @return
	 */
	public synchronized long nextId() {
		long timestamp = timeGen();

		// 如果当前时间小于上一次ID生成的时间戳，说明系统时钟回退过这个时候应当抛出异常
		if (timestamp < lastTimestamp) {
			throw new MyException(String.format("%d生成ID失败", lastTimestamp - timestamp), "当前时间小于上一次ID生成的时间戳");
		}
		// 如果是同一时间生成的，则进行毫秒内序列
		if (lastTimestamp == timestamp) {
			sequence = (sequence + 1) & sequenceMask;
			// 毫秒内序列溢出
			if (sequence == 0) {
				// 阻塞到下一个毫秒,获得新的时间戳
				timestamp = tilNextMillis(lastTimestamp);
			}
		}
		// 时间戳改变，毫秒内序列重置
		else {
			sequence = 0L;
		}
		// 上次生成ID的时间截
		lastTimestamp = timestamp;
		// 移位并通过或运算拼到一起组成64位的ID
		return ((timestamp - twepoch) << timestampLeftShift) | (dataCenterId << dataCenterIdShift)
				| (workerId << workerIdShift) | sequence;
	}

	/**
	 * 阻塞到下一个毫秒，直到获得新的时间戳
	 *
	 * @param lastTimestamp 上次生成ID的时间截
	 * @return 当前时间戳
	 */
	protected long tilNextMillis(long lastTimestamp) {
		long timestamp = timeGen();
		while (timestamp <= lastTimestamp) {
			timestamp = timeGen();
		}
		return timestamp;
	}

	/**
	 * 返回以毫秒为单位的当前时间
	 *
	 * @return 当前时间(毫秒)
	 */
	protected long timeGen() {
		return System.currentTimeMillis();
	}

	// 得到雪花序列生成器-指定工作ID和数据中心ID
	public static SnowflakeSequence newSnowflakeSeq(long workerId, long datacenterId) {
		return new SnowflakeSequence(workerId, datacenterId);
	}

	// 使用默认的工作ID和数据中心ID
	public static SnowflakeSequence newSnowflakeSeq() {
		return new SnowflakeSequence(-1, -1);
	}

	public static void main(String[] args) {
		// 获得雪花序列生成器
		SnowflakeSequence sequence = SnowflakeSequence.newSnowflakeSeq(21, 21);
		long startTime = System.nanoTime();
		// 生成十万个ID
		for (int i = 0; i < 100000; i++) {
			long id = sequence.nextId();
			System.out.println(id);
		}
		System.out.println((System.nanoTime() - startTime) / 1000000 + "ms");
	}
}
