###Raft(参考:https://blog.csdn.net/xu__cg/article/details/73555161)
#####1.概述
```
与Paxos不同Raft强调的是易懂，Raft和Paxos一样只要保证n/2+1节点正常就能够提供服务；
Raft也使用了分而治之的思想把算法流程分为三个子问题：
选举（Leader election）
日志复制（Log replication）
安全性（Safety）
```
#####2.流程
```
Raft开始时在集群中选举出Leader负责日志复制的管理，Leader接受来自客户端的事务请求（日志），并将它们复制给集群的其他节点，然后负责通知集群中其他节点提交日志，
Leader负责保证其他节点与他的日志同步，当Leader宕掉后集群其他节点会发起选举选出新的Leader；
```

#####3.详解
```
1、 角色
　　Raft把集群中的节点分为三种状态：Leader、 Follower 、Candidate，每种状态负责的任务也是不一样的，Raft运行时提供服务的时候只存在Leader与Follower两种状态；
　　Leader（领导者）：负责日志的同步管理，处理来自客户端的请求，与Follower保持心跳的联系；
　　Follower（追随者）：刚启动时所有节点为Follower状态，响应Leader的日志同步请求，响应Candidate的请求，把请求到Follower的事务转发给Leader；
　　Candidate（候选者）：负责选举投票，Raft刚启动时由一个节点从Follower转为Candidate发起选举，选举出Leader后从Candidate转为Leader状态；
2、 Term
　　选举周期,每个Term都是一个连续递增的编号，每一轮选举都是一个Term周期
	Raft开始时所有Follower的Term为1，其中一个Follower逻辑时钟到期后转换为Candidate，Term加1这时Term为2（任期），然后开始选举，这时候有几种情况会使Term发生改变：
　　1：如果当前Term为2的任期内没有选举出Leader或出现异常，则Term递增，开始新一任期选举
　　2：当这轮Term为2的周期选举出Leader后，过后Leader宕掉了，然后其他Follower转为Candidate，Term递增，开始新一任期选举
　　3：当Leader或Candidate发现自己的Term比别的Follower小时Leader或Candidate将转为Follower，Term递增
　　4：当Follower的Term比别的Term小时Follower也将更新Term保持与其他Follower一致；
　　当某节点收到的请求中Term比当前Term小时则拒绝该请求；
3、 选举（Election）
　　Raft的选举由定时器来触发，每个节点的选举定时器时间都是不一样的，开始时状态都为Follower某个节点定时器触发选举后Term递增，状态由Follower转为Candidate，
	向其他节点发起RequestVote RPC请求，这时候有三种可能的情况发生：
　　1：该RequestVote请求接收到n/2+1（过半数）个节点的投票，从Candidate转为Leader，向其他节点发送heartBeat以保持Leader的正常运转
　　2：在此期间如果收到其他节点发送过来的AppendEntries RPC请求，如该节点的Term大则当前节点转为Follower，否则保持Candidate拒绝该请求
　　3：Election timeout发生则Term递增，重新发起选举
　　在一个Term期间每个节点只能投票一次，所以当有多个Candidate存在时就会出现每个Candidate发起的选举都存在接收到的投票数都不过半的问题，
	这时每个Candidate都将Term递增、重启定时器并重新发起选举，由于每个节点中定时器的时间都是随机的，所以就不会多次存在有多个Candidate同时发起投票的问题。
　　有这么几种情况会发起选举:
		1：Raft初次启动，不存在Leader，发起选举；
		 2：Leader宕机或Follower没有接收到Leader的heartBeat，发生election timeout从而发起选举;
4、 日志复制（Log Replication）
	日志复制（Log Replication）主要作用是用于保证节点的一致性，这阶段所做的操作也是为了保证一致性与高可用性；
	当Leader选举出来后便开始负责客户端的请求，所有事务（更新操作）请求都必须先经过Leader处理，这些事务请求或说成命令也就是这里说的日志，
	保证节点的一致性就要保证每个节点都按顺序执行相同的操作序列，日志复制（Log Replication）就是为了保证执行相同的操作序列所做的工作；
	在Raft中当接收到客户端的日志（事务请求）后先把该日志追加到本地的Log中，然后通过heartbeat把该Entry同步给其他Follower,
	Follower接收到日志后记录日志然后向Leader发送ACK，当Leader收到大多数（n/2+1）Follower的ACK信息后将该日志设置为已提交并追加到本地磁盘中，
	通知客户端并在下个heartbeat中Leader将通知所有的Follower将该日志存储在自己的本地磁盘中。
5、 安全性（Safety）
　　安全性是用于保证每个节点都执行相同序列的安全机制，如当某个Follower在当前Leader commit Log时变得不可用了，稍后可能该Follower又会倍选举为Leader，
	这时新Leader可能会用新的Log覆盖先前已committed的Log，这就是导致节点执行不同序列；Safety就是用于保证选举出来的Leader一定包含先前 commited Log的机制；
　　选举安全性（Election Safety）每个Term只能选举出一个Leader
　　Leader完整性（Leader Completeness）
　　这里所说的完整性是指Leader日志的完整性，当Log在Term1被Commit后，那么以后Term2、Term3…等的Leader必须包含该Log；
	Raft在选举阶段就使用Term的判断用于保证完整性：当请求投票的该Candidate的Term较大或Term相同Index更大则投票，否则拒绝该请求；
```
#####例子参考:https://github.com/sofastack/sofa-jraft