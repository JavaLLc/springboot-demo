package com.llc.arithmetic.encryption.aes;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import com.llc.arithmetic.encryption.base64.Base64Utils;

/**
 * AES加解密
 *
 * @Author:chenssy
 * @date:2016年5月21日 上午9:01:41
 */
public class AESUtils {
	/** 默认秘钥 */
	protected static final String KEY = "w@#$4@#$s^&3*&^4";

	/**
	 * AES解密
	 * @param encryptValue 密文
	 * @param key 密钥
	 * @return
	 * @throws Exception
	 */
	public static String decrypt(String encryptValue, String key) throws Exception {
		return aesDecryptByBytes(base64Decode(encryptValue), key);
	}

	/**
	 * AES加密
	 * @param value 待加密内容
	 * @param key 密钥
	 * @return
	 * @throws Exception
	 */
	public static String encrypt(String value, String key) throws Exception {
		return base64Encode(aesEncryptToBytes(value, key));
	}

	private static String base64Encode(byte[] bytes) {
		return Base64Utils.encrypt(bytes);
	}

	private static byte[] base64Decode(String base64Code) throws Exception {
		return base64Code == null ? null : Base64Utils.decrypt(base64Code);
	}

	private static byte[] aesEncryptToBytes(String content, String encryptKey) throws Exception {
		KeyGenerator kgen = KeyGenerator.getInstance("AES");
		kgen.init(128, new SecureRandom(encryptKey.getBytes()));
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(kgen.generateKey().getEncoded(), "AES"));
		return cipher.doFinal(content.getBytes("utf-8"));
	}

	private static String aesDecryptByBytes(byte[] encryptBytes, String decryptKey) throws Exception {
		KeyGenerator kgen = KeyGenerator.getInstance("AES");
		kgen.init(128, new SecureRandom(decryptKey.getBytes()));
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(kgen.generateKey().getEncoded(), "AES"));
		byte[] decryptBytes = cipher.doFinal(encryptBytes);
		return new String(decryptBytes);
	}

	// pE7l6lOSwazFqpwRroO8og==
	public static void main(String[] args) {
		try {
			String encrypt = AESUtils.encrypt("123456", KEY);
			String decrypt = AESUtils.decrypt(encrypt, KEY);
			System.out.println(encrypt);
			System.out.println(decrypt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
