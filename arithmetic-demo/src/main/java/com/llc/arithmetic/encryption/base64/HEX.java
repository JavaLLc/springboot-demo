package com.llc.arithmetic.encryption.base64;

import java.util.Arrays;

import org.apache.commons.codec.binary.Hex;

public class HEX {
	public static void main(String[] args) {
		// 一个字符串
		String hex = "HEX";
		// 获取字符串的byte数组
		byte[] buf = hex.getBytes();
		// 输出byte数组
		System.out.println(Arrays.toString(buf));
		// 转换成Hex输出
		System.out.println(Hex.encodeHexString(buf));
	}
}
