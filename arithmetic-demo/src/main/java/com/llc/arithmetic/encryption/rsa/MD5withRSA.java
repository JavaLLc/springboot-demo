package com.llc.arithmetic.encryption.rsa;

import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import org.apache.commons.codec.binary.Hex;
import org.apache.xmlbeans.impl.util.Base64;

/**
 * 数字签名 
 * 1:MD5withRSA：将正文通过MD5数字摘要后，将密文 再次通过生成的RSA密钥加密，生成数字签名，
 * 将明文与密文以及公钥发送给对方，对方拿到私钥/公钥对数字签名进行解密，然后解密后的，与明文经过MD5加密进行比较 如果一致则通过
 * 2:使用Signature的API来实现MD5withRSA
 * 使用2
 *
 */
public class MD5withRSA {

	/**
	 * 使用RSA生成一对钥匙
	 * 
	 * @param str
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static KeyPair getKeyPair() throws NoSuchAlgorithmException {
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(1024);
		// 生成返回带有公钥和私钥的对象
		KeyPair generateKeyPair = keyPairGenerator.generateKeyPair();
		return generateKeyPair;
	}

	/**
	 * 生成私钥
	 * 
	 * @param str
	 * @return
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static PrivateKey getPrivateKey(KeyPair key) {
		PrivateKey generatePrivate = null;
		try {
			PrivateKey private1 = key.getPrivate();
			byte[] encoded = private1.getEncoded();
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
			KeyFactory factory = KeyFactory.getInstance("RSA");
			generatePrivate = factory.generatePrivate(keySpec);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return generatePrivate;
	}

	/**
	 * 私钥加密
	 * 
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 */
	public static byte[] encrtpyByPrivateKey(byte[] bb, PrivateKey key)
			throws IllegalBlockSizeException, BadPaddingException {
		byte[] doFinal = null;
		try {
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			doFinal = cipher.doFinal(bb);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doFinal;
	}

	/**
	 * 获取公钥
	 * 
	 * @param str
	 * @return
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static PublicKey getPublicKey(KeyPair keyPair) {
		PublicKey publicKey = null;
		try {
			PublicKey public1 = keyPair.getPublic();
			byte[] encoded = public1.getEncoded();
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
			KeyFactory factory = KeyFactory.getInstance("RSA");
			publicKey = factory.generatePublic(keySpec);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return publicKey;
	}

	/**
	 * 使用公钥解密
	 * 
	 * @param str
	 * @return
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static byte[] decodePublicKey(byte[] b, PublicKey key) {
		byte[] doFinal = null;
		try {
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, key);
			doFinal = cipher.doFinal(b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doFinal;
	}

	// 通过MD5加密
	public static byte[] encryptMD5(String str) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("MD5");
		byte[] digest2 = digest.digest(str.getBytes());
		return digest2;
	}

	// 数据+私钥 签名
	public static byte[] sign(String str, PrivateKey key)
			throws NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException {
		byte[] encryptMD5 = encryptMD5(str);
		byte[] encrtpyByPrivateKey = encrtpyByPrivateKey(encryptMD5, key);
		return encrtpyByPrivateKey;
	}

	// 校验
	public static boolean verify(String str, byte[] sign, PublicKey key) throws NoSuchAlgorithmException {
		byte[] encryptMD5 = encryptMD5(str);
		byte[] decodePublicKey = decodePublicKey(sign, key);
		String a = new String(encryptMD5);
		String b = new String(decodePublicKey);
		if (a.equals(b)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Signature的用法 数字签名
	 * 
	 * @param args
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws SignatureException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static byte[] signMethod(String str, PrivateKey key)
			throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
		// 初始化 MD5withRSA
		Signature signature = Signature.getInstance("MD5withRSA");
		// 使用私钥
		signature.initSign(key);
		// 需要签名或校验的数据
		signature.update(str.getBytes());
		return signature.sign();// 进行数字签名
	}

	/**
	 * 数字校验
	 * @param str 数据
	 * @param sign 数字签名
	 * @param key 密钥/公钥
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws SignatureException
	 */
	public static boolean verifyMethod(String str, byte[] sign, PublicKey key)
			throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
		Signature signature = Signature.getInstance("MD5withRSA");
		signature.initVerify(key);
		signature.update(str.getBytes());
		return signature.verify(sign);
	}

	/**
	 * 通过私钥(String类型)获取PrivateKey类型私钥
	 * 
	 * @param key
	 * @return
	 */
	public static PrivateKey getPrivateKeyByStr(String privateKeyStr) {
		PrivateKey priKey = null;
		// 转化私钥材料
		PKCS8EncodedKeySpec pkcs8keySpec = new PKCS8EncodedKeySpec(Base64.decode(privateKeyStr.getBytes()));
		// 实例化秘钥工厂
		KeyFactory keyFactory = null;
		try {
			keyFactory = KeyFactory.getInstance("RSA");
			// 取私钥对象
			priKey = keyFactory.generatePrivate(pkcs8keySpec);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return priKey;
	}

	/**
	 * 通过公钥(String类型)获取PrivateKey类型公钥
	 * 
	 * @param key
	 * @return
	 */
	public static PublicKey getPublicKeyByStr(String publicKeyStr) {
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.decode(publicKeyStr.getBytes()));
		PublicKey generatePublic = null;
		KeyFactory factory = null;
		try {
			factory = KeyFactory.getInstance("RSA");
			generatePublic = factory.generatePublic(keySpec);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return generatePublic;
	}

	/**
	 * 获得公钥/私钥-字符串形式
	 * @throws NoSuchAlgorithmException
	 */
	public static String[] getKeyPairToString() throws NoSuchAlgorithmException {
		KeyPair keyPair = MD5withRSA.getKeyPair();
		PublicKey publicKey = MD5withRSA.getPublicKey(keyPair);
		PrivateKey privateKey = MD5withRSA.getPrivateKey(keyPair);

		String publicKey_str = new String(Base64.encode(publicKey.getEncoded()));
		String privateKey_str = new String(Base64.encode(privateKey.getEncoded()));
		System.out.println("公钥(长度--" + publicKey_str.length() + "):" + publicKey_str);
		System.out.println("私钥(长度--" + privateKey_str.length() + "):" + privateKey_str);
		String[] pair = { privateKey_str, publicKey_str };
		return pair;
	}

	public static void main(String[] args) {
		try {
			String[] keyPairToString = getKeyPairToString();
			// 数据
			String d = "123456";
			// 获取PrivateKey类型私钥
			PrivateKey privateKey = getPrivateKeyByStr(keyPairToString[0]);
			// Signature 数字签名(加密)
			byte[] sign = signMethod(d, privateKey);
			String signMethodS = new String(Hex.encodeHex(sign));
			System.out.println("Hex编码" + signMethodS);
			// URLEncoder转码
			String encode = URLEncoder.encode(signMethodS, "UTF-8");
			System.out.println("URLEncoder转码" + encode);
			System.out.println("-------------------验证开始------------------");
			// 公钥-PublicKey
			PublicKey publicKey = getPublicKeyByStr(keyPairToString[1]);
			boolean b = verifyMethod(d, sign, publicKey);
			System.out.println("验证结果" + b);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}