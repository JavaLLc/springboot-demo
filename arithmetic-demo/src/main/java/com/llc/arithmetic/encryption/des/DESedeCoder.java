package com.llc.arithmetic.encryption.des;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 * DESede对称加密算法
 * @author LLC
 * @date 2019年6月20日
 * @Description:
 */
public class DESedeCoder {
	/** 
	 * 密钥算法 
	 * */
	public static final String KEY_ALGORITHM = "DESede";

	/** 
	 * 加密/解密算法/工作模式/填充方式 
	 * */
	public static final String CIPHER_ALGORITHM = "DESede/ECB/PKCS5Padding";

	/** 
	 *  
	 * 生成密钥 
	 * @return byte[] 二进制密钥 
	 * */
	public static byte[] initkey() throws Exception {
		// 实例化密钥生成器
		KeyGenerator kg = KeyGenerator.getInstance(KEY_ALGORITHM);
		// 初始化密钥生成器
		kg.init(168);
		// 生成密钥
		SecretKey secretKey = kg.generateKey();
		// 获取二进制密钥编码形式
		return secretKey.getEncoded();
	}

	/** 
	 * 转换密钥 
	 * @param key 二进制密钥 
	 * @return Key 密钥 
	 * */
	public static Key toKey(byte[] key) throws Exception {
		// 实例化Des密钥
		DESedeKeySpec dks = new DESedeKeySpec(key);
		// 实例化密钥工厂
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(KEY_ALGORITHM);
		// 生成密钥
		SecretKey secretKey = keyFactory.generateSecret(dks);
		return secretKey;
	}

	/** 
	 * 加密数据 
	 * @param data 待加密数据 
	 * @param key 密钥 
	 * @return byte[] 加密后的数据 
	 * */
	public static byte[] encrypt(byte[] data, byte[] key) throws Exception {
		// 还原密钥
		Key k = toKey(key);
		// 实例化
		Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
		// 初始化，设置为加密模式
		cipher.init(Cipher.ENCRYPT_MODE, k);
		// 执行操作
		return cipher.doFinal(data);
	}

	/** 
	 * 解密数据 
	 * @param data 待解密数据 
	 * @param key 密钥 
	 * @return byte[] 解密后的数据 
	 * */
	public static byte[] decrypt(byte[] data, byte[] key) throws Exception {
		// 转换密钥
		Key k = toKey(key);
		// 实例化
		Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
		// 初始化，设置为解密模式
		cipher.init(Cipher.DECRYPT_MODE, k);
		// 执行操作
		return cipher.doFinal(data);
	}

	/** 
	 * 进行加解密的测试 
	 * @throws Exception  
	 */
	public static void main(String[] args) throws Exception {
		// 秘钥
		byte[] key = initkey();
		String keyString = new String(Base64.encodeBase64(key));
		System.out.println("密钥:" + keyString);
		System.out.println("-----------------------------加密-------------------------------------------");
		// 明文
		String string = "123456";
		// 字符串形式的秘钥转码
		byte[] base64 = Base64.decodeBase64(keyString.getBytes());
		// 明文byte[]转码
		byte[] bytes = string.getBytes("UTF-8");
		// 加密
		byte[] encrypt = DESedeCoder.encrypt(bytes, base64);
		// 密文转码
		String base_encrypt = new String(Base64.encodeBase64(encrypt), "UTF-8");
		System.out.println(base_encrypt);
		// URLEncoder转码
		String encode = URLEncoder.encode(base_encrypt, "UTF-8");
		// 加密后密文
		System.out.println(encode);

		System.out.println("------------------------------解密-------------------------------------------");
		//URLDecoder解码
		String decode = URLDecoder.decode(encode,"utf-8");
		//Base64解码
		byte[] decodeBase64 = Base64.decodeBase64(decode.getBytes());
		// 解密
		byte[] decrypt = DESedeCoder.decrypt(decodeBase64, base64);
		// 转码打印
		System.out.println(new String(decrypt, "UTF-8"));
	}

}