package com.llc.arithmetic.paxos;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.llc.arithmetic.paxos.doer.Acceptor;
import com.llc.arithmetic.paxos.doer.Proposer;

public class PaxosMain {
	// 提案人数
	private static final int NUM_OF_PROPOSER = 5;
	// 决策人数
	private static final int NUM_OF_ACCEPTOR = 7;

	public static CountDownLatch latch = new CountDownLatch(NUM_OF_PROPOSER);;

	public static void main(String[] args) {
		// 决策者
		List<Acceptor> acceptors = new ArrayList<>();
		for (int i = 0; i < NUM_OF_ACCEPTOR; i++) {
			acceptors.add(new Acceptor());
		}
		// 线程池
		ExecutorService es = Executors.newCachedThreadPool();
		for (int i = 0; i < NUM_OF_PROPOSER; i++) {
			Proposer proposer = new Proposer(i, i + "#Proposer", NUM_OF_PROPOSER, acceptors);
			es.submit(proposer);
		}
		es.shutdown();
	}
}
