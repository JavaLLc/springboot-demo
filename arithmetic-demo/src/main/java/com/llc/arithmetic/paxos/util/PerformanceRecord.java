package com.llc.arithmetic.paxos.util;

import lombok.extern.log4j.Log4j2;

/**
 * 性能记录
 * @author LLC
 * @date 2019年6月17日
 * @Description:
 */
@Log4j2
public class PerformanceRecord {
	private static final int MAX_ID_VALUE = 200;
	private static PerformanceRecord instance = null;
	private String[] despArray;
	private long[] startArray;

	// 实例获取方法
	public static PerformanceRecord getInstance() {
		if (null == instance) {
			synchronized (PerformanceRecord.class) {
				if (null == instance)
					instance = new PerformanceRecord();
			}
		}
		return instance;
	}

	private PerformanceRecord() {
		despArray = new String[MAX_ID_VALUE];
		startArray = new long[MAX_ID_VALUE];
	}

	/**
	 * 开始性能记录。调用此函数后，计时开始
	 * ID >= 0 && ID < 200
	 * @param description
	 * @param ID
	 */
	public void start(String description, int ID) {
		despArray[ID] = description;
		startArray[ID] = System.currentTimeMillis();
	}

	/**
	 * 结束id为ID的性能记录
	 * @param ID
	 */
	public void end(int ID) {
		long endTime = System.currentTimeMillis();
		String performStr = String.format("%s 耗时为%d ms.", despArray[ID], endTime - startArray[ID]);
		log.info(performStr);
		despArray[ID] = null;
	}
}
