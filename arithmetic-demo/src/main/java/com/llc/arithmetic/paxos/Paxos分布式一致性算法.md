###Paxos一致性算法(参考:https://blog.csdn.net/gaixm/article/details/78314512)
```
(Zookeeper也是基于Paxos算法来实现)
准备阶段
Proposer阶段1：发送提案
Proposer生成全局唯一且递增的提案ID，并向Paxos集群的所有机器发送请求，这里只需携带提案ID即可，无需携带提案内容（暂且把提案ID叫做Pn）

Proposer阶段2：Acceptor应答Proposer请求
Acceptor收到请求后：
不再应答 提案ID <= Pid(上一个) 的Proposer请求
对于 < Pid 的Accept请求也不处理

Acceptor对于Proposer请求处理：
应答前要在本地持久化当前提案ID（Pid）
如果现在请求的提案ID（Pid）大于此前存放的ProposerID，则做以下逻辑：
如果 Pid > ProposerID 那么 ProposerID = Pid
如果该Acceptor Accept过提案，则返回提案中ProposerID最大的那个提案的内容，否则返回空值。

确认阶段
Accept阶段1：Proposer发送Accept请求
Proposer收集到多数派应答（多数派是指超过 n/2 + 1单数,n是集群数）Proposer阶段的返回值后，从中选择ProposerID最大的提案内容，
作为要发起Accept的提案，如果这个提案为空值，则可以自己随意决定提案内容。然后携带上当前ProposerID，向Paxos集群的所有机器发送Accept请求。

Accept阶段2：Acceptor应答Accept请求
Acceptor收到Accept请求后，检查不违背约定的情况下，持久化当前ProposerID和提案内容，最后Proposer收集到多数派应答的Accept回复后，形成决议。
```

