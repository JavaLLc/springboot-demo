package com.llc.arithmetic.paxos.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 决策者针对提议者的准备提案的回复
 * @author LLC
 * @date 2019年6月17日
 * @Description:
 */
@Getter
@Setter
@ToString
public class PrepareResult {
	// 是否承诺
	private boolean isPromised;
	// 决策者的状态
	private AcceptorStatus acceptorStatus = AcceptorStatus.NONE;
	// 决策者返回的提案。
	private Proposal proposal;
}
