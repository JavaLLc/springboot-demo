package com.llc.arithmetic.paxos.entity;

/**
 * 决策者记录已处理提案的状态
 * @author LLC
 * @date 2019年6月17日
 * @Description:
 */
public enum AcceptorStatus {
	//接受
	ACCEPTED,
	//承诺 
	PROMISED,
	//未处理
	NONE
}
