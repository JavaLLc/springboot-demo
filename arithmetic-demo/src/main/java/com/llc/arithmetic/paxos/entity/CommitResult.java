package com.llc.arithmetic.paxos.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 提议者向决策者确认时,决策者的确认回复
 * @author LLC
 * @date 2019年6月17日
 * @Description:
 */
@Getter
@Setter
@ToString
public class CommitResult {
	// 提交的提案是否被接受
	private boolean accepted = false;
	// 决策者的状态
	private AcceptorStatus acceptorStatus = AcceptorStatus.NONE;
	// 返回的决策者已确认的提案
	private Proposal proposal;
}
