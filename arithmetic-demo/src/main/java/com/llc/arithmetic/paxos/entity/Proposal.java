package com.llc.arithmetic.paxos.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 提案
 * @author LLC
 * @date 2019年6月17日
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Proposal {
	// 提案的序列号
	private int id;
	// 提案名称
	private String name;
	// 提案的值
	private String value;
	
	public void copyFromInstance(Proposal proposal){
		this.id = proposal.id;
		this.name = proposal.name;
		this.value = proposal.value;
	}
}
