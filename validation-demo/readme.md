###验证器
```
bean validation:使用注解的方式来实现对 Java Bean 的验证功能
内嵌约束注解:
@Null	验证对象是否为空
@NotNull	验证对象是否为非空
@AssertTrue	验证 Boolean 对象是否为 true
@AssertFalse	验证 Boolean 对象是否为 false
@Min	验证 Number 和 String 对象是否大等于指定的值
@Max	验证 Number 和 String 对象是否小等于指定的值
@DecimalMin	验证 Number 和 String 对象是否大等于指定的值，小数存在精度
@DecimalMax	验证 Number 和 String 对象是否小等于指定的值，小数存在精度
@Size	验证对象（Array,Collection,Map,String）长度是否在给定的范围之内
@Digits	验证 Number 和 String 的构成是否合法
@Past	验证 Date 和 Calendar 对象是否在当前时间之前
@Future	验证 Date 和 Calendar 对象是否在当前时间之后
@Pattern	验证 String 对象是否符合正则表达式的规则
@Email	验证对象是否为邮箱地址
@Length(min=,max=)	验证对象长度是否在指定范围内
@NotEmpty	验证字符串非空
@Range(min=,max=,message=)	验证 对象必须在合适范围内
@NotBlank(message =)	验证字符串非null且长度大于0


接口接收参数需要添加注解:@Validated
```
#####测试
```
访问:http://localhost:8081/hello?id=&name= &password=123&email=123&mobile=123
5 errors
访问:http://localhost:8081/hello?id=1&name= &password=123&email=123&mobile=123
4 errors
访问:http://localhost:8081/hello?id=1&name=zs&password=123&email=123&mobile=123
3 errors
访问:http://localhost:8081/hello?id=1&name=zs&password=123456&email=123@qq.com&mobile=13766699969
2 errors
访问:http://localhost:8081/hello?id=1&name=zs&password=123456&email=123@qq.com&mobile=123
1 errors
访问:http://localhost:8081/hello?id=1&name=zs&password=123456&email=123@qq.com&mobile=13766699969
ok
```



