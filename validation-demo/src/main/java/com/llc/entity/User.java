package com.llc.entity;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * NotNull 对象非空,集合对象不能是 null, 但可以是空集
 * NotBlank 字符串非null,且去除两端空白字符后的长度必须大于0 
 * NotEmpty 对象,非空,并且相关对象的 size 大于 0
 * @author LLC
 * @date 2019年6月28日
 * @Description:
 */
@Getter
@Setter
@ToString
public class User {
	@NotNull(message = "主键ID不能为空")
	private Integer id;
    @NotBlank(message = "姓名不能为空")
    private String name;
    @NotBlank(message = "密码不能为空")
    @Size(min=6,max = 10,message="密码长度不能小于6且不能大于10")
    private String password;
    @Email(message = "电子邮件验证不通过",groups = Default.class)
    private String email;
    @Pattern(regexp = "^\\s*|((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\\d{8}$",message = "手机号验证不通过")// 验证为空字符串或手机号码
    private String mobile;
}
