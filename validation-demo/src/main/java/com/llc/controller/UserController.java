package com.llc.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.llc.entity.User;

@RestController
public class UserController {

	@RequestMapping("/hello")
	public User demo(@Validated User user) {
		return user;
	}

}
