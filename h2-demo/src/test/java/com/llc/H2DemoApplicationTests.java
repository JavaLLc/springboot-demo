package com.llc;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.llc.dao.IUserRepository;
import com.llc.entity.UserEntity;

import lombok.extern.log4j.Log4j2;

@RunWith(SpringRunner.class)
@SpringBootTest
@Log4j2
public class H2DemoApplicationTests {

	@Autowired
	private IUserRepository userRepository;

	@Test
	public void contextLoads() {
		List<UserEntity> list = userRepository.findByName("Jone");
		log.info("查询结果:{}", list);
	}

}
