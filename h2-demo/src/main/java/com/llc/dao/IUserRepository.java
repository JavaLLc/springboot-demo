package com.llc.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.llc.entity.UserEntity;

/**
 * 
 * @author LLC
 * @date 2019年6月11日
 * @Description:
 */
@Repository
public interface  IUserRepository extends JpaRepository<UserEntity,Long> {

    List<UserEntity> findByName(String name);

}
